"  after/indent/nix.vim

" TODO maybe it would be nicer to modify the indentexpr instead (so that a
" trailing ":" on previous non blank causes the indention to be increased?
"
" Also: https://github.com/LnL7/vim-nix/issues/30 - not the same issue but
" related.

if !exists('b:did_indent')
  finish
endif

" This modifies the indent from vim-nix so that
"
" In situation like:   foo:
"                        foo bar baz; {}
"                                     |
"                                   Press <Return> here
"
" Becomes like:        foo:
"                        foo bar baz; {
"                        }
"
" Instead of like:     foo:
"                      foo bar baz: {
"                      }
"
setlocal indentkeys-=*<Return>
setlocal indentkeys+=*0<Return>


" The upstream indent script doesn't set this so we must.
let b:undo_indent = 'setlocal indentkeys< indentexpr<'
