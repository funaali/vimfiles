" File: ~/.vim/after/plugin/utl_uri.vim
" Author: Samuli Thomasson
" License: WTFPL

" Utl documentation: <url:vimhelp:utl>

if !exists('loaded_utl_uri')
  finish
endif

" Original version of this function in
" <url:file:~/.cache/vim/dein/.cache/vimrc/.dein/plugin/utl_uri.vim>
" parses things like ".config", ".foo." and so on domain names instead of
" filenames, which breaks relative links e.g. ".config/foobar". This custom
" version fixes that.

let s:Orig_authority = funcref('UtlUri_authority')
let s:Orig_path      = funcref('UtlUri_path')
let s:Orig_scm_file  = funcref('Utl_AddressScheme_file')

function! Utl_AddressScheme_file(uri, fragment, dispMode)
  function! UtlUri_authority(uri)
    return '<undef>'
  endfunction
  function! UtlUri_path(uri)
    return call(s:Orig_authority, [a:uri]) . call(s:Orig_path, [a:uri])
  endfunction
  let result = call(s:Orig_scm_file, [a:uri, a:fragment, a:dispMode])
  function! UtlUri_authority(uri)
    return call(s:Orig_authority, [a:uri])
  endfunction
  function! UtlUri_path(uri)
    return call(s:Orig_path, [a:uri])
  endfunction
  return result
endfunction
