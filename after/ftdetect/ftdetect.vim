" File: after/ftdetect/ftdetect.vim
" Related Files: ftdetect/ftdetect.vim
" vint: -ProhibitAutocmdWithNoGroup (already in filetypedetect from filetype.vim)

" Overrule a "setl ft=json" from the "json" plugin
au! BufRead,BufNewFile *.template

" jinja2
au BufRead,BufNewFile *.{j2,jinja{,2}}
      \ exe "doautocmd filetypedetect BufRead " . fnameescape(expand('<afile>:r'))
      \ | let &l:ft = substitute(&l:ft, '.\zs$', '.', '') . 'jinja2'

" Ansible
au BufRead,BufNewFile */{defaults,handlers,meta,tasks,vars}/[^/]\+.y{a,}ml set ft=yaml.ansible
au BufRead,BufNewFile */ansible*.y{a,}ml set ft=yaml.ansible
