" Syntax: org

if !exists('b:current_syntax')
  finish
endif

call SyntaxRange#Include('#+BEGIN_SRC sh', '#+END_SRC', 'sh')
call SyntaxRange#Include('#+BEGIN_SRC haskell',  '#+END_SRC', 'haskell', 'comment')
call SyntaxRange#Include('#+BEGIN_SRC markdown', '#+END_SRC', 'markdown', 'comment')
call SyntaxRange#Include('#+BEGIN_SRC help',     '#+END_SRC', 'help', 'comment')
