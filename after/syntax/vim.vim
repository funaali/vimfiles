" Syntax: vim

if !exists('b:current_syntax')
   finish
endif

" There can be line comment after map {rhs}, if written as .. |" a comment.
" Highlight that.
syntax match vimLineComment +[=\\]\@<!|".*$+ containedin=vimMapRhs

" Alias plugin
syntax keyword vimAliasCommand Alias containedin=vimIsCommand

highlight helpExample cterm=bold ctermfg=14 guifg=#93a1a1
highlight link vimAliasCommand vimMap

" highlight <cmd> <url:vimhelp::map-cmd>
syntax match vimNotation /\%#=1\c\(\\\|<lt>\)\=<\(cmd\)>/

" Make "# .." style comments display as errors instead
hi link vim9Comment     Error
hi link vim9LineComment Error
