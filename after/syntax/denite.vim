
" Extra highlights
syntax match deniteSource_fileDirectory +.*/+ contained containedin=deniteSource_file
hi link  deniteSource_fileDirectory Directory

" Fixes for jump source
syn match deniteSource_jump_Text /text:.*/  contained containedin=deniteSource_jump
syn match deniteSource_jump_lnum  /\s*\d\+/    contained nextgroup=deniteSource_jump_bufnr containedin=deniteSource_jump
syn match deniteSource_jump_bufnr /\s*\d\+/    contained nextgroup=deniteSource_jump_col
syn match deniteSource_jump_col   /\s*\d\+\s*/ contained nextgroup=deniteSource_jump_Text,deniteSource_jump_File
hi link deniteSource_jump_lnum  LineNr
hi link deniteSource_jump_bufnr Constant
hi link deniteSource_jump_col   Constant

" register
syn match deniteSource_register_name     /.[^:]*\ze:/   contained nextgroup=deniteSource_register_sep containedin=deniteSource_register
syn match deniteSource_register_sep      /:/    contained nextgroup=deniteSource_register_content
syn match deniteSource_register_content  /..*/  contained
hi link deniteSource_register_name     Special
hi link deniteSource_register_sep      Label
hi link deniteSource_register_content  Normal
