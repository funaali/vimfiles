" kitty
" extends https://github.com/sudorook/kitty.vim/blob/master/syntax/kitty.vim

if !exists('b:current_syntax')
  finish
endif

" Misc
syn keyword kittyNone none contained skipwhite
" Add "-" to isk so that "no-op" is recognized as a keyword
syn iskeyword @,48-57,_,-,192-255
syn keyword kittyAction contained no-op
" {kitty_pid}
syn match kittyPID '{kitty_pid}' contained containedin=kittySt
hi def link kittyPID Special

" for indicating syntax errors
syn match kittyError '\S\+' contained
hi def link kittyError Error

" add @Spell to comments
syn clear kittyComment
syn match kittyComment /^\s*#.*$/ contains=kittyTodo,@Spell

" globinclude kitty.d/**/*.conf
syn match kittyInclude /^globinclude/ display

" Booleans
syn match kittyBool '\s\%(yes\|no\)\ze\%(\s\|$\)'ms=s+1 contained containedin=kittySt
hi def link kittyBool Boolean

" Recognize "\n", "\x00", "\uNNNN" etc.
syn match kittyStringEsc +\\[abfnrtv'"\\]+ display contained containedin=kittySt
syn match kittyStringEsc +\\x[0-9a-f]\{2}+ display contained containedin=kittySt
syn match kittyStringEsc +\\u[0-9a-f]\{4}+ display contained containedin=kittySt
hi def link kittyStringEsc SpecialChar

" @foobar: special arguments
" https://sw.kovidgoyal.net/kitty/launch/#special-arguments
syn match kittySpecialArg '@\%(selection\|active-kitty-window-id\|line-count\|input-line-number\|scrolled-by\|cursor-[xy]\|\%(first\|last\)-line-on-screen\)' contained containedin=kittySt
hi def link kittySpecialArg PreProc

" Environment variable interpolation; ${...}
syn match kittyVar '\${[[:alnum:]_]*}' contained containedin=kittySt
hi def link kittyVar PreProc

" Recognize "c" as a unit specifier (e.g. "initial_window_height 40c")
syn match kittyUnit 'c' contained
syn match kittyNumber '\s[+-]\?\d\+\.\?\d*c'ms=s+1 contained contains=kittyUnit

" Action: (any)
syn cluster kittyActionList contains=kittyActionCombine,kittyActionSendText,kittyAction,kittyNumber,kittyColor

" Action: combine <separator> action1 <separator> action2 ...
syn keyword kittyActionCombine combine contained nextgroup=kittyCombine skipwhite
syn region kittyCombine matchgroup=Operator start=+\z(\S\)+ end=+$+ end=+\z1+me=e-1 contained keepend contains=@kittyActionList nextgroup=kittyCombine skipwhite
hi def link kittyActionCombine Structure

" Action: send_text [mode] special text
syn keyword kittyActionSendText send_text contained nextgroup=kittySendTextModes,kittyError skipwhite
syn match kittySendTextModes '\%(all\|\%(\%(normal\|application\|kitty\),\?\)\{1,3}\)' contained nextgroup=kittySendTextString
syn match kittySendTextString '.\+' contained contains=kittyStringEsc
hi def link kittyActionSendText kittyAction
hi def link kittySendTextModes Type
hi def link kittySendTextString String

" KW: map
syn clear kittyMap kittyKeybind
syn match kittyMap '^map' display nextgroup=kittyKeybind skipwhite
syn match kittyKeybind '\S\+' contained contains=kittyMod nextgroup=@kittyActionList skipwhite

" KW: mouse_map button-name event-type modes action
syn match kittyMouseMap '^mouse_map' display nextgroup=kittyButtonbind skipwhite
syn match kittyButtonbind '\S\+' contains=kittyMod contained nextgroup=kittyButtonEvent,kittyError skipwhite
syn keyword kittyButtonEvent press release doublepress triplepress click doubleclick contained nextgroup=kittyMouseModes,kittyError skipwhite
syn match kittyMouseModes '\%(un\)\?grabbed\%(,\%(un\)grabbed\)\?' contained nextgroup=@kittyActionList skipwhite
hi def link kittyMouseMap kittyMap
hi def link kittyButtonbind kittyKeybind
hi def link kittyButtonEvent Type
hi def link kittyMouseModes Type

" KW: env VAR=value
syn match kittyEnvKW '^env' nextgroup=kittyEnvIdent skipwhite transparent
syn match kittyEnvIdent '[[:alnum:]_]\+' contained nextgroup=kittyEnvAssign
syn match kittyEnvAssign '=' contained nextgroup=kittyEnvValue
syn match kittyEnvValue '.*' contained contains=kittyVar
hi def link kittyEnvIdent Identifier
hi def link kittyEnvAssign Operator

" KW: action_alias
syn match kittyActionAliasKW '^\%(action\|kitten\)_alias' nextgroup=kittyActionIdent skipwhite transparent
syn match kittyActionIdent '\k\+' contained
hi def link kittyActionIdent Identifier
