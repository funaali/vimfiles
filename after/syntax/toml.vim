" Syntax: toml

if !exists('b:current_syntax')
  finish
endif

" Apply vim highlighting rules for toml only inside specific directory
if expand('%:p:h') =~? '/.vim/dein'
  " Embed "vim" in """,""" blocks (for dein toml)
  call SyntaxRange#IncludeEx('start=+\(#.\{-}\)\@<!''\{3}+ms=e+1 end=+''\{3}+me=s-1 keepend containedin=tomlString contained', 'vim')
endif
