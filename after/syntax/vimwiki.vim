" Syntax: vimwiki
" Late customizations for the vimwiki filetype.

if !exists('b:current_syntax')
  finish
endif

highlight VimwikiMarkers cterm=NONE ctermfg=3 guifg=#b58900
highlight VimwikiHeader1 cterm=bold ctermfg=9 guifg=#cb4b16
highlight VimwikiHeader2 cterm=bold ctermfg=9 guifg=#cb4b16
highlight VimwikiHeader3 cterm=bold ctermfg=9 guifg=#cb4b16
highlight VimwikiHeader4 cterm=NONE ctermfg=3 guifg=#b58900
highlight VimwikiHeader5 cterm=NONE ctermfg=3 guifg=#b58900
highlight VimwikiHeader6 cterm=NONE ctermfg=3 guifg=#b58900
highlight VimwikiCode    cterm=NONE ctermfg=1 guifg=#dc322f
highlight VimwikiPre                ctermfg=6 guifg=#2aa198
highlight VimwikiHR                 ctermfg=3 guifg=#b58900
highlight link VimwikiList Type

" XXX work around taskwiki's after/syntax/vimwiki
setlocal foldmethod=expr

" This is often necessary (especially with nested syntaxes) on large bfile.
" XXX migcht slow down redras quite a bit.
syntax sync fromstart
