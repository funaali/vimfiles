" Syntax: snippets (UltiSnips)

if !exists('b:current_syntax')
  finish
endif

syn match snipSnippetBodyLeadingTabs /^\t\+/ contained containedin=snipLeadingSpaces
hi def link snipSnippetBodyLeadingTabs SpecialKey
