
if !exists('b:current_syntax')
  finish
endif

" TODO
"if !has_key(g:polyglot_is_disabled, 'haskell')

" Fix character literals in polyglot's haskell syntax
"syn match haskellChar /^'\([^\\]\|\\[^']\+\|\\'\)'/  contains=hsSpecialChar,hsSpecialCharError
syn match haskellChar /[^a-zA-Z0-9_']'\([^\\]\|\\[^']\+\|\\'\)'/ms=s+1,lc=1
