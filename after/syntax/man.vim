" Syntax: man

if !exists('b:current_syntax')
  finish
endif

" allow more characters in headers. Default : /^[a-z][a-z0-9& ,.-]*[a-z]$/
" TODO allow more, similar to custom iskeyword
syntax match manSectionHeading +^[a-z][a-z0-9& ,.-/]*[a-z]$+
