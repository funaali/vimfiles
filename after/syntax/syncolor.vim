" syncolor: syntax highlight customizations. See <url:vimhelp:syncolor>

function! s:do_solarized_flattened() abort "{{{
  highlight Folded cterm=none
  highlight FoldColumn       term=standout  ctermfg=10   ctermbg=0    guifg=#586e75 guibg=#002b36
  highlight SignColumn       term=standout  ctermfg=12   ctermbg=0    guifg=#839496 guibg=#073642
  highlight CursorLineNr     cterm=bold     ctermfg=3    ctermbg=0    guifg=#b58900 guibg=#073642
  " QFS
  highlight MySignMarks      cterm=NONE     ctermfg=4    ctermbg=0    guifg=#268bd2 guibg=#073642
  highlight MySignLonglines  cterm=NONE     ctermfg=3    ctermbg=0    guifg=#b58900 guibg=#073642
  highlight MySignDiffAdd    cterm=bold     ctermfg=2    ctermbg=0    guifg=#859900 guibg=#073642
  highlight MySignDiffDelete cterm=NONE     ctermfg=160  ctermbg=0    guifg=#d70000 guibg=#073642
  highlight MySignDiffChange cterm=bold     ctermfg=6    ctermbg=0    guifg=#2aa198 guibg=#073642
  highlight MySignWarning    cterm=NONE     ctermfg=1    ctermbg=0    guifg=#dc322f guibg=#073642

  " Search: highlighting with the "reverse" attribute somehow makes the cursor
  " disappear in the hilights.
  " Original: hi Search term=reverse cterm=reverse ctermfg=3 gui=reverse guifg=#b58900
  hi Search term=reverse cterm=none ctermfg=0 ctermbg=11 gui=none guibg=#b17e28 guifg=#073642
endfunction "}}}

function! s:do_solarized() "{{{
  if &t_Co < 256
    finish
  endif
  " SpecialKey: The default background is slightly too bright
  highlight SpecialKey                                            guibg=#0e313a
  highlight Todo             cterm=NONE ctermfg=198  ctermbg=NONE
  highlight Underlined       cterm=NONE ctermfg=67   ctermbg=NONE
  highlight LineNr           cterm=NONE ctermfg=30   ctermbg=NONE guifg=#008787 guibg=NONE
  highlight CursorLine       cterm=NONE ctermfg=NONE ctermbg=0    guifg=NONE    guibg=#073642
  highlight CursorLineNr     cterm=NONE ctermfg=30   ctermbg=0    guifg=#008787 guibg=#073642
  highlight SignColumn       cterm=NONE ctermfg=12   ctermbg=NONE guifg=#839496 guibg=NONE
  highlight FoldColumn       cterm=NONE ctermfg=12   ctermbg=0    guifg=#839496 guibg=#073642
  highlight Folded           cterm=bold ctermfg=11   ctermbg=NONE
  highlight MySignDiffAdd    cterm=bold ctermfg=28   ctermbg=0    guifg=#008700 guibg=#073642
  highlight MySignDiffDelete cterm=NONE ctermfg=160  ctermbg=0    guifg=#d70000 guibg=#073642
  highlight MySignDiffChange cterm=bold ctermfg=6    ctermbg=0    guifg=#2aa198 guibg=#073642
  highlight MySignWarning    cterm=NONE ctermfg=1    ctermbg=0    guifg=#dc322f guibg=#073642
endfunction "}}}

function s:do_mods() abort "{{{
  if get(g:,'colors_name') ==# 'flattened_dark'
    call s:do_solarized_flattened()
  elseif get(g:,'colors_name') ==# 'solarized' && &background ==# 'dark'
    call s:do_solarized()
  endif
endfunction "}}}

augroup myvimrc_syncolor
  autocmd!
  autocmd ColorScheme * call s:do_mods()
augroup END
