" File: ftdetect/ftdetect.vim

" Load Order:
" - this (ftdetect/)
" - plugins (~/.cache/vim/dein/.cache/vimrc/.dein/ftdetect/)
" - after files (after/ftdetect/)

" Note: :setfiletype will not change a previously detected filetype.
" Note: :set filetype=<ft> does override any previous filetype.
" Note: ftdetect/<ft>.vim files may overrule the default file type checks.
" Note: to set filetype based on file contents write a scripts.vim.
" Note: See vim-polyglot/ftdetect/polyglot.vim if using vim-polyglot.

augroup filetypedetect

  " <url:vimhelp:ft-mail-plugin>
  autocmd BufRead,BufNewFile *mutt-* setfiletype mail

  " help
  autocmd BufRead,BufNewFile */.vim/doc/*.txt setfiletype help

  " systemd
  autocmd BufRead,BufNewFile *.{link,network,netdev} setfiletype systemd
  autocmd BufRead,BufNewFile */systemd/*/*.d/*.conf  setfiletype systemd

  " envrc (direnv)
  autocmd BufRead,BufNewFile .envrc setfiletype sh

  " ansible
  autocmd BufRead,BufNewFile hosts setfiletype ansible_hosts

  " Bookmarks
  autocmd BufRead,BufNewFile NERDTreeBookmarks setfiletype nerdtreebookmarks

  " rtorrent.rc
  autocmd BufRead,BufNewFile {.,}rtorrent.rc setlocal ft=rtorrent
  autocmd BufRead,BufNewFile */rtorrent.d/*.rc{,.include} setlocal ft=rtorrent

  " kitty_config
  autocmd BufRead,BufNewFile kitty.conf,*/kitty/*.conf setfiletype kitty_config

  " Ansible
  autocmd BufRead,BufNewFile */*ansible*.y{a,}ml set ft=yaml.ansible
  autocmd BufRead,BufNewFile */{inventory,roles}/*.y{a,}ml set ft=yaml.ansible

  " Pipewire/wireplumber
  autocmd BufRead,BufNewFile */.config/pipewire/pipewire*.conf.d/*.conf setlocal ft=config
augroup END
