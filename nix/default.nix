{ nixpkgs
, pkgs
, system
, plugins ? ./plugins.nix
# per-plugin arguments
, plugin_name ? null
}:

let
  lib = nixpkgs.lib;

  inherit (builtins) typeOf;
  inherit (lib) seq assertMsg;

  addPackagesFrom = pkgset: x:
    if typeOf x == "string" then [ pkgset.${x} ]
    else if typeOf x == "list" then lib.flatten (map (addPackagesFrom pkgset) x)
    else if typeOf x == "function" || typeOf x == "lambda" then x pkgset
    else throw "Couldn't process packages from type: ${typeOf x}"
  ;

  depsComputed = lib.foldr
    ({ name, path, merged, expr }: deps:
      let
        packages' = expr.packages or [ ];
        python3' = expr.python3 or { };
        hooks' = expr.hook or { };
        unrecognized = lib.attrNames (removeAttrs expr [ "packages" "python3" "hook" ]);
        assertUnrecognized = assertMsg (unrecognized == [ ])
          "Unrecognized attributes in plugin ${name}: ${toString unrecognized}";
      in
      deps // seq assertUnrecognized ({
        plugins = deps.plugins // { ${name} = { inherit path merged; }; };
        packages = deps.packages ++ addPackagesFrom pkgs packages';
        python3.pkgs =
          if python3'?pkgs
          then ps: deps.python3.pkgs ps ++ addPackagesFrom ps python3'.pkgs
          else deps.python3.pkgs;
        hooks = deps.hooks // { ${lib.replaceStrings [ "-" ] [ "_" ] name} = hooks'; };
      }))
    {
      plugins = { }; # plugins = { {name} = [...] }
      packages = [ ];
      python3.pkgs = _: [ ];
      hooks = { }; # hooks.{plugin_name}.{hook}.{command}
    }
    (import plugins);

  pyEnv = pkgs.python3.withPackages depsComputed.python3.pkgs;

  requiredPackages = depsComputed.packages ++ [ pyEnv ];

  runtimeEnv = pkgs.buildEnv {
    name = "my-vim-extras";
    paths = requiredPackages;
  };

  # $1 : [path]
  plugin-setup-pkgs = lib.packagesFromDirectoryRecursive {
    callPackage = pkgs.callPackage;
    directory = ./plugin-setup;
#   '';
  };

in
rec {
  install = pkgs.writeShellScriptBin "install-runtime-env" ''
    ${./install-runtime-env} ${runtimeEnv} "$@"
  '';

  inherit runtimeEnv pyEnv depsComputed;

  runHook = pkgs.writeShellScriptBin "runHook" ''
    # plugin and hook names
    ${lib.toShellVar "plugin_hook_names"
      ((lib.filterAttrs
        (_: hooks: hooks != [])
        ((lib.mapAttrs (_: lib.attrNames) depsComputed.hooks))))}

    # The hook values
    ${lib.toShellVars depsComputed.hooks}

    for PLUG_NAME in ''${!plugin_hook_names[@]}; do
      PLUG_HOOKS=''${plugin_hook_names[$PLUG_NAME]}
      echo "Plugin: $PLUG_NAME. Hooks: $PLUG_HOOKS" >&2
      for HOOK_NAME in $PLUG_HOOKS; do
        eval "printf -v HOOK_SCRIPT %s \"\''${$PLUG_NAME[$HOOK_NAME]}\""
        echo "### Hook $PLUG_NAME/$HOOK_NAME:" >&2
        echo "$HOOK_SCRIPT" >&2
      done
    done
  '';

  # $1 : [path]
  plugin-setup = pkgs.writeShellScriptBin "plugin-setup" (
      if plugin-setup-pkgs ? ${plugin_name} then
        ''${lib.getExe plugin-setup-pkgs.${plugin_name}} "$@"''
      else "echo >&2 no setup: ${plugin_name}"
  );

   inherit plugin-setup-pkgs;
}
