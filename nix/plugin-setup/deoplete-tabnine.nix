# deoplete-tabnine
{ writeShellApplication, ... }:

writeShellApplication {
  name = "plugin-setup-deoplete-tabnine";
  text = ''
    set -x
    path=''${1:?path positional argument is required}
    cd "$path"

    exit 1

    echo READY
  '';
}
