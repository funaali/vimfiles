[
 { name = "hug-neovim-rpc"; path = "/home/sim/.cache/vim/dein/repos/github.com/roxma/vim-hug-neovim-rpc"; merged = 1; expr = 
    { python3.pkgs = ps: [ps.pynvim ps.greenlet]; };
 }
 { name = "yarp"; path = "/home/sim/.cache/vim/dein/repos/github.com/roxma/nvim-yarp"; merged = 1; expr = 
    { python3.pkgs = ps: [ps.pynvim ps.greenlet]; };
 }
 { name = "deoplete-tabnine"; path = "/home/sim/.cache/vim/dein/repos/github.com/tbodt/deoplete-tabnine"; merged = 0; expr = 
    { hook.install = ''
       echo Install the latest tabnine binary at path:
       echo "{plugin.path}/binaries/{version}/x86_64-unknown-linux-musl/TabNine"
       echo "Check configuration too!"
       exit 1
      '';
    };
 }
 { name = "taskwiki"; path = "/home/sim/.cache/vim/dein/repos/github.com/tbabej/taskwiki"; merged = 1; expr = 
    { python3.pkgs = ["tasklib"]; };
 }
 { name = "vimwiki-utils"; path = "/home/sim/.cache/vim/dein/repos/github.com/vimwiki/utils"; merged = 1; expr = 
    { packages = ["vimwiki-markdown"]; python3.pkgs = ["packaging"]; };
 }
 { name = "cpsm"; path = "/home/sim/.cache/vim/dein/repos/github.com/nixprime/cpsm"; merged = 1; expr = 
    { hook.install = ''
        echo Take care of cpsm module installation:
        echo cd {g:dein#plugin.path}
        echo PY3=ON ./install.sh
        echo cp build/cpsm_py.so bin
        exit 1
      '';
    };
 }
 { name = "fruzzy"; path = "/home/sim/.cache/vim/dein/repos/github.com/raghur/fruzzy"; merged = 0; expr = 
    { hook.install = "echo take care of fruzzy#install() - maybe use nixpkgs.vimPlugins.fruzzy instead; exit 1"; };
 }
 { name = "deoplete-clang"; path = "/home/sim/.cache/vim/dein/repos/github.com/deoplete-plugins/deoplete-clang"; merged = 0; expr = 
    { python3.pkgs = ps: [ps.jedi]; };
 }
 { name = "deoplete"; path = "/home/sim/.cache/vim/dein/repos/github.com/Shougo/deoplete.nvim"; merged = 1; expr = 
    { python3.pkgs = ps: [ps.pynvim ps.msgpack]; };
 }
 { name = "hexokinase"; path = "/home/sim/.cache/vim/dein/repos/github.com/RRethy/vim-hexokinase"; merged = 0; expr = 
    { hook.update = "echo take care of build hexokinase; exit 1"; };
 }
 { name = "LeaderF"; path = "/home/sim/.cache/vim/dein/repos/github.com/Yggdroot/LeaderF"; merged = 0; expr = 
    { hook.install = "echo take care of ~/.cache/vim/dein/repos/github.com/Yggdroot/LeaderF/install.sh; exit 1"; };
 }
 { name = "syntastic"; path = "/home/sim/.cache/vim/dein/repos/github.com/scrooloose/syntastic"; merged = 0; expr = 
    {
      packages = pkgs: let
        vint-latest = pkgs.vim-vint.overrideAttrs (oa: rec {
          name = "${oa.pname}-${version}";
          version = "0.4.0+94d2c";
    
          src = pkgs.fetchFromGitHub {
            owner = "Vimjas";
            repo = "vint";
            rev = "94d2cb3fd9526a89911b7c1e083a1fd78bace729";
            hash = "sha256-5dxrqE8gpgyAwgjK76Whu5hrNFjtsqctqPyme85g8ro=";
          };
    
          nativeBuildInputs = oa.nativeBuildInputs or [] ++ (with pkgs.python3.pkgs; [
            setuptools-scm
            pip
          ]);
    
          nativeCheckInputs = oa.nativeCheckInputs or [] ++ (with pkgs.python3.pkgs; [
            pytestCheckHook
          ]);
    
          doCheck = true;
    
          preCheck = null;
    
          disabledTests = [ "test_exec_vint_with_pipe" ];
        });
       in [ vint-latest ];
    };
 }
 { name = "unicode"; path = "/home/sim/.cache/vim/dein/repos/github.com/chrisbra/unicode.vim"; merged = 0; expr = 
    {
      hook.update = ''
       Install the data file
       from 'https://www.unicode.org/Public/UNIDATA/UnicodeData.txt'
       as "{vimDataDir}/UnicodeData.txt"
      '';
    };
 }
]
