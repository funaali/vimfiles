
" NOTE: The balloon is displayed only if the cursor is on a text
" character.  If the result of evaluating 'balloonexpr' is not empty,
" Vim does not try to send a message to an external debugger (Netbeans
" or Sun Workshop).

function! MyBalloonExpr()
  return 'Cursor is at line ' . v:beval_lnum .
      \', column ' . v:beval_col .
      \ ' of file ' .  bufname(v:beval_bufnr) .
      \ ' on word "' . v:beval_text . '"'
endfunction
"let g:OldBalloonExpr = &bexpr
set bexpr=MyBalloonExpr()
