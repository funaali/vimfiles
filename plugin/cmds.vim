" File: .vim/plugin/cmds.vim

" Execute: execute lines in current buffer
command! -range Execute execute '<mods>' join(getbufline('', <line1>, <line2>), "\n")


" Align:                                                                  {{{1
command! RAlign call vimrc#ralign()


" Indentation:                                                            {{{1
command! IndentInfo
      \ call vimrc#indent_info()

command! -range=%
      \ IndentCheck
      \ exe '<line1>,<line2>g/\v^%(%(\t|\s{'.&l:sw.'})+)@>\s/echo expand("%") line(".")'

command! -range=% -nargs=+ -complete=command
      \ IndentCheckDo
      \ exe '<line1>,<line2>g/\v^%(%(\t|\s{'.&l:sw.'})+)@>\zs\s*/' . <q-args>


" Highlight:                                                              {{{1

" Note: "match none" clears previous highlight

command! HighlightIndentErrors
      \ highlight default link indentError Error |
      \ exe 'match indentError /\v^%(%(\t|\s{'.&l:sw.'})+)@>\zs\s+/'

command!
      \ HighlightLine
      \ exe 'match Search /\%'.line('.').'l/'

nnoremap <silent> <Leader>hl :<C-U>HighlightLine<CR>
nnoremap <silent> <Leader>hi :<C-U>HighlightIndentErrors<CR>

" Debug:                                                                  {{{1

command! Syn
      \ call s:showCursorSyn()

function! s:showCursorSyn() abort
  let stack = synstack(line('.'), col('.'))
  let hl = synIDattr(synIDtrans(get(stack, -1)), 'name')
  if !empty(stack)
    exe 'syn list' join(map(copy(stack), {_, id -> synIDattr(id, 'name')}))
    echohl Title | echo '--- Highlight ---' | echohl None
  endif
  exe 'hi' (empty(hl)?'Normal':hl)
endfunction

command! DiffOrig
      \ vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis

command! LogAutocmds
      \ call logautocmds#toggle()

command! -nargs=+ -complete=dir
      \ Glob call s:do_glob(<f-args>)

function! s:do_glob(...)
  for str in a:000
    for path in globpath(&rtp, str, 0, 1, 1)
      echo fnamemodify(path, ':~')
    endfor
  endfor
endfunction

" Dein:                                                                   {{{1

command! -nargs=* -complete=customlist,s:dein_plugins DeinShow      call s:dein_show([<f-args>])
command! -nargs=* -complete=customlist,s:dein_plugins DeinUpdate    call dein#update([<f-args>])
command! -nargs=* -complete=customlist,s:dein_plugins DeinReInstall call dein#reinstall([<f-args>])

command! DeinInstall if dein#check_install() > 0 | call dein#install() | endif
command! DeinLog execute 'edit' fnameescape(g:dein#install_log_filename)
command! DeinCleanReCache
      \ call dein#clear_state() | call dein#recache_runtimepath()

function! s:dein_show(plugins) abort                                      "{{{
  for name in a:plugins
    echo printf('Plugin: %s', name)
    for [k,v] in items(dein#get(name))
      echo printf('  %s: %s', k, v)
    endfor
  endfor
endfunction "}}}

function! s:dein_plugins(A,L,P) abort                                     "{{{
  let names = keys(dein#get())
  if !empty(a:A)
    call filter(names, printf('v:val =~? "%s"', escape(a:A,'"\')))
  endif
  return names
endfunction "}}}

" Misc:                                                                   {{{1

" Edit configuration files in a new tabpage.
command! -nargs=* -complete=customlist,s:vimfiles_complete
      \ EConfig
      \ call s:vimfiles(<q-args>, <q-mods>)

fun! s:vimfiles_complete(A,L,P)
  let l:prefix = fnamemodify($MYVIMRC,':p:h')
  let l:files  = globpath(fnameescape(l:prefix), '**'.a:A.'**', 0, 1)
  call map(l:files, 'v:val['.(len(l:prefix)+1).':]')
  call filter(l:files, 'v:val !~# ".snippets"')
  return l:files
endfun

function! s:vimfiles(files, mods)
  let l:prefix = fnamemodify($MYVIMRC,':p:h')
  arglocal | %argdelete
  for f in expand(a:files, 0, 1)
    exe 'argadd' l:prefix.'/'.f
  endfor
  tabnew | exe a:mods 'vert' 'sall!' 4
endfunction

function! MyGoto(type, what) abort "{{{
  if a:type ==? 'f'
    try
      let [filepath, lnum] = matchlist(get(split(execute('verbose function ' . a:what), '\n'), 1, ''), 'from \(.*\) line \(\d*\)$')[1:2]
    catch /E123:/
      echoerr 'No such function: ' . a:what
      return
    endtry
  else
    echoerr 'Unrecognized type: ' . a:type
    return
  endif
  if !empty(filepath)
    execute 'edit' filepath
    execute 'normal!' lnum.'G'
  else
    echomsg 'Definition not found: ' . a:what
  endif
endfunction "}}}

command! -nargs=1 -complete=function GotoFunction call MyGoto('f', <f-args>)

command! -nargs=1 ApplyCursor call init#set_cursor(<f-args>)
