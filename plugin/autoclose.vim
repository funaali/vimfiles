" File: plugin/autoclose.vim
" Description: Automatically close windows under certain conditions.

if exists('g:loaded_myvimrc_autoclose')
  finish
endif
let g:loaded_myvimrc_autoclose = 1

let s:conditions = [ ['&bt', 'quickfix'], ['&ft', '\v^%(nerdtree|defx)$'] ]

"function! s:autoclose()
"  " Do nothing if there are at least two windows
"  if winnr('$') !=# 1
"    return
"  endif
"  let l:bufnr = winbufnr(winnr())
"  for [l:var, l:pat] in s:conditions
"    if match(getbufvar(l:bufnr, l:var), l:pat) >= 0
"      quit
"    endif
"  endfor
"endfunction

"function! s:qf_autoclose() "{{{
"  " When closing a window whose location-list is displayed in a window, close
"  " the location-list window, too.
"  let abuf = expand('<abuf>')
"  let ft = getbufvar(abuf, '&ft')
"  let winIds = win_findbuf(abuf)
"  let ll = getloclist(awin, {'filewinid':0,'qfbufnr':0,'winid':0})
"  if ll.winid > 0 && ll.qfbufnr != abuf
"    exe win_id2win(ll.winid) . 'close'
"  endif
"endfunction "}}}
"
"function! s:tagbar_autoclose() "{{{
"  if !exists('t:tagbar_autoclose') || !exists('*tagbar#inspect')
"    return
"  endif
"  let buf = expand('<buf>')
"  if bufwinnr(get(t:,'tagbar_buf_name')) > 0
"    if &l:ft !=# '' && &l:ft !=# 'tagbar' && empty(get(tagbar#inspect('known_types'),&l:ft))
"      call tagbar#CloseWindow()
"    endif
"  endif
"  unlet! s:tagbar_autoclose
"endfunction "}}}

"augroup myvimrc_autoclose
"  autocmd!
"  " Location-list autoclose
"  autocmd BufWinLeave * call s:qf_autoclose()
"  " Tagbar autoclose
"  autocmd FileType,FileReadPost *
"        \ if expand('<amatch>') !~ '__Tagbar__.*\|tagbar' | let t:tagbar_autoclose = expand('<abuf>') | endif
"        "\ if expand('<amatch>') !~ '__Tagbar__.*\|tagbar' | let t:tagbar_autoclose = 1 | endif
"  autocmd BufWinLeave * call s:tagbar_autoclose()
"  "autocmd SafeState * call s:tagbar_autoclose()
"augroup END
