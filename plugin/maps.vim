" File: ~/.vim/plugin/maps.vim
scriptencoding utf-8

" Plugin Maps: various commands and mappings that extend one or more plugin.
if !exists('*dein#get')
  finish
endif

" Misc. {{{
" Plugin: operator-surround
" Usage: "{command}{motion}{block}"
" Add: surrounding block: "saiw*" replaces "word" with "*word*"
" Delete: surrounding block: "sda(" removes surrounding parantheses "(...)"
" Replace: surrounding block: "sra<{" replaces "<foobar>" with "{foobar}"
map <silent> sa <Plug>(operator-surround-append)
map <silent> sd <Plug>(operator-surround-delete)
map <silent> sr <Plug>(operator-surround-replace)
" This overwrites vim default mapping S:
map <silent> S  <Plug>(operator-surround-replace)

" Plugin: operator-replace
" Paste over operand
map <silent> gr <Plug>(operator-replace)

" Plugin: space (contiguous areas of whitespace)
omap <silent> <Space>  <Plug>(inner_space)
xmap <silent> <Space>  <Plug>(inner_space)
omap <silent> a<Space> <Plug>(around_space)
xmap <silent> a<Space> <Plug>(around_space)

" Plugin: zoomwintab
nnoremap <silent> <C-W>z     <Cmd>ZoomWinTabToggle<CR>
nnoremap <silent> <C-W><C-Z> <Cmd>ZoomWinTabToggle<CR>

" Plugin: tagbar
nnoremap <leader>st <Cmd>TagbarToggle<CR>

" Plugin: Undotree
nnoremap <leader>su  <Cmd>UndotreeToggle<CR>
" Close all auxiliary windows
nnoremap <leader>s! <Cmd>exe 'silent! TagbarClose' \| exe 'silent! NERDTreeClose' \| pclose \| helpclose \| windo lclose<CR><Cmd>wincmd p<CR>
" }}}

" Folds mappings                                                          {{{
" Plugin: origami + custom
nmap     <silent> ZD <Plug>DeleteFoldmarker
nmap     <silent> Za <Plug>AlignFoldmarkers
nmap     <silent> ZA <Plug>AlignFoldmarkersAll
nmap     <silent> Zf <Plug>SetFoldmarkerOpenNocomment
nmap     <silent> ZF <Plug>SetFoldmarkerOpenComment
nmap     <silent> Zc <Plug>SetFoldmarkerCloseNocomment
nmap     <silent> ZC <Plug>SetFoldmarkerCloseComment
" Plugin: FastFold
nmap     <silent> ZU <Plug>(FastFoldUpdate)
xnoremap          iz :<c-u>FastFoldUpdate<cr><esc>:<c-u>normal! ]zv[z<cr>
xnoremap          az :<c-u>FastFoldUpdate<cr><esc>:<c-u>normal! ]zV[z<cr>
" Plugin: fold-cycle
nmap     <silent> <C-Tab>   <Plug>(fold-cycle-open)
nmap     <silent> <C-S-Tab> <Plug>(fold-cycle-close)
" Custom:
xmap     <silent> z> <Plug>(fold-renumber-inc)
xmap     <silent> z< <Plug>(fold-renumber-dec)
nmap     <silent> Z> <Plug>(fold-increase)
nmap     <silent> Z< <Plug>(fold-decrease)
noremap  <silent> <Plug>(fold-renumber-inc) :call vimrc#fold_renumber(1)<CR>
noremap  <silent> <Plug>(fold-renumber-dec) :call vimrc#fold_renumber(-1)<CR>
nmap     <silent> <Plug>(fold-increase)     V<Plug>(textobj-fold-i)<Plug>(fold-renumber-inc)
nmap     <silent> <Plug>(fold-decrease)     V<Plug>(textobj-fold-i)<Plug>(fold-renumber-dec)
"}}}

" Denite mappings                                                        {{{
" Plugin: bookmarks
nmap <silent> <Leader>m <Plug>BookmarkToggle
nmap <silent> <Leader>a <Plug>BookmarkAnnotate
" Denite: bookmarks
nnoremap <leader>gm <Cmd>Denite bookmark<CR>
" Denite: buffer/file
"nnoremap <Leader>gg <Cmd>DeniteBufferDir buffer file<CR>
"nnoremap <Leader>gf <Cmd>DeniteBufferDir file<CR>
"nnoremap <Leader>gb <Cmd>Denite buffer -immediately-1<CR>
" Denite: line
"nnoremap <leader>gl <Cmd>Denite line<CR>
" Denite: mark
nnoremap <leader>g' <Cmd>Denite mark<CR>
" Denite: register
nnoremap <Leader>gr <Cmd>Denite register<CR>
" Denite: tag
"nnoremap <leader>gt <Cmd>Denite tag<CR>
" Denite: jump
"nnoremap <leader>gj <Cmd>Denite jump<CR>
" Denite: command_history
"nnoremap <leader>g: <Cmd>Denite command_history -reversed -winheight=8<CR>
"}}}

" Utl mappings                                                            {{{1
nnoremap <leader>o   <Cmd>Utl openLink underCursor edit<CR>
nnoremap <leader>Ot  <Cmd>Utl openLink underCursor tabedit<CR>
nnoremap <leader>Os  <Cmd>Utl openLink underCursor split<CR>
nnoremap <leader>Ov  <Cmd>Utl openLink underCursor vsplit<CR>

vnoremap <leader>o   <Cmd>Utl openLink visual edit<CR>
vnoremap <leader>Ot  <Cmd>Utl openLink visual tabedit<CR>
vnoremap <leader>Os  <Cmd>Utl openLink visual split<CR>
vnoremap <leader>Ov  <Cmd>Utl openLink visual vsplit<CR>

nnoremap <leader>yl  <Cmd>Utl copyLink underCursor<CR>
nnoremap <leader>yf  <Cmd>Utl copyFileName currentFile<CR>

vnoremap <leader>yl  <Cmd>Utl copyLink visual<CR>
"}}}
