" ~/.vim/plugin/leaderf.vim

scriptencoding utf-8

" TODO: gutentags
" https://github.com/freehaha/Leaderf-projects

let g:Lf_TabpagePosition = 3 " put newly opened tap page as last one

let g:Lf_UseCache                = 1
let g:Lf_CacheDirectory          = init#get_directory('leaderf','cache')
let g:Lf_FollowLinks             = 1 " traverse symlinks
let g:Lf_ShowHidden              = 1
let g:Lf_ShowRelativePath        = 1
let g:Lf_IgnoreCurrentBufferName = 1
let g:Lf_UseVersionControlTool   = 1
let g:Lf_WildIgnore = {
      \ 'dir' : ['.svn', '.git', '.hg'],
      \ 'file': ['*.sw?', '~$*', '*.o', '*.so', '*.bin'],
      \}

" MRU:
let g:Lf_MruFileExclude = ['*.so'] " Files LeaderF should not record
let g:Lf_MruMaxFiles    = 8000

" TAG:
let g:Lf_PreviewCode = 1

" RG:
" LeaderF-rg-wiki <url:https://github.com/Yggdroot/LeaderF/wiki/Leaderf-rg>
" regex-syntax    <url:https://docs.rs/regex/1.5.4/regex/#syntax>
" glob-syntax     <url:https://docs.rs/globset/0.3.0/globset/#syntax>
let g:Lf_RgStorePattern = 'g' " Register to store the search pattern upon exit.
let g:Lf_RgConfig = [
    \ '--max-columns=150',
    \ '--hidden',
    \ ]
"  \ '--glob=!git/*',
"  \ '--type-add web:*.{html,css,js}*',

" History:
"g:Lf_HistoryNumber = 100
"let g:Lf_HistoryExclude = {
"      \ 'cmd': [],
"      \ 'search': []
"      \}

" Working Directory:                                                      {{{1
" Option 1: some heuristics
let g:Lf_RootMarkers = [ '.git', 'flake.nix' ]
let g:Lf_WorkingDirectoryMode = 'AF'

" Option 2: set it explicitly
"let g:Lf_WorkingDirectory = finddir('.git', '.;')

" Mappings:                                                               {{{1

" Prompt: <url:vimhelp:leaderf-prompt>
let g:Lf_CommandMap = {
      \'<C-K>' : ['<C-K>','<Up>'  ],
      \'<C-J>' : ['<C-J>','<Down>'],
      \'<Up>'  : ['<C-P>'],
      \'<Down>': ['<C-N>'],
      \'<Tab>' : ['<Tab>','<Esc>'],
      \'<C-]>' : ['<C-]>','<C-V>'],
      \'<C-X>' : ['<C-X>','<C-_>'],
      \}

" Normal Mode:
let g:Lf_NormalCommandMap = {
    \ "*":      {
    \              "<Down>": "j",
    \              "<Up>"  : "k",
    \           },
    \}

" LeaderF Window:                                                         {{{1
let g:Lf_HideHelp              = 1 " don't show the help in normal mode
let g:Lf_PopupShowFoldcolumn   = 0
let g:Lf_WindowPosition        = 'popup'
let g:Lf_PopupHeight           = 0.65
let g:Lf_PopupWidth            = 0.80
let g:Lf_PopupAutoAdjustHeight = 1
let g:Lf_PopupPreviewPosition  = 'bottom'
let g:Lf_PreviewInPopup        = 1

let g:Lf_PreviewResult = {
      \ 'File': 1,
      \ 'Buffer': 1,
      \ 'Mru': 1,
      \ 'Tag': 1,
      \ 'BufTag': 1,
      \ 'Function': 1,
      \ 'Line': 1,
      \ 'Colorscheme': 0,
      \ 'Rg': 0,
      \ 'Gtags': 1
      \}

" Statusline:                                                             {{{1
let g:Lf_StlSeparator = { 'left': "\ue0b0", 'right': "\ue0b2", 'font': 'DejaVu Sans Mono for Powerline' }
"let g:Lf_StlColorscheme = 'powerline'
"let g:Lf_StlPalette = g:leaderf#colorscheme#gruvbox_material#palette

" Mappings:                                                               {{{1
let g:Lf_ShortcutF = '' " disable default maps
let g:Lf_ShortcutB = ''


" NOTE: Append <Tab> to the command to switch to the normal mode
" automatically for desired commands.

" File: gf - file in current tabpage's directory
noremap <expr> <leader>gf printf('<Cmd>LeaderfFile %s<CR>',fnameescape(getcwd(-1,0)))
" gF - file in current file's directory
noremap <leader>gF <Cmd>LeaderfFile %:h<CR>
" MRU: gg fm
noremap <leader>gg <Cmd>Leaderf mru<CR>
noremap <leader>fm <Cmd>Leaderf mru<CR>

" Git: vl
noremap <leader>vl <Cmd>Leaderf git log --graph --quick-select<CR><Tab>
noremap <leader>vd <Cmd>Leaderf git diff --cached<CR><Tab>
noremap <leader>vb <Cmd>Leaderf git blame<CR><Tab>

" Buffer: gb
noremap <leader>gb <Cmd>LeaderfBufferAll<CR>
" Tag: gt ft
noremap <leader>gt <Cmd>LeaderfBufTagAllCword<CR>
noremap <leader>ft <Cmd>Leaderf bufTag<CR>
" Line: gl fl
noremap <leader>gl <Cmd>LeaderfLine<CR>
noremap <leader>fl <Cmd>Leaderf line<CR>
" Cmd: g:
noremap <leader>g: <Cmd>LeaderfHistoryCmd<CR>
" Shist: g/
noremap <leader>g/ <Cmd>Leaderf searchHistory --cword<CR>
" Jumps: gj
noremap <leader>gj <Cmd>Leaderf jumps<CR>
" LLQF: gC g]
noremap <leader>g] <Cmd>Leaderf loclist<CR>
noremap <leader>gC <Cmd>Leaderf quicklist<CR>
" Help: gh gH
noremap <leader>gh <Cmd>LeaderfHelp<CR>
noremap <leader>gH <Cmd>LeaderfHelpCword<CR>
" Rg:
" gG - rg prompt
map     <leader>gG <Plug>LeaderfRgPrompt<CR>
" ^B - in current buffer for cursor word
noremap <C-B> :<C-U><C-R>=printf("Leaderf! rg --current-buffer -e %s ", expand("<cword>"))<CR>
" ^F - in * for cursor word
noremap <C-F> :<C-U><C-R>=printf("Leaderf! rg -e %s ", expand("<cword>"))<CR>
"  G - search visually selected text literally
xnoremap <leader>G :<C-U><C-R>=printf("Leaderf! rg -F -e %s ", leaderf#Rg#visual())<CR>
" go - recall old search
noremap <leader>go <Cmd>Leaderf! rg --recall<CR>

" TODO WIP                                                                {{{1
" <url:https://github.com/Yggdroot/LeaderF/blob/master/autoload/leaderf/python/leaderf/anyExpl.py#L20>

if !exists('g:Lf_Extensions')
	let g:Lf_Extensions = {}
endif

function! Lf_manSource(...) abort "{{{
   return man#completion#run(0,0,0)
endfunction "}}}

function! Lf_manAccept(line, args) abort "{{{
  execute 'Man ' . a:line
endfunction "}}}

let g:Lf_Extensions.man = {
			\ 'source': 'Lf_manSource',
			\ 'accept': 'Lf_manAccept',
			\ 'highlights_def': {
			\ 'Lf_hl_manTitle': '^.\+\ze(',
			\ 'Lf_hl_manNumber': '(.\+)',
			\ },
			\ 'highlights_cmd': [
			\ 'hi link Lf_hl_manTitle Title',
			\ 'hi link Lf_hl_manNumber Number',
			\ ],
			\ }

function! Lf_reg_source(args) abort                                       "{{{
  return execute('registers')->split('\n')
endfunction "}}}
"
function! Lf_reg_accept(line, args) abort                                 "{{{
  "echomsg "ok"
endfunction "}}}

le: g:Lf_Extensions = get(g:, 'Lf_Extensions', {})
let g:Lf_Extensions.register = {
  \ 'source': 'Lf_reg_source',
  \ 'accept': 'Lf_reg_accept',
  \ }
  "\ 'preview': string(function('lf_floaterm#preview'))[10:-3],
