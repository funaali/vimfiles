" Automatically reload file if changed somewhere else

if exists('g:loaded_myvimrc_autoreload')
  finish
endif
let g:loaded_myvimrc_autoreload = 1

augroup myvimrc_file_autoreload
  au!
  au CursorHold * silent! checktime
augroup END
