" Description: FileChangedShell handling
" File: plugin/fcs.vim

" NOTES
" - Triggered after executing a shell command.
" - Also triggered with a |:checktime| command, or when gvim regains input
"   focus.
" - Not used when 'autoread' is set and the buffer was not changed.
" - If a FileChangedShell autocommand is present the warning message and
"   prompt is not given.

function! FCSHandler(file, buf) " {{{
  let msg = printf('[FCS] %2i  %s', a:buf, fnamemodify(a:file, ':~:S'))
  let v:fcs_choice = ''
  if v:fcs_reason ==# 'deleted'
    let msg .= " no longer available - 'modified' set"
    call setbufvar(a:buf, '&modified', '1')
  elseif v:fcs_reason ==# 'changed'
    let msg .=  'contents changed'
    let v:fcs_choice = 'ask'
  elseif v:fcs_reason ==# 'mode'
    let msg .=  'permissions changed - reloaded'
    let v:fcs_choice = 'reload'
  elseif v:fcs_reason ==# 'time'
    let msg .=  'timestamp changed'
  elseif v:fcs_reason ==# 'conflict'
    let msg .= ' -- CONFLICT -- is modified, but was changed outside Vim'
    let v:fcs_choice = 'ask'
    echohl Error
  else " unknown values (future Vim versions?)
    let msg .=  '-- UNKNOWN -- reason=' . v:fcs_reason
    let v:fcs_choice = 'ask'
    echohl Error
  endif
  echomsg msg
  echohl None
endfunction " }}}

augroup myvimrc_fcs
  autocmd!
  autocmd FileChangedShell * call FCSHandler( expand('<afile>:p'), expand('<abuf>') )
  "XXX: FileChangedShellPost
augroup END
