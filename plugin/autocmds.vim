" File: ~/.vim/plugin/autocmds.vim

scriptencoding utf-8

augroup myvimrc_autocmds
  au!

  " On leaving insert mode, unset paste mode.
  au InsertLeave * if &paste | set nopaste | endif

  " Do init of new colorscheme
  au ColorScheme * nested call init#on('ColorScheme')

  " Perform (again) filetype detection after a write if filetype is not yet known.
  au BufWritePost * nested call init#redetect_filetype()

  " Buffer not backed with a file is removed when hidden.
  if !init#is_pager()
    au BufWinEnter * if empty(expand('<afile>')) | setlocal bufhidden=delete | endif
  endif

  " Suppress "File has been edited" warning when starting to edit read-only file.
  au FileChangedRO * setl noro

  if v:version > 800 " uses ++once
    " Insert a skeleton (header) when a buffer of a new file is loaded.
    " This relies on UltiSnips. When snippet with trigger 'header' is in scope
    " when a new file is opened, it is automatically inserted and expanded.
    au BufNewFile *? ++nested
          \ au FileType <buffer=abuf> ++once call vimrc#insert_skel_snippet('header')
  endif
augroup END

" cursor{column,line}                                                      {{{1
augroup myvimrc_cursorcolumn
  au!
  au FileType * if (expand('<amatch>') =~# 'help\|nerdtree\|tagbar') || (!empty(&l:buftype))
        \ | let b:cursorcolumn_enabled = 0
        \ | setlocal nocursorcolumn
        \ | else | setlocal cursorcolumn | endif
  au WinLeave * if get(b:,'cursorcolumn_enabled',get(g:,'cursorcolumn_enabled')) | setlocal nocursorcolumn | endif
  au WinEnter * if get(b:,'cursorcolumn_enabled',get(g:,'cursorcolumn_enabled')) | setlocal cursorcolumn   | endif
augroup END "}}}1

augroup myvimrc_cursorline                                                "{{{1
  au!
  au FileType    * if !exists('b:cursorline_enabled') &&  &l:cursorline | let b:cursorline_enabled = 1 | endif
  au InsertEnter * if get(b:,'cursorline_enabled') | setlocal nocursorline | endif
  au InsertLeave * if get(b:,'cursorline_enabled') | setlocal cursorline   | endif
  "au WinLeave,InsertEnter * set nocursorline
  "au WinEnter,InsertLeave * set cursorline
augroup END "}}}1

function! s:handle_resize(flag) "{{{
  if a:flag != 1 && !exists('#myvimrc_resize#VimResized')
    au myvimrc_resize VimResized * call s:handle_resize(1)
  endif
  " Only just initialized
  if a:flag == -1
    return
  endif
  " Not supported when a popup is active
  if win_gettype() ==# 'popup'
    return
  endif
  " Ok state to run
  if empty(state('S'))
    wincmd =
  " Post-pone
  elseif a:flag == 1
    au! myvimrc_resize
    au! myvimrc_resize SafeState * call s:handle_resize(0)
  endif
endfunction

augroup myvimrc_resize
augroup END

call s:handle_resize(-1)
"}}}

function! s:adjust_qf(minheight, maxheight) abort "{{{
  let l = 1
  let n_lines = 0
  let w_width = winwidth(0)
    " || n_lines < a:maxheight
  while l <= line('$')
      if line('$') >= a:maxheight
        let n_lines = a:maxheight
        break
      endif
        " number to float for division
       let l_len = strlen(getline(l)) + 0.0
       let line_width = l_len/w_width
       let n_lines += float2nr(ceil(line_width))
       let l += 1
   endw
   let n_height = max([min([n_lines, a:maxheight]), a:minheight])
   call setwinvar(0, '&winheight', n_height)
   exe n_height . 'wincmd _'
endfunction "}}}

augroup myvimrc_adjust_qf_height                                          "{{{1
  au!
  au FileType qf call s:adjust_qf(3, 10)
augroup END
