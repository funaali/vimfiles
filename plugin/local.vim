" ~/.vim/plugin/local.vim

if exists('g:loaded_myvimrc_local')
  finish
endif
let g:loaded_myvimrc_local = 1

let g:author_name     = 'Samuli Thomasson'
let g:author_email    = 'samuli.thomasson@paivola.fi'
let g:default_license = 'WTFPL'
