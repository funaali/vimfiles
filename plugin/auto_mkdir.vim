if exists('g:did_myvimrc_auto_mkdir')
  finish
endif
let g:did_myvimrc_auto_mkdir = 1

" Create missing parent directories when writing the buffer of a new file
" for the first time.
" Note: Must use '%' instead of <afile>, because the buffer name may be
" changed between the BufNewFile and BufWritePre events.

augroup myvimrc_auto_mkdir
  autocmd! BufNewFile *?
        \ autocmd myvimrc_auto_mkdir BufWritePre <buffer=abuf>
        \ call s:mkdir( expand('<abuf>'), expand('%:p:h') )
augroup END

function! s:mkdir(buf, dir) abort
  execute 'autocmd! myvimrc_auto_mkdir * <buffer='.a:buf.'>'
  " TODO handle the case if we should escalate to superuser in order to write
  " the path
  if !isdirectory(a:dir)
    silent call mkdir(a:dir, 'p')
  endif
endfunction
