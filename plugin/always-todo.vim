" HACK NOTE XXX FIXME todo TODO REVIEW etc.

if exists('g:loaded_always_todo')
  finish
endif
let g:loaded_always_todo = 1

function! s:update() abort "{{{
  syntax cluster MyCommentAny add=..*Comment
  syntax keyword MyTodo containedin=@MyCommentAny contained HACK[S] NOTE[S] XXX FIXME todo TODO REVIEW
  highlight default link MyTodo Todo
endfunction "}}}

augroup myvimrc_always_todo
  autocmd!
  autocmd FileType *? call s:update()
  autocmd FileType *? autocmd! myvimrc_always_todo Syntax <buffer=abuf> call s:update()
augroup END

command! AlwaysTodoRefresh call s:update()
