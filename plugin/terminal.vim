
" Solarized dark
let g:terminal_ansi_colors = [
      \ '#073642',
      \ '#dc322f',
      \ '#859900',
      \ '#b58900',
      \ '#268bd2',
      \ '#d33682',
      \ '#2aa198',
      \ '#eee8d5',
      \ '#002b36',
      \ '#cb4b16',
      \ '#586e75',
      \ '#657b83',
      \ '#839496',
      \ '#6c71c4',
      \ '#93a1a1',
      \ '#fdf6e3']

" NOTE: use "TerminalWinOpen" for non-hidden terminals and "TerminalOpen" for
" hidden terminals.
augroup myvimrc_terminal
  au!
  au TerminalWinOpen * if &buftype == 'terminal'
        \ | setlocal bufhidden=hide colorcolumn=
        \ | endif

  "au TerminalOpen * call setbufvar(expand('<abuf>')->str2nr()
  "      \ '&termwinscroll', 1000)
  "au TerminalOpen * exe printf(
  "  \    'au BufWinEnter <buffer=%d> ++once setlocal colorcolumn=%d',
  "  \       expand('<abuf>')->str2nr(), 123)


augroup END
