scriptencoding utf-8

setlocal foldmethod=expr
setlocal foldexpr=GetNixFold(v:lnum)
setlocal foldtext=GetNixFoldText()

if exists('*GetNixFold') && exists('*GetNixFoldText')
  finish
endif

function! GetNixFold(lnum)
  "let l:ind = indent(a:lnum)
  let l:line = getline(a:lnum)
  if match(l:line, '[[{(]$\|''''$') >= 0
    return 'a1'
  elseif match(l:line, '^\s*[\]})]\|''''\S*$') >= 0
    return 's1'
  else
    return '='
  endif
endfunction

function! GetNixFoldText()
  let l:winwd = winwidth(0) - &l:foldcolumn - &l:numberwidth
  let l:foldlines = v:foldend - v:foldstart + 1
  let l:info = ' ' . l:foldlines . ' lines'
  let l:foldtext = getline(v:foldstart) . ' ⋯ ' . getline(v:foldend)
  let l:sub = v:folddashes . substitute(' ' . l:foldtext, '\s\+', ' ', 'g')
  let l:fill = repeat(' ', l:winwd - len(l:sub) - len(l:info))
  return l:sub . l:fill . l:info
endfunction
