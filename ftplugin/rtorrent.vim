" FileType: rtorrent

if exists('b:did_ftplugin')
  finish
endif
let b:did_ftplugin = 1

if !exists('b:undo_ftplugin')
  let b:undo_ftplugin = ''
endif

let b:undo_ftplugin .= 'setlocal comments< commentstring< expandtab<'

setlocal comments=:# commentstring=#\ %s expandtab
