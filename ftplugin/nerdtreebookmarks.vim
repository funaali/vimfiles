" ftplugin/nerdtreebookmarks.vim

if exists('b:did_ftplugin')
  finish
endif
let b:did_ftplugin = 1

"setlocal iskeyword= TODO

function! s:updateBookmarks()
  let l:nerdtree = g:NERDTree.ForCurrentTab()
  if !empty(l:nerdtree) && l:nerdtree.IsOpen()
    call nerdtree#exec(l:nerdtree.GetWinNum()."wincmd w | exe 'ReadBookmarks' | wincmd p", 1)
    edit
  endif
endfunction

augroup vim_nerdtreebookmarks
  autocmd!
  autocmd BufWritePost NERDTreeBookmarks call s:updateBookmarks()
augroup END
