scriptencoding utf-8

setlocal foldmethod=expr foldexpr=GetGotexttmplFold(v:lnum) foldtext=GetGotexttmplFoldText()

if exists('*GetGotexttmplFold') && exists('*GetGotexttmplFoldText')
  finish
endif

let s:start = '{{-\?\s*\%(define\|if\|range\|block\|with\)\%(\_s\|}\)'
let s:end = '{{-\?\s*end\%(\_s\|}\)'
let s:cstart = '{{\%(- \)\?\/\*'
let s:cend = '\*\/\%( -\)\?}}'

function! GetGotexttmplFold(lnum)
  let l:pos = getcurpos()
  let l:res = '='
  let l:line = getline(a:lnum)

  " {{ foo }} ... {{ end }} blocks
  if match(l:line, s:start) >= 0
    call cursor(a:lnum, match(l:line, s:start) + 1)
    " the condition handles the case when start and its corresponding end are on the same line
    if searchpair(s:start, '', s:end, 'nW') != a:lnum
      let l:res = '>' . (searchpair(s:cstart, '', s:cend, 'nWbm') + searchpair(s:start, '', s:end, 'nWrm'))
    endif
    call setpos('.', l:pos)
  elseif match(l:line, s:end) >= 0
    call cursor(a:lnum, match(l:line, s:end) + 1)
    let l:res = '<' . (searchpair(s:cstart, '', s:cend, 'nWbm') + searchpair(s:start, '', s:end, 'nWrmb'))
    call setpos('.', l:pos)
  " Comment blocks
  elseif match(l:line, s:cstart) >= 0
    call cursor(a:lnum, match(l:line, s:cstart) + 1)
    if searchpair(s:cstart, '', s:cend, 'nW') != a:lnum
      let l:res = '>' . (1 + searchpair(s:start, '', s:end, 'nWrmb'))
    endif
    call setpos('.', l:pos)
  elseif match(l:line, s:cend) >= 0
    call cursor(a:lnum, match(l:line, s:cend) + 1)
    let l:res = '<' . (1 + searchpair(s:start, '', s:end, 'nWrmb'))
    call setpos('.', l:pos)
  endif
  return l:res
endfunction

" ┫ ┣
"set fillchars=fold:━

function! GetGotexttmplFoldText()
  let l:winwd = winwidth(0) - &l:foldcolumn - max([&l:numberwidth, strdisplaywidth(line('$')) + 1])
  let l:foldlines = v:foldend - v:foldstart + 1

  "let l:right = '┫ ' . l:foldlines . ' lines ┣'
  let l:right = '- ' . l:foldlines . ' lines -'

  " text
  let fs = v:foldstart
  while getline(fs) =~# '^\s*$'
    let fs = nextnonblank(fs + 1)
  endwhile
  let l:foldtext = getline(fs > v:foldend ? v:foldstart : fs)
  let l:foldtext = l:foldtext[:(l:winwd - strchars(l:right) - 5)]

  let l:left = l:foldtext

  let l:fill = repeat(' ', l:winwd - strchars(l:left) - strchars(l:right))
  return l:left . l:fill . l:right
endfunction
