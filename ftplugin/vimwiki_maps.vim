" FileType: vimwiki

if exists('b:did_vimwiki_maps')
  finish
endif
let b:did_vimwiki_maps = 1

" Navigate link-wise
nmap <unique><silent><buffer> <c-j>          <Plug>VimwikiNextLink
nmap <unique><silent><buffer> <c-k>          <Plug>VimwikiPrevLink

" Modify headers
nmap <unique><silent><buffer> <localleader>> <Plug>VimwikiAddHeaderLevel
nmap <unique><silent><buffer> <localleader>< <Plug>VimwikiRemoveHeaderLevel

" Modify links
nmap <unique><silent><buffer> L              <Plug>VimwikiNormalizeLink
vmap <unique><silent><buffer> L              <Plug>VimwikiNormalizeLinkVisual
nmap <unique><silent><buffer> <localleader>d <Plug>VimwikiDeleteLink
nmap <unique><silent><buffer> <localleader>l <Plug>VimwikiRenameLink
nmap <unique><silent><buffer> t              <Plug>VimwikiTabnewLink

"silent! TagbarOpen
