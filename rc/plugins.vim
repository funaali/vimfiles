" rc/plugins.vim

" Dein options {{{
if !exists('g:did_myvimrc_dein_settings')
  let g:did_myvimrc_dein_settings    = 1
  let g:dein#auto_recache            = 1
  let g:dein#cache_directory         = init#get_directory('dein', 'cache')
  let g:dein#default_options         = {} " Default options for plugins
  let g:dein#enable_name_conversion  = 1
  let g:dein#install_process_timeout = '500'
  let g:dein#install_progress_type   = 'echo'
  let g:dein#install_message_type    = 'echo'
  let g:dein#install_log_filename    = mydein#generate_log_filename()
  let g:dein#lazy_rplugins           = 1 " nvim only
  let g:dein#enable_notification     = 1
  "let g:dein#notification_icon       = '/usr/share/icons/hicolor/48x48/apps/gvim.png'
  let g:dein#notification_time       = 10
  let g:dein#inline_vimrcs           = [] " Sourced in dein#end or dein#load_state
endif " }}}

" Ensure dein is setup
if !mydein#setup()
  finish
endif

" Load plugins
call mydein#load({
      \ 'vimrc': [$MYVIMRC, expand('<sfile>')],
      \ 'local': [g:init#vimfiles . '/bundle']
      \})

" The post_source hooks are not called automatically when vim is starting. So
" we setup a autocommand to call them.
if has('vim_starting')
  augroup myvimrc_dein
    autocmd! VimEnter * nested call dein#call_hook('post_source')
          \| autocmd! myvimrc_dein VimEnter
  augroup END
endif
