" ~/.vim/rc/options.vim
" See also :options window

if exists('g:loaded_myvimrc_options')
  finish
endif
let g:loaded_myvimrc_options = 1

" encoding                                                                {{{1
setg encoding=utf-8   " "global" encoding (latin1 or from "$LANG")

" cpoptions                                                               {{{1
setg cpoptions& " "global" cpoptions (aABceFs)
setg cpo+=i     " *cpo-i* Interrupting the reading of a file will leave it modified.
setg cpo+=I     " *cpo-I* When moving the cursor up or down just after inserting indent for 'autoindent', do not delete the indent.
setg cpo+=o     " *cpo-o* Line offset to search command is not remembered for next search.
setg cpo+=t     " *cpo-t* Search pattern for the tag command is remembered for "n" command.
setg cpo+=w     " *cpo-w* When using "cw" on a blank character, only change one character and not all blanks until the start of the next word.
setg cpo+=X     " *cpo-X* When using a count with "R" the replaced text is deleted only once.  Also when repeating "R" with "." and a count.
setg cpo+=$     " *cpo-$* When making a change to one line, don't redisplay the line, but put a '$' at the end of the changed text.
setg cpo-=s     " *cpo-s* Set buffer options when entering buffer for first time.
setg cpo+=K     " *cpo-K* Don't wait for a key code to complete when it is halfway a mapping.

set clipboard+=unnamed
if has('unnamedplus')
  set clipboard+=unnamedplus
  set clipboard+=autoselectplus
endif

" common                                                                  {{{1
set shell=sh
if has('filterpipe')
  set noshelltemp       " "global" shelltemp (on) - When off, prefer pipe in shell commands.
endif
if has('python3')
  set pyxversion=3      " "global" pyxversion ('')
endif
set secure              " "global" secure (off)

" Empty packpath to avoid unwanted loading of "pack/*/start" plugins.
"setg packpath=           " "global" packpath (...)

" Reset rtp to default and clear the system defaults. Enable if undesired
" files are loaded from the default paths.
"setg rtp&                " "global" runtimepath (...)
"setg rtp-=/usr/share/vim/vimfiles
"setg rtp-=/usr/share/vim/vimfiles/after

setg sessionoptions&     " "global" sessionoptions (....) Note: don't include 'folds' if using FastFold
setg ssop-=folds
setg ssop-=curdir
setg ssop-=options
setg ssop+=sesdir,localoptions
setg viewoptions-=options

set selectmode=key
set keymodel=startsel

" GUI                                                                     {{{1
if !has('gui_running')
  set background=dark   " "global" background (Vim tries to guess if not set (and easily fails on terminals))
endif
setg guioptions&         " "global" guioptions ("egmrLT")
setg guioptions+=!fc
setg guioptions-=m       " Hide menubar
setg guioptions-=T       " Hide toolbar
setg guioptions-=rL
setg guioptions+=p " pointer callback hack
set guicursor+=a:blinkon0 " Disable GUI cursor blinking in all modes
set guiheadroom=0
set guifont=TerminessTTF\ Nerd\ Font\ Medium\ 14
set mousefocus

" statusline, prompt, wildmenu                                            {{{1
set laststatus=2         " "global" laststatus (1)
set showcmd              " "global" showcmd (on)
set showfulltag          " "global" (off)
set showmatch            " "global" showmatch (off)
set noshowmode           " "global" showmode (on)
set showtabline=2        " (1)
set confirm              " "global" confirm (off)
set cmdheight=2          " "global" cmdheight (1)
set shortmess=aoOtcIs    " "global" shortmess ("filnxtToOS")  TFA
set nowildmenu           " "global" wildmenu (off)
set wildchar=<tab>       " "global" wildchar (<tab>)
set wildcharm=<tab>      " "global" wildcharm (none 0)
                          " "global" wildmode (full)
set wildmode=list:longest,full
" TODO how to combine list:lastused, list:longest and full without needing to
" smash wildchar needlessly?

set wildignorecase       " "global" wildignorecase (off)
set tabpagemax=50        " "global" tabpagemax (10)

" edit                                                                    {{{1
setg pastetoggle=<F2>        " "global" pastetoggle ('')
setg tildeop                 " "global" tildeop (off)
setg backspace=indent,start  " "global" backspace ("indent,eol,start")
setg nojoinspaces            " "global" joinspaces (on)
setg virtualedit=block       " "global" virtualedit ("")
setg nostartofline           " "global" startofline (on)
setg whichwrap=<,>,[,]       " "global" whichwrap ("b,s")

" formatting                                                              {{{1
if has('linebreak')
  setg breakat&         " "global" breakat (^I!@*-+;:,./?)
  setg breakindent      " "local (window)" breakindent (off)
  setg breakindentopt=  " "local (window)" breakindentopt ('')
endif
setg smarttab&          " "global" smarttab (off)
setg shiftround&        " "global" shiftround (off)
setg tabstop&           " "local (buffer)" tabstop (8)
setg shiftwidth=0       " "local (buffer)" shiftwidth (8)
setg softtabstop=-1     " "local (buffer)" softtabstop (0)
" NOTE: default to wrapping at width of 78 characters (excluding CRLF) per the
" recommendation in RFC822.
setg textwidth=78       " "local (buffer)" textwidth (0)
setg expandtab&         " "local (buffer)" expandtab (off)
setg autoindent         " "local (buffer)" autoindent (off)
setg formatoptions&     " "local (buffer)" formatoptions ("tcq")
setg fo+=ronlj
setl ts< sw< sts< tw< et< ai< fo<

" search                                                                  {{{1
setg ignorecase         " "global" ignorecase (off)
setg smartcase          " "global" smartcase (off)
if has('extra_search')
  set incsearch         " "global" incsearch (off)
endif
setg matchtime=2        " "global" matchtime (5)
setg matchpairs&        " "local (buffer)" matchpairs ("(:),{:},[:]")
setg matchpairs+=<:>,^:$
setg makeprg&           " "global-local (buffer)" makeprg (make)
setg errorformat&       " "global-local (buffer)" errorformat (...)
setg grepprg=grep\ -snPHi\ --color=no\ --directories=recurse
                        " "global-local (buffer)" grepprg ("grep -n $* /dev/null")
setg grepformat=%f:%l:%m " "global" grepformat (%f:%l:%m,...) <filename>:<linenr>:<message>
setl mps< mp< gp< efm<
set  hlsearch

" Buffer                                                                  {{{1
set hidden             " "global" hidden (off). See also: bufhidden
set switchbuf=useopen,usetab,uselast  " "global" switchbuf ("")
set lazyredraw         " "global" lazyredraw (off)
set errorbells         " "global" errorbells (off)
set belloff=cursor,complete,ctrlg,esc,insertmode,spell,wildmode,register,error,showmatch
set autowrite&         " "global" autowrite (off)
set autoread           " "global-local (buffer)" autoread (off)

" modelines                                                               {{{1
setg modelines=2        " "global" modelines (5)
setg modeline&          " "local (buffer)" modeline (on) (off for root)
setl modeline<

" window                                                                  {{{1
"set helpheight=20      " "global" helpheight (20)
"set previewheight=12   " "global" previewheight (12)
"setg equalalways       " "global" equalalways (on)
"setg eadirection=both  " "global" eadirection (both)
"setg winheight=12       " "global" winheight (1)
"setg winwidth=20        " "global" winwidth (20)
setg winminheight=0     " "global" winminheight (1)
" NOTE: the FastFold plugin throws errors if winminwidth is set to a value greater than 1
"setg winminwidth=20     " "global" winminwidth (1)
"setg splitbelow        " "global" splitbelow (off)
"setg splitright         " "global" splitright (off)
setg scrolljump=5       " "global" (0)
setg scrolloff=256      " "global-local (window)" scrolloff (5)
setg sidescroll=1       " "global" (0)
setg sidescrolloff=5    " "global-local (window)" (0)
setl scrolloff< sidescrolloff<
setg nowrap             " "local (window)" wrap (on)
setg linebreak          " "local (window)" linebreak (off)
setg number             " "local (window)" number (off)
setg relativenumber     " "local (window)" relativenumber (off)
setg numberwidth=2      " "local (window)" numberwidth (4)
setg cursorline         " "local (window)" cursorline (off)
setl wrap< lbr< bri< briopt< nu< rnu< nuw< cul<

" signs                                                                   {{{1
if has('signs')
  setg signcolumn=number " "local (window)" signcolumn ("auto")
  setl signcolumn<
endif

" conceal                                                                 {{{1
setg conceallevel=2     " "local (window)" conceallevel (0)
setg concealcursor=nc   " "local (window)" concealcursor ("")
setl cole< cocu<

" folding                                                                 {{{1
if has('folding')
  setg foldopen-=block    " "global" foldopen ("block,hor,mark,percent,quickfix,search,tag,undo")
  setg foldopen-=undo
  setg foldlevelstart=1   " "global" foldlevelstart (-1)
  setg foldignore=        " "local (window)" foldignore ("#")
  setl foldignore<
  setg foldmethod=marker  " "local (window)" foldmethod ("manual")
  setl foldmethod<
  setg foldcolumn=1       " "local (window)" foldcolumn (0)
  setl foldcolumn<
  set foldnestmax=6       " "local (window)" foldnestmax (20)
endif

" history                                                                 {{{1
set history=2000        " "global" history (200)

" undo                                                                    {{{1
setg undoreload=15000   " "global" undoreload (10000)
setg undolevels=500     " "global-local (buffer)" undolevels (1000)
setl undolevels<

if has('persistent_undo')
  setg undofile         " "local (buffer)" undofile (off)
  setl undofile<
endif

" backup                                                                  {{{1
setg backup             " "global" backup (off)
setg writebackup        " "global" writebackup (off)
setg backupskip&        " "global" backupskip (/tmp/*,$TMPDIR/*,$TMP/*,$TEMP/*)
setg bsk+=/run/*
setg bsk+=/dev/*
setg bsk+=*/.ssh/id*
setg bsk+=*_EDITMSG
setg backupcopy&        " "global-local (buffer)" backupcopy (auto)
setl bkc<

" balloon-eval                                                            {{{1
if has('balloon_eval_term')
  set ballooneval
  set balloonevalterm
endif

" listchars (global-local)                                                {{{1
setg listchars=eol:$,tab:\\_
setg listchars+=multispace:\ \ \ +,leadmultispace:\ ---,trail:.
setg listchars+=extends:>,precedes:<,nbsp:+
setl listchars<

" tags: search upwards                                                    {{{1
setglobal tags-=./tags tags-=./tags; tags^=./tags;

" How many milliseconds of nothing typed before writing the swap file to disk.
setglobal updatetime=1250
