" rc/lsp_buffer.vim

"setl signcolumn=yes
setl tagfunc=lsp#tagfunc

" inspecting/jumping
nmap <buffer> K  <plug>(lsp-hover)
nmap <buffer> gd <plug>(lsp-definition)
nmap <buffer> <localleader>pd <plug>(lsp-peek-definition)

" diagnostics
nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
nmap <buffer> ]g <Plug>(lsp-next-diagnostic)

" formatting
xmap <buffer> <localleader>gq <plug>(lsp-document-range-format)
nmap <buffer> <localleader>GQ <plug>(lsp-document-format)

" completion
nmap <buffer> gl <plug>(lsp-code-lens)

" TODO interferes with vim's default omnifunc for lang/ghcopt pragmas
"setl omnifunc=lsp#complete

" Commands                                                                {{{1

command! LspFoldEnable setlocal foldmethod=expr foldexpr=lsp#ui#vim#folding#foldexpr() foldtext=lsp#ui#vim#folding#foldtext()
