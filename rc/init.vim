" File: ~/.vim/rc/init.vim
" Note: directory, backupdir, undodir are (also) set in /usr/share/vim/vimfiles/archlinux.vim on ArchLinux.

if exists('g:did_myvimrc_init')
  finish
endif
let g:did_myvimrc_init = 1

" Set language                                                            {{{1
language message C

" Paths and directories                                                   {{{1

" XXX workaround for vim-utils/vim-man. to prefer `manpath` over any $MANPATH
" value.
unlet $MANPATH

call init#setup_directories()

" Swap files
setg swapsync=
let s:dir = init#get_directory('swap', 'cache')
if !empty(s:dir)
  let &directory = s:dir . '//' . ',.'
endif

" Backup files
let s:dir = init#get_directory('backup', 'cache')
if !empty(s:dir)
  let &backupdir = s:dir . '//' . ',.'
endif

if has('mksession')
  let s:dir = init#get_directory('view', 'cache')
  if !empty(s:dir)
    let &viewdir = s:dir
  endif
  unlet s:dir
endif


if has('viminfo')
  set viminfo='100,<1000,s100,r/tmp,r/run,r/sys,r/proc
  if &viminfo !~# ',u' && &viminfofile !=# 'NONE'
    " Default value when empty
    if empty(&viminfofile)
      let s:dir = init#get_directory('viminfo', 'data')
      if !empty(s:dir)
        let &viminfofile = s:dir . '/viminfo'
        " Forcibly reload viminfo in case the file changed.
        if !v:vim_did_enter && filereadable(&viminfofile)
          exe 'rviminfo!' fnameescape(&viminfofile)
        endif
      endif
    endif
  endif
endif

" TODO SwapExists

if has('persistent_undo')
  let s:dir = init#get_directory('undo', 'cache')
  if !empty(s:dir)
    let &undodir = s:dir . '//,.'
  endif
endif

unlet s:dir

" Python                                                                  {{{1
if has('pythonx')
  " Force python not to read user packages.
  let $PYTHONNOUSERSITE = 1

  "let $PYTHONPATH = init#get_directory('','python') . substitute($PYTHONPATH, '\ze.', ':', '')
  "let $PATH = init#get_directory('bin','python') . ':' . $PATH
endif

" Make sure nix-managed binaries get priority.
let $PATH = g:init#installedDir . '/bin:' . $PATH

" Vim default plugins                                                     {{{1

let g:loaded_getscriptPlugin  = 1
let g:loaded_gzip             = 1
let g:loaded_logiPat          = 1
let g:loaded_matchparen       = 1
let g:loaded_netrwPlugin      = 1
let g:loaded_rrhelper         = 1
let g:loaded_spellfile_plugin = 1
let g:loaded_tarPlugin        = 1
let g:loaded_2html_plugin     = 1
let g:loaded_zipPlugin        = 1
let g:loaded_vimballPlugin    = 1
"let g:loaded_matchit         = 1 " <url:vimhelp:matchit.txt> - extended matching with `%'

" Default Filetype plugins (vim ships with)  <url:vimhelp:ftplugin-docs>

" BASH  <url:vimhelp:ft-bash-syntax>
let g:is_bash                 = 1
let g:sh_fold_enabled         = 7 " For foldmethod=syntax
" MAN  <url:vimhelp:ft-man-plugin>
let g:no_man_maps             = 1
let g:ft_man_folding_enable   = 1
let g:ft_man_no_sect_fallback = 1
let g:ft_man_open_mode        = 'tab'
" VIM                                              <url:vimhelp:ft-vim-syntax>
let g:vimsyn_embed   = 'lpPr'
let g:vimsyn_folding = 'aflpPr'
" Define [[, ]] and ]", ["                        <url:vimhelp:ft-vim-plugin>
let g:no_vim_maps    = 1
" QF

let g:qf_disable_statusline   = 1
" NOTE The cecutil plugin is included in so many other plugins

let g:no_cecutil_maps = 1
" RUST  <url:vimhelp:ft_rust.txt>
" NOTE not configured
" SQL  <url:vimhelp:ft_sql.txt>
" NOTE not configured
" TEX
" NOTE not configured
" cecutil plugin
