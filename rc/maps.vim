" ~/.vim/rc/maps.vim

if exists('g:loaded_myvimrc_maps')
  finish
endif
let g:loaded_myvimrc_maps = 1

" Set mapleaders
" NOTE: Leaders expand when a mapping is defined, so it's useful to define them early.
let g:mapleader      = ','
let g:maplocalleader = ','

" Map ",," (comma x2) to "," (comma)                           <url:vimhelp:,>
noremap ,, ,

" Map "\" (comma) to "<Leader>" (alias for g:map(local)leader)
map \ <Leader>

" Map ";" (semicolon) to ":" (colon) (normal mode only)      <url:vimhelp://;>
nnoremap ; :

" Map "CTRL-@" to "CTRL-^" (dvp layout opt.)       <url:vimhelp:CTRL-W_CTRL-^>
nmap <C-@> <C-^>
" Disable some default ":lmap" mapping
cnoremap <C-^> <NOP>

" kitty modifiers <C-@>
" Can't pass a null through to e.g. tmux
if !empty($TMUX)
  map  <Esc>[64;5~ <C-@>
  imap <Esc>[64;5~ <C-@>
  cmap <Esc>[64;5~ <C-@>
endif

" Jumplist
nnoremap <S-Tab> <C-O>

" Don't enter Ex mode with "Q" (use "gQ"). Use "Q" like "=" (apply "equalprg").
nnoremap Q <Nop>
nnoremap Q =
vnoremap Q =

"TODO execute command and stay in the command-line window
"autocmd CmdwinEnter * map <buffer> <F5> <CR>q:
"enter insert mode
"au CmdwinEnter [/?]  startinsert

" Use backspace in n/v/o mode
noremap <BS> X

" Use "c" like motion for <count> characters
onoremap c l

" Use ~~ (normal) to swap case of one character
onoremap <expr> ~ v:operator == 'g~' ? 'l' : '~'

" Toggle: syntax on/off, quickfix window
noremap <unique> <F4> <Cmd>call vimrc#toggle_crap()<CR>
noremap <silent> <F5> <Cmd>call vimrc#toggle_qfix()<CR>
noremap <silent> <F6> <Cmd>call vimrc#toggle_loclist()<CR>
"noremap <silent> <F4> <Cmd>call vimrc#toggle_syntax()<CR>

" Toggle "list"
nnoremap <unique> <leader>sl <Cmd>setlocal list!<CR>

" Clear "hlsearch" highlights (re-added automatically)
nnoremap <unique> <leader>hh <Cmd>noh<CR><Cmd>call clearmatches()<CR>

" Note: modifyotherkeys required for following
inoremap <C-{> {{{
inoremap <C-}> }}}
nnoremap <C-Tab> :

" Alternatives for CTRL-N/CTRL-P and CTRL-G/CTRL-T (with incsearch)
cnoremap <expr> <C-J> getcmdtype() =~ '[/?]' ? '<C-G>' : '<C-N>'
cnoremap <expr> <C-K> getcmdtype() =~ '[/?]' ? '<C-T>' : '<C-P>'

" Exit Ex-mode with CTRL-C
cnoremap <expr> <C-C> mode(1) == 'cv' ? '<C-E><C-U>visual<CR>' : '<C-C>'

" Misc                                                                     {{{1
" select whole file
map <M-a> ggVG

" meta-arrow keys                                                         {{{1
nnoremap <silent> <M-h> <C-w><
nnoremap <silent> <M-k> <C-w>-
nnoremap <silent> <M-j> <C-w>+
nnoremap <silent> <M-l> <C-w>>

" Emacs/Readline style mps                                                {{{1
"
" <url:https://github.com/tpope/vim-rsi/blob/master/doc/rsi.txt>
" <url:vimhelp:emacs-keys>
" <url:vimhelp:ins-special-special>

" start of line
" Remap original i_CTRL-A to <C-X><C-A>
inoremap      <C-A> <C-O>^
inoremap <C-X><C-A> <C-A>
cnoremap      <C-A> <Home>
cnoremap <C-X><C-A> <C-A>

" end of line
" Handle original i_CTRL-E
inoremap <expr> <C-E> col('.')>strlen(getline('.'))<bar><bar>pumvisible()?"\<Lt>C-E>":"\<Lt>End>"
cnoremap        <C-E> <End>

" back one character
cnoremap <C-B> <Left>

" forward one character
" Retain original i_CTRL-F effect when at end of line
inoremap <expr> <C-F> &cindent&&col('.')>strlen(getline('.'))?"\<Lt>C-F>":"\<Lt>Right>"
cnoremap <expr> <C-F> getcmdpos()>strlen(getcmdline())?&cedit:"\<Lt>Right>"

" delete character under cursor
" handle original i_CTRL-D and c_CTRL-D
inoremap <expr> <C-D> col('.')>strlen(getline('.'))?"\<Lt>C-D>":"\<Lt>Del>"
cnoremap <expr> <C-D> getcmdpos()>strlen(getcmdline())?"\<Lt>C-D>":"\<Lt>Del>"

" backward-word
cnoremap <M-b>  <S-Left>
inoremap <M-b>  <C-G>u<C-Left>
if !empty($TMUX)
  cnoremap <Esc>b <S-Left>
  inoremap <Esc>b <C-G>u<C-Left>
endif

" forward one word
cnoremap <M-f>  <S-Right>
inoremap <M-f>  <C-G>u<C-Right>
if !empty($TMUX)
  cnoremap <Esc>f <S-Right>
  inoremap <Esc>f <C-G>u<C-Right>
endif

" delete one word back
cnoremap <M-BS>    <C-W>
inoremap <M-BS>    <C-G>u<C-W>
nnoremap <M-BS>    a<C-G>u<C-W><C-\><C-G>
if !empty($TMUX)
  cnoremap <Esc><BS> <C-W>
  inoremap <Esc><BS> <C-G>u<C-W>
endif

" delete one word forward
cnoremap <M-d>  <S-Right><C-W>
inoremap <M-d>  <C-O>daw
nnoremap <M-d>  daw
if !empty($TMUX)
  cnoremap <Esc>d <S-Right><C-W>
  inoremap <Esc>d <C-O>daw
endif

" go one line down; i_<Down>
inoremap <M-n> <Down>
" recall newer command-line
cnoremap <C-N> <Down>

" go one line up; i_<Up>
inoremap <M-p> <Up>
" recall previous (older) command-line
cnoremap <C-P> <Up>

" delete line back                                                        {{{1
" Note: insert-mode <C-Y> inserts the character above the cursor (<C-E> that
" which is below)
" Note: command-line mode <C-Y> (and other modes) ynaks the modeless
" selection.

function! s:c_ctrl_u()                                                    "{{{
  if getcmdpos() > 1
    let @- = getcmdline()[:getcmdpos()-2]
  endif
  return "\<C-U>"
endfunction "}}}

cnoremap <expr> <C-U> <SID>c_ctrl_u()
cnoremap <expr> <C-Y> pumvisible() ? "\<C-Y>" : "\<C-R>-"

" Make it possible to undo the effect of <C-U> (delete all entered characters
" before cursor) in insert mode.
" Same for <C-W> (delete the word before the cursor).
inoremap <C-U> <C-G>u<C-\><C-O>"-d0
inoremap <C-W> <C-G>u<C-W>

" ???
inoremap <M-y> <C-R>-

" transpose two characters                                                {{{1

function! s:transpose() abort                                             "{{{2
  let pos = getcmdpos()
  if getcmdtype() =~# '[?/]'
    return "\<C-T>"
  elseif pos > strlen(getcmdline())
    let pre = "\<Left>"
    let pos -= 1
  elseif pos <= 1
    let pre = "\<Right>"
    let pos += 1
  else
    let pre = ''
  endif
  return pre . "\<BS>\<Right>".matchstr(getcmdline()[0 : pos-2], '.$')
endfunction "}}}2

" Note: probably don't want to overrdie i_CTRL-T
cnoremap <expr> <C-T> <SID>transpose()
cnoremap <expr> <M-t> <SID>transpose()
"inoremap <expr> <M-t> <SID>transpose()
