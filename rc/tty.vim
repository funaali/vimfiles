" Settings for terminals
" https://iterm2.com/documentation-escape-codes.html

scriptencoding utf-8

if exists('g:did_myvimrc_tty')
  finish
endif
let g:did_myvimrc_tty = 1

" NOTE: on some tmux, it's necessary to do this to get proper 24-bit color
" support:
"
"    set -ag terminal-overrides "xterm-kitty:Tc"
"
" Reset after running vim:
"
"     stty cs8 -parenb -istrip

" TTY defaults                                                             {{{1
set nottybuiltin      " "global" ttybuiltin (on)
set ttyfast           " "global" ttyfast (...)
set ttyscroll=3       " "global" ttyscroll (999) - set small if redrawing is fast
set esckeys           " "global" (on) do <Esc> escapes in insert mode
set digraph<          " "global" digraph (off)
set visualbell        " "global" visualbell (off) - visual bell instead of beeping
"set t_vb=             " "global" t_vb (...) - bell character (empty for no bell)
set notimeout
set timeoutlen=1000 " "global" timeout to complete mappings (1000ms)
set ttimeout
set ttimeoutlen=50  " "global" timeout to complete keycodes, if different than 'timeoutlen' (-1)

" Allow 16 (bright) colors even if terminal doesn't advertise to support them.
if &t_Co == 8 && !empty($TERM)
  set t_Co=16
endif

" Mouse                                                                    {{{1
set mouse=a           " "global" mouse ("a") - enable/disable mouse
set mousemodel=popup_setpos
if has('mouse')
  " Don't set ttymouse automatically
  "set t_RV=
  " sgr:    tmux, kitty
  " urxvt:  urxvt
  " xterm2: xterm
  set ttymouse=sgr
endif

" Title string                                                             {{{1
set title
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:.:h\")})%)%(\ %a%)
" TODO titleold
"let  &titleold = 'VIM (gone) ['.getcwd().']'

" Cursor shape codes (xterm)                                               {{{
" 0 -- blink block
" 1 -- box with blink
" 2 -- box without blink
" 3 -- hbar(underline) with blink
" 4 -- hbar(underline) without blink
" 5 -- bar(vertical) line with blink
" 6 -- bar(vertical) line without blink
"}}}
let g:Terminfo_cursor_shapes = {
      \'n': 2,
      \'i': 5,
      \'r': 4
      \}
let g:Terminfo_cursor_colors = {
      \'n': '#2aa198',
      \'i': '#b58900',
      \'r': '#dc322f',
      \}
let g:Terminfo_kitty_cursor_colors = {
      \'n': '',
      \'RESET': "\<Esc>]112;\<C-G>"
      \}

function! g:Terminfo_setup(term, client) abort "{{{
  set noxtermcodes
  if has('termguicolors')
    let hasTc = $COLORTERM =~# 'truecolor\|24bit' || a:client =~# 'iterm\|-truecolor\|vte.*\|kitty'
    let &termguicolors = hasTc
  endif
  if a:term =~# '\<tmux\>'
    let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
    let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
    let &t_TI = "\<Esc>[>4;2m"
    let &t_TE = "\<Esc>[>4;m"
    " Enable bracketed paste mode, see  :help xterm-bracketed-paste
    let &t_BE = "\<Esc>[?2004h"
    let &t_BD = "\<Esc>[?2004l"
    let &t_PS = "\<Esc>[200~"
    let &t_PE = "\<Esc>[201~"
    " Enable focus event tracking, see  :help xterm-focus-event
    let &t_fe = "\<Esc>[?1004h"
    let &t_fd = "\<Esc>[?1004l"
    " Enable modified arrow keys, see  :help arrow_modifiers
    execute "silent! set <xUp>=\<Esc>[@;*A"
    execute "silent! set <xDown>=\<Esc>[@;*B"
    execute "silent! set <xRight>=\<Esc>[@;*C"
    execute "silent! set <xLeft>=\<Esc>[@;*D"
  endif
  if a:term =~# '\<screen\>'
	  let &t_BE = "\<Esc>[?2004h"
	  let &t_BD = "\<Esc>[?2004l"
	  let &t_PS = "\<Esc>[200~"
	  let &t_PE = "\<Esc>[201~"
  endif
  if a:term =~# '\<kitty\>'
    set esckeys
    set timeout
    set nottimeout
    set tsl=999
    set sj=1
    " Enter/leave "raw" mode
    let &t_TI = "\<Esc>[>1u"
    let &t_TE = "\<Esc>[<u"
    " https://sw.kovidgoyal.net/kitty/keyboard-protocol/#legacy-text
    exe "set t_kh=\<Esc>[*H"   |"  <Home> \E[1;*F
    exe "set t_@7=\<Esc>[*F"   |"  <End>  \E[1;*F
    " alternate function keys
    exe "set <xF1>=\<Esc>[*P"
    exe "set <xF2>=\<Esc>[*Q"
    exe "set <xF3>=\<Esc>[*R"
    exe "set <xF4>=\<Esc>[*S"
  endif
  if a:client =~# '\<xterm\>'
    " set cursor shape
    let &t_SH = "\<Esc>[%p1%d q"
    " set cursor color
    let &t_SC = "\<Esc>]12;"
    let &t_EC = "\x7"
    "let &t_EC = "\<Esc>\\"
  endif
  if a:term.'/'.a:client =~# '\<kitty\>'
    set t_ut=             " kitty does not support bg color erase note: could see if "tput bce" works
    set ttimeoutlen=50    " Lower key code timeout (mainly for <Esc>)
    set colorcolumn=+1,+2 " Fast terminals
    let g:cursorcolumn_enabled = 1
    " Fix <Esc>
    execute "silent! set <Esc>=\<Esc>[27;*u"
    " Truecolor support
    let &t_RF = "\e]10;?\e\\"
    let &t_RB = "\e]11;?\e\\"
    " Cursor control
    let &t_RC = "\e[?12$p"
    let &t_RS = "\eP$q q\e\\"
  endif
endfunction "}}}

" Autocmds: {{{
" TermChanged: after value of 'term' has changed:
"   - re-load syntax
augroup myvimrc_tty
  autocmd!
  au TermChanged  * call init#refresh_tty()
  "au TermResponse * echomsg ("[term response] received: ".v:termresponse)
  au VimLeave     * call init#reset_tty()
augroup END "}}}

call init#refresh_tty()
