scriptencoding utf-8

function! pluginhooks#call(plugin, hook) abort "{{{
  if exists('*pluginhooks#'. a:plugin . '_' . a:hook)
    call call('pluginhooks#'. a:plugin . '_' . a:hook, [])
  elseif a:hook ==# 'post_update'
    " Nixed patching on post_update (before build)
    let l:args  = ['-f', $HOME.'/.vim/nix', 'plugin-post-update']
    let l:args += ['--argstr', 'plugin_name', a:plugin]
    let l:args += ['--argstr', 'plugin_path', dein#get(a:plugin)->get('path')]
  endif
endfunction "}}}
