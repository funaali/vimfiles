" hsimport vim bindings
" Home: https://github.com/fendor/hsimport
" Usage:
" hsimport <options> <sourcefile>
"   -H         Hide given symbols
"   -m ITEM    Module to import
"   -s ITEM    Symbol to import
"   -a         Select all constructors/methods
"   -w ITEM    Select item
"   -q ITEM    Set qualified name
"   --as ITEM  Set unqualified name
"   -o FILE    Output file (default: input file)
" Sandboxed Usage:
"   # set GHC_PACKAGE_PATH:
"   export GLOBAL_PKG_DB=/usr/lib/ghc/package.conf.d/
"   export SANDBOX_PKG_DB=/home/you/hsimport-build-dir/.cabal-sandbox/*-packages.conf.d/
"   GHC_PACKAGE_PATH=$GLOBAL_PKG_DB:$SANDBOX_PKG_DB hsimport ...
" Examples:
"   call hs#hsimport#execute('-m Control.Applicative')

" Operates on the current file
function! hs#hsimport#execute(opts) abort "{{{
  if !exists('b:hsimport_ghc_package_path')
    if exists('$GHC_PACKAGE_PATH') && !empty($GHC_PACKAGE_PATH)
      let b:hsimport_ghc_package_path = $GHC_PACKAGE_PATH
    else
      let b:hsimport_ghc_package_path = trim(system('stack path --ghc-package-path'))
    endif
  endif
  let args = a:opts . ' ' . fnameescape(expand('%'))

  " Alternative: build diff
  " TODO tmp file name
  let s:diff = ''
  silent call s:run_it('w !cat >tempin; GHC_PACKAGE_PATH='.shellescape(b:hsimport_ghc_package_path,1).' hsimport -o /dev/stdout '.a:opts.' tempin | diff -e tempin -;[ $? -eq 1 ]')
  if !v:shell_error
    call hs#hsimport#applydiff(s:diff)
    return s:diff
  endif

  "let exe  = 'GHC_PACKAGE_PATH=' . b:hsimport_ghc_package_path . ' hsimport '
  " Alternative: in-place
  "call system(exe . args)
  " Alternative: filter
  "execute '%!'.exe.'-o /dev/stdout '.args

endfunction "}}}

function! s:run_it(cmd) abort "{{{
  redir => s:diff
  exe a:cmd
  redir END
endfunction "}}}

" Apply diffs like
" 138c
" import....
" .
" Or
" 138a
" import....
" .
" TODO only one-line changes
function! hs#hsimport#applydiff(diff) abort "{{{
  let [l:pos,l:text,l:dot] = split(a:diff,'\n')
  let [l:op]   = split(l:pos,'[0-9]')
  let [l:lnum] = split(l:pos,'[^0-9]')

  exe 'normal 0'.l:lnum.'G'
  if l:op ==# 'a'
    call append(l:lnum, [l:text])
  elseif l:op ==# 'c'
    call setline(l:lnum, l:text)
  else
    exe 'echoerr' 'dont know how to apply: '.a:diff
  endif

  return [l:lnum, l:op, l:text]
endfunction "}}}
