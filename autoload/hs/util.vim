" ~/.vim/autoload/hs/util.vim

function! hs#util#module_name(path) abort                                 "{{{1

  " <url:file:~/.vim/UltiSnips/haskell.snippets#snip:haskell:header>

  if fnamemodify(a:path, ':t') !~# '\u'
    return 'Main'
  endif

  let module = []
  for part in reverse(split(fnamemodify(a:path, ':r'), '/'))
    if part !~# '^\u' | break | endif
    let module += [part]
  endfor

  return join(reverse(module), '.')
endfunction

