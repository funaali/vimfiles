" ~/.vim/autoload/compulsion.vim

scriptencoding utf-8

let g:compulsion_directory = init#get_directory('data', 'compulsion')

function! compulsion#handle(name) abort "{{{
  let session = get(g:, 'compulsion_session', v:this_session)
  if empty(session)
    echoerr 'No session defined!'
    return
  elseif get(g:,'SessionLoad',0)
    return
  else
    let file = g:compulsion_directory . '/' . session . '.vim'
    if filereadable(file) && getfsize(file) > 0
      echoerr 'Session file exists already!'
      return
    elseif filewritable(file)
      echo '[compulsion] session updated [NEW] ('.fnamemodify(file, ':~').')'
      exe 'mksession' fnameescape(file)
      " TODO schedule ":mksession!" updates here
    endif
  endif
endfunction "}}}
