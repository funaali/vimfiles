" File: ~/.vim/autoload/mydein.vim
" Author: Samuli Thomasson
" License: WTFPL
" Description: mydein

scriptencoding utf-8

function! mydein#on_host(hostname) abort                                  "{{{
    " Override plugin setup on hostname
    if a:hostname =~? '^monad'
      call dein#disable('deoplete-tabnine')
    endif
endfunction "}}}

function! mydein#setup() abort                                            "{{{
  " Setup dein. Installs from the git repository if necessary.
  " Return:
  "   0: setup failed
  "   1: setup succeeded (rtp updated)
  "   2: already setup (probably)

  let l:to = s:base_path() . '/repos/github.com/Shougo/dein.vim'
  if stridx(&g:runtimepath, l:to) >= 0 || exists('#dein')
    return 2
  elseif init#is_pager()
    " Don't try to setup if vimpager
    return 0
  elseif !isdirectory(l:to) && !get(g:, 'myvimrc#dein_only_cached')
    let l:src = 'https://github.com/Shougo/dein.vim'
    try
      execute '!git clone -q' shellescape(l:src) shellescape(l:to)
    catch /.*/
      echomsg 'WARNING: couldn''t perform initial dein checkout from ' . l:src . ' to ' . l:to . ' the exception was: ' . v:exception
      return 0
    endtry
  endif
  exe 'set runtimepath+=' . l:to
  return 1
endfunction "}}}

function! mydein#load(args) abort  "{{{
  let all_vimrc = get(a:, 'vimrc', [$MYVIMRC]) + s:get_hook_files()
  let all_toml = get(a:, 'toml', s:get_toml_configs())
  let all_local = get(a:, 'local', [])

  if dein#load_state(s:base_path())
    call dein#begin(s:base_path(), all_vimrc)

    " Load plugins from TOML files
    for [fp, cfg] in items(all_toml)
      call dein#load_toml(fp, cfg)
    endfor

    " Load plugins from local directories
    for fp in all_local
      let fp = fnamemodify(fp, ':p')
      if isdirectory(fp)
        call dein#local(fp)
      endif
    endfor

    " Load hooks from files
    call s:get_hooks_from_files('add')
    call s:get_hooks_from_files('post_update')
    call s:get_hooks_from_files('post_source')

    " Host overrides
    call mydein#on_host(hostname())

    " Set runtimepath
    call dein#end()

    " Write cache script
    call dein#save_state()

    " Install plugins (async)
    if !has('vim_starting') && !init#is_pager() && dein#check_install()
      call dein#install()
    endif
  endif
endfunction "}}}

function! mydein#generate_log_filename() abort                            "{{{
  " Create a new file name for dein install log
  return printf('%s/install_%.9s%s.log',
        \ s:base_path(),
        \ empty($MYVIMRC) ? 'NONE' : fnamemodify($MYVIMRC, ':t:r:r'),
        \ exists('*strftime') ? strftime('_%Y-%m-%d') : '')
endfunction "}}}

" Internal:

function! s:base_path() abort                                             "{{{
  " Get dein base dir. Provides a default in case dein has not yet been
  " initialized, and returns the current one otherwise.
  if exists('g:dein#_base_path')
    return g:dein#_base_path
  elseif has('win32') " XXX Weird windows hack
    return $HOME . '/dein'
  else
    return init#get_directory('dein', 'cache')
  endif
endfunction "}}}

function! s:get_toml_configs() abort                                      "{{{
  let paths = globpath(g:init#vimfiles . '/dein', '*.toml', 0, 1)
  let dict = {}
  for path in paths
    let dict[path] = {'lazy': (path =~? 'lazy.toml$')}
  endfor
  return dict
endfunction "}}}

"function! s:get_hook_files() abort                                        "{{{
"   return globpath(g:init#vimfiles . '/hooks/', '*.vim', 0, 1)
"endfunction "}}}

" Get the hook file(s) for a plugin (if any exist)
function! s:get_hook_files(name = '*', hook = '*') abort "{{{
  let files = globpath(g:init#vimfiles . '/hooks/', printf('%s_%s.vim', a:name, a:hook), 0, 1)
  return files
endfunction "}}}

" Read plugin hooks from files named after the plugins.
function! s:get_hooks_from_files(hook) abort                              "{{{
  for [plugin, cfg] in items(dein#get())
    let name = cfg.normalized_name
    let lines = []

    " Call hook from pluginhooks#{plugin}_{hook}
    let lines += [ printf('call pluginhooks#call("%s", "%s")', name, a:hook) ]

    " Call hook from hooks/{plugin}_{hook}
    for f in s:get_hook_files(name, a:hook)
      if !filereadable(f)
        echoerr "dein-hooks-from-file: File '".f."' is not readable!"
        continue
      endif
      for line in readfile(f)
        if line !~# '^scriptencoding '
          let lines += [ line ]
        endif
      endfor
    endfor

    " At end of post_source hook, do any User#dein#{hook}#{plugin}
    " autocommands.
    if a:hook ==# 'post_source'
      let lines += [ printf('silent doautocmd <nomodeline> User dein#%s#%s', a:hook, plugin) ]
    endif

    " Install python requirements
    " FIXME done via nix now
    "if a:hook ==# 'post_update'
      "let lines += [printf('call mydein#install_python_requirements("%s")', plugin)]
    "endif

    " Append to any existing hook
    let current = get(cfg, 'hook_' . a:hook, '')
    let new = join(lines, "\n")
    if !empty(new) && new !=# current
      call dein#config(plugin, { 'hook_' . a:hook : current . "\n" . new })
    endif
  endfor
endfunction "}}}


" FIXME Deprecated, should use only nix packages instead.
"function! mydein#get_py_requirements(names = []) abort                    "{{{
"  " Parse python requirements from plugins
"  let plugins = {}
"  if empty(a:names)
"    let plugins = dein#get()
"  elseif type(a:names) == v:t_list
"    for name in a:names
"      let plugins[name] = dein#get(name)
"    endfor
"  elseif type(a:names) == v:t_string
"    let plugins[a:names] = dein#get(a:names)
"  elseif type(a:names) == v:t_dict
"    let plugins[a:names.normalized_name] = a:names
"  else
"    echoerr 'Invalid argument type: ' . string(type(a:names)) . '. Value: ' . string(a:names)
"  endif
"
"  let l:reqs = []
"  for [l:name, l:cfg] in items(plugins)
"    let l:val = get(l:cfg, 'py_requirements', v:null)
"    if type(l:val) ==# type([])
"      let l:reqs += l:val
"    elseif type(l:val) ==# type('')
"      let l:reqs += [l:val]
"    elseif type(l:val) ==# type(v:null)
"    else
"      echoerr 'Failed to parse py_requirements field of: ' . l:name
"    endif
"    for l:file in globpath(l:cfg.path, 'requirements.txt', 0, 1)
"      let l:reqs += readfile(l:file)
"    endfor
"  endfor
"  return uniq(sort(l:reqs))
"endfunction "}}}
"function! mydein#install_python_requirements(names = []) abort            "{{{
"  " Install python requirements for plugins
"  let req = mydein#get_py_requirements(a:names)
"  if !empty(req)
"    echomsg '[init] Installing plugins python requirements: ' . join(req, ', ')
"    call python#pip_install(req, 1)
"  endif
"endfunction "}}}
