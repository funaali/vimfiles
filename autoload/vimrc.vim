" ~/.vim/autoload/vimrc.vim

scriptencoding 'utf-8'

function! vimrc#indent_info() abort
  echom printf('sta:%s sr:%s  <ft:%s> [tw:%d et:%s wrap:%s] (sw:%d ts:%s sts:%s)  +%s+  %s:%s %s:%s:%s:%s [indk:%s] [flp:%s]',
        \ &g:sta ? 'yes' : 'no',
        \ &g:sr  ? 'yes' : 'no',
        \ &l:ft, &l:tw,
        \ &l:et   ? 'yes' : 'no',
        \ &l:wrap ? 'yes' : 'no',
        \ &l:sw,
        \ empty(&l:vts)  ? &l:ts  : &l:vts.' ('.&l:ts.')',
        \ empty(&l:vsts) ? &l:sts : &l:vsts.' ('.&l:sts.')',
        \ &l:commentstring,
        \ &l:fo,
        \ &l:ai ? 'ai' : '',
        \ &l:ci ? 'ci' : '',
        \ &l:si ? 'si' : '',
        \ &l:indentexpr,
        \ &l:lisp ? 'lisp' : '',
        \ &l:indentkeys, &l:formatlistpat)
endfunction

" Modifies all numbered fold markers (open and close) by adding (subtracting)
" a:d to the foldlevel digits. When new marker level would be zero the marker is
" deleted instead.
function! vimrc#fold_renumber(d) range abort
  if ! abs(a:d) > 0
    return
  endif
  let l:save = winsaveview()
  while search('[{}]\{3}\zs\d', 'cWz', a:lastline)
    exe 'normal ' . abs(a:d) . (a:d > 0 ? '' : '')
    call setline('.', substitute(getline('.'), '\s*[{}]\{3}\%#0', '', 'eI'))
  endwhile
  call winrestview(l:save)
endfunction

" Make u undo twice (temporarily) to work around UltiSnip's undo-breaking
" anti-feature.
" Via: https://github.com/noahfrederick/dots/commit/a93dc5f3fe2bb0c140f3ef2e6f9ea9c782e6f098
function! vimrc#install_undo_workaround() abort
  nnoremap <silent><buffer> u :call <SID>undo_workaround()<CR>
endfunction

function! s:undo_workaround() abort
  normal! 2u
  nunmap <buffer> u
endfunction

function! vimrc#insert_snippet(snippet) abort
  if !&l:modifiable
    return
  endif
  if has_key(UltiSnips#SnippetsInCurrentScope(1), a:snippet)
    startinsert!
    exe ':normal! a'.a:snippet."\<C-r>=UltiSnips#ExpandSnippetOrJump()\<CR>"
  endif
endfunction

function! vimrc#insert_skel_snippet(snippet) abort
  if empty(&bt) && byte2line(2) == -1
    call vimrc#insert_snippet(a:snippet)
  endif
endfunction

" Right-align text right to current cursor position.
function! vimrc#ralign() abort
  let l:et = &l:et
  if !l:et
    setl et
  endif
  call search('\s', 'cb', line('.'))
  normal! v0d
  right
  normal! 0gvp
  if !l:et
    setl noet
  endif
endfunction

function! vimrc#get_buffer_list() abort
  redir =>buflist
  silent! ls!
  redir END
  return buflist
endfunction

function! vimrc#toggle_qfix() abort                                       "{{{1
  let qf = getqflist({'winid':1,'size':1})
  if qf.winid
    cclose
  elseif qf.size
    execute 'copen' min([15,max([3,qf.size])])
    wincmd p
  endif
endfunction

function! vimrc#toggle_loclist() abort
  let ll = getloclist(0,{'winid':1,'size':1})
  if ll.winid
    if ll.winid ==# win_getid()
      " XXX noautocmd seems to be important if nohidden is set on the loclist
      " buffer
      noautocmd lclose
    else
      lclose
    endif
  elseif ll.size
    execute 'lopen' min([15,max([3,ll.size])])
    wincmd p
  endif
endfunction

function! vimrc#toggle_syntax() abort                                     "{{{1
  if exists('g:syntax_on')
    syntax off
  else
    syntax enable
  endif
endfunction

function! vimrc#toggle_crap() abort                                          "{{{
  pclose
  cclose
  noautocmd lclose
  helpclose
  if exists(':TagbarClose')
    TagbarClose
  endif
  if exists(':NERDTreeClose')
    NERDTreeClose
  endif
  if exists('*denite#call_map')
    for bufnr in tabpagebuflist()
      for winid in win_findbuf(bufnr)
        if gettabwinvar(0, winid, '&filetype') ==# 'denite'
          call win_execute(winid, 'call denite#call_map("quit")')
          break
        endif
      endfor
    endfor
  endif
endfunction "}}}

function! vimrc#follow_man_keyword(sect = 0, split_type = 'horizontal') abort
  " e.g. "topic(1),"
  let str = expand('<cword>')
  let res = matchlist(str, '\v(\w+)%(\(([0-9][a-z]?)\))?')
  let page = get(res, 1, '')
  let sect = a:sect ? a:sect : get(res, 2, '')
  call man#get_page(a:split_type, sect, page)
endfunction
