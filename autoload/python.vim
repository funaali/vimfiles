" File: ~/.vim/autoload/python.vim

if !has('python3') || !executable('python3')
  finish
endif

" To sandbox python packages for vim, we set (in <url:../rc/init.vim>):
"   PYTHONNOUSERSITE=1
"   PYTHONPATH={cachedir}/python-packages
"
" It's a good idea to nuke the python-packages directory from time to time,
" because pip upgrades leave old versions laying around which slows upgrades
" considerably.

" Set this to true (1) to allow python#pip_install() to install packages in
" the vim python prefix. Python modules are now installed via nix_depends,
" it's a much better way!
let g:python_pip_install_allowed = get(g:,'python_pip_install_allowed',0)

function! python#find_module(module) abort                                "{{{1
  " Checks for existance of a python3 module (not actually importing it).
  py3 import importlib.util
  return py3eval('getattr(importlib.util.find_spec("'.a:module.'"),"origin",None)')
  return v:none
endfunction

function! python#pip_list() abort                                         "{{{1
  return python#pip('list', ['--path', g:myvimrc#pythondir])
endfunction

function! python#pip_install(args, upgrade = 0) abort                     "{{{1
  if empty(a:args) || !g:python_pip_install_allowed
    return
  endif
  let l:args = ['--no-warn-conflicts', '--target', g:myvimrc#pythondir]
  " Do not use binary (wheel) sources (for NixOS in particular)
  let l:args += ['--no-binary', ':all:']
  if a:upgrade
    let l:args += ['--upgrade']
    "let l:args += ['--upgrade', '--upgrade-strategy', 'eager']
  endif
  if type(a:args) ==# type([])
    let l:args += a:args
  else
    let l:args += [a:args]
  endif
  return python#pip('install', l:args, ['--quiet'])
endfunction

function! python#pip(cmd, args=[], opts=[]) abort                         "{{{1
  let l:args = copy(a:opts) + [a:cmd] + copy(a:args)
  call map(l:args, 'shellescape(v:val)')
  let res = system('python3 -m pip '.join(l:args))
  return res
endfunction
