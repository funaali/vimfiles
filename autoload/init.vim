" File: ~/.vim/autoload/init.vim

" Define g:init#vimfiles                                                   {{{
if has('win32')
  let g:init#vimfiles = $HOME . '/vimfiles'
else
  let g:init#vimfiles = $HOME . '/.vim'
endif " }}}

" Directory where nix-managed dependencies get installed.
let g:init#installedDir = g:init#vimfiles . '/.installed'

let s:rc_path = g:init#vimfiles . '/rc/'

function! init#rc(name, required = 1) abort                               "{{{
  " Sources file from ~/.vim/rc/
  let l:file = resolve(s:rc_path . fnamemodify(a:name, ':t') . '.vim')
  if !filereadable(l:file)
    if a:required
      echoerr 'File ' . l:file . ' not readable!'
    endif
    return 0
  endif
  execute 'source' fnameescape(l:file)
endfunction "}}}

" truthy if in vimcat/vimpager mode
function! init#is_pager() abort                                           "{{{
  return get(get(g:, 'vimpager', {}), 'enabled', 0) || get(g:, 'vimcat_mode')
endfunction "}}}

function! init#setup_directories() abort                                  "{{{
  if has('win32')
    call s:set_directory('data', $HOME . '/vim_data')
    call s:set_directory('cache', $HOME . '/vim_cache')
  else
    if empty($XDG_CACHE_HOME)
      let $XDG_CACHE_HOME = $HOME.'/.cache'
    endif
    if empty($XDG_DATA_HOME)
      let $XDG_DATA_HOME = $HOME.'/.local/share'
    endif
    call s:set_directory('data',  $XDG_DATA_HOME  . '/vim')
    call s:set_directory('cache', $XDG_CACHE_HOME . '/vim')
    call s:set_directory('swap',  $XDG_CACHE_HOME . '/vim/swap//')
  end

  " Note: system() does not consider 'shelltemp', so we setup $TMPDIR if it doesn't exist yet.
  if empty($TMPDIR) || $TMPDIR ==# '/tmp'
    if isdirectory($XDG_RUNTIME_DIR) && filewritable($XDG_RUNTIME_DIR)
      let $TMPDIR = $XDG_RUNTIME_DIR.'/vim'
    else
      let $TMPDIR = init#get_directory('tmp', 'cache')
    endif
  endif

  call s:set_directory('tmp', $TMPDIR)
  call s:set_directory('runtime', $TMPDIR)

  " NOTE prefer nix_depends for python installations instead.
  "call s:set_directory('python', init#get_directory('python-packages', 'cache'))
endfunction "}}}

function! s:set_directory(name, value) abort                              "{{{
  if exists('g:myvimrc#{a:name}dir')
    return
  endif
  if !isdirectory(a:value)
    call mkdir(a:value, 'p', '0700')
  endif
  let g:myvimrc#{a:name}dir = a:value
endfunction "}}}

function! init#get_directory(name, type) abort                            "{{{
  let d = get(g:,'myvimrc#'.a:type.'dir','')
  if !empty(d)
    if !empty(a:name)
      let d .= '/' . a:name
    endif
    if !isdirectory(d)
      call mkdir(d, 'p', '0700')
    endif
    return d
  endif
endfunction "}}}

function! init#on(ev) abort                                               "{{{
  if a:ev ==# 'ColorScheme'
    if exists('#QuickFixSignsVscdiff#ColorScheme')
      doautoall <nomodeline> QuickFixSignsVscdiff ColorScheme
    endif
  endif
endfunction "}}}

function! init#redetect_filetype() abort                                  "{{{
  if empty(&l:filetype) || exists('b:ftdetect')
    unlet! b:ftdetect
    filetype detect
  endif
endfunction "}}}

function! init#refresh_tty() abort                                        "{{{
  let term = &term
  let client = empty($TMUX) ? &term :
        \ get(systemlist('tmux list-clients -F "#{client_termname}" -t "\$${TMUX##*,}" 2>/dev/null'), 0, &term)
  call g:Terminfo_setup(term, client)
  if client =~# '\<kitty\>'
    call init#set_cursors('kitty')
  else
    call init#set_cursors(client)
  endif
  " TODO: why?
  hi NonText cterm=NONE ctermfg=NONE
endfunction "}}}

function! init#reset_tty() abort                                          "{{{
  silent! call echoraw(&t_EI) " reset text cursor color
  silent! call echoraw(&t_TE) " ensure we exit raw mode
endfunction "}}}

function! init#set_cursors(tt) abort                                      "{{{
  let colors = extend(copy(g:Terminfo_cursor_colors), exists('g:Terminfo_{a:tt}_cursor_colors') ? g:Terminfo_{a:tt}_cursor_colors : {})
  let shapes = extend(copy(g:Terminfo_cursor_shapes), exists('g:Terminfo_{a:tt}_cursor_shapes') ? g:Terminfo_{a:tt}_cursor_shapes : {})
  " start insert/replace mode
  let &t_SI = init#apply_cursor(shapes.i, colors.i)
  let &t_SR = init#apply_cursor(shapes.r, colors.r)
  " end insert/replace mode
  let &t_EI = get(shapes, 'RESET', init#apply_cursor(shapes.n, '')) .
            \ get(colors, 'RESET', init#apply_cursor('', colors.n))
  silent! call echoraw(&t_EI)
endfunction "}}}

function! init#apply_cursor(style, color) abort                           "{{{
  let r = ''
  if has('cursorshape') && !empty(a:style)
    let r .= substitute(&t_SH, '%p1%d', a:style, '')
  endif
  if !empty(a:color) && !empty(&t_SC) && !empty(&t_EC)
    let r .= &t_SC . a:color . &t_EC
  endif
  return r
endfunction "}}}

" init#tmux_dcs(cmd) : Tmux DCS sequence wrapper                           {{{
" Note: requires 'tmux set-option -w allow-passthrough on'
" "\033Ptmux;\033\033[<u\033\\"
function! init#tmux_dcs(cmd) abort
  return empty($TMUX) ? a:cmd : printf("\<Esc>Ptmux;\<Esc>%s\<Esc>\\", a:cmd)
endfunction "}}}

" init#screen_dcs(cmd) : GNU screen DCS sequence wrapper                   {{{
function! init#screen_dcs(cmd) abort
  return printf("\<Esc>P\<Esc>%s\<Esc>\\", a:cmd)
endfunction "}}}
