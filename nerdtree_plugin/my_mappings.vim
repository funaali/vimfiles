" File: nerdtree_plugin/my_mappings.vim

" Boilerplate (start)                                                      {{{1
if exists('g:loaded_nerdtree_my_mappings')
  finish
endif
let g:loaded_nerdtree_my_mappings = 1

let s:cpo_save = &cpo
set cpo&vim

" Defaults                                                                 {{{1
if !exists('g:NERDTreeMapAddBookmark')
  let g:NERDTreeMapAddBookmark = 'a'
endif

if !exists('g:NERDTreeMapEditBookmarks')
  let g:NERDTreeMapEditBookmarks = '<F4>'
endif

if !exists('g:NERDTreeMapFuzzyOpen')
  let g:NERDTreeMapFuzzyOpen = '<LocalLeader>ff'
endif

function! s:AddBookmark(node) abort                                       "{{{1
  let l:msg = 'New bookmark: ' . a:node.path.str()
        \ . "\nName: "
  let l:name = input(l:msg, a:node.path.getLastPathComponent(0))
  call a:node.bookmark(l:name)
  call b:NERDTree.render()
endfunction

function! s:EditBookmarks(...) abort                                      "{{{1
  call nerdtree#exec('wincmd w', 1)
  call nerdtree#exec('edit '.g:NERDTreeBookmarksFile, 0) " Don't ignore events so custom filetype is detected
endfunction

function! s:FuzzyFind(node) abort                                         "{{{1
  let l:path = a:node.path.isDirectory ? a:node.path : a:node.path.getParent()
  call fzf#run({
        \ 'sink': function('s:OpenPath', [l:path.str()]),
        \ 'options': [],
        \ 'dir': l:path.str()})
endfunction

function! s:OpenPath(root, fname) abort                                   "{{{1
  let l:path = g:NERDTreePath.New(a:root.'/'.a:fname)
  let l:dir  = l:path.isDirectory ? l:path : l:path.getParent()
  let l:node = b:NERDTree.root.New(l:dir, b:NERDTree)
  " Focus to selected directory
  call b:NERDTree.changeRoot(l:node)
  " If selected path is a file, open it too
  if !l:path.isDirectory
    let l:file = l:node.findNode(l:path)
    call l:file.putCursorHere(0, 1)
    call b:NERDTree.render()
    call l:file.openVSplit()
  endif
endfunction

" Maps                                                                     {{{1
call NERDTreeAddKeyMap({
      \ 'key': g:NERDTreeMapAddBookmark,
      \ 'scope': 'Node',
      \ 'callback': function('s:AddBookmark'),
      \ 'quickhelpText': 'AddBookmark' })

call NERDTreeAddKeyMap({
      \ 'key': g:NERDTreeMapEditBookmarks,
      \ 'scope': 'all',
      \ 'callback': function('s:EditBookmarks'),
      \ 'quickhelpText': 'Edit bookmarks' })

call NERDTreeAddKeyMap({
      \ 'key': g:NERDTreeMapFuzzyOpen,
      \ 'scope': 'Node',
      \ 'callback': function('s:FuzzyFind'),
      \ 'quickhelpText': 'Invoke FZF at selection or root' })

" Boilerplate (end)                                                        {{{1
let &cpo = s:cpo_save
unlet s:cpo_save
