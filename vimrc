" ~/.vim/vimrc

" Colorscheme: modified solarized
let g:colors_name = 'flattened_dark'

" Load: Basic setup
call init#rc('init')

" Load: Option defaults
call init#rc('options')

" Load: TTY settings (if output is tty)
if has('ttyout')
  call init#rc('tty')
endif

" Load: packages (pack-add)
packadd! matchit

" Load: custom maps
call init#rc('maps')

" Load: plugins (dein)
call init#rc('plugins')

" Load: Settings for vimpager
if init#is_pager()
  setg hlsearch
endif

" Hack: Make sure colorscheme is set
if get(g:, 'dein#_plugins', v:null) !=# v:null
  execute 'colorscheme ' . g:colors_name
endif
