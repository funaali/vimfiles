if exists('g:loaded_syntastic_nix_nix_linter_checker')
    finish
endif
let g:loaded_syntastic_nix_nix_linter_checker = 1

function! SyntaxCheckers_nix_nix_linter_IsAvailable() dict
    return executable(self.getExec())
endfunction

function! SyntaxCheckers_nix_nix_linter_GetLocList() dict
    let makeprg = self.makeprgBuild({})

    let errorformat =
          \ '%m at %f:%l:%c,' .
          \ '%m at %f:%l:%c-%k'

    return SyntasticMake({
        \ 'makeprg': makeprg,
        \ 'errorformat': errorformat,
        \ 'defaults': {'type':'W'},
        \ 'subtype': 'Style',
        \ 'returns': [0, 1]})
endfunction

call g:SyntasticRegistry.CreateAndRegisterChecker({
    \ 'filetype': 'nix',
    \ 'exec': 'nix-linter',
    \ 'name': 'nix_linter'})
