scriptencoding utf-8

" Docs: <url:vimhelp:syntastic-config-makeprg>
" <url:https://ro-che.info/articles/2020-07-08-integrate-ghcid-vim>
"
" Command Lines: "<exe> <args> <fname> <post_args> <tail>"

" Error Example:
"  /home/sim/.config/xmonad/xmonad.hs:170:20-22: error:
"      • Variable not in scope: dir ∷ FilePath
"      • Perhaps you meant one of these:
"          ‘di’ (line 168), ‘div’ (imported from XMonad.Config.Prime)
" Note: "%.%#" stands for ".*" in errorformat
"
" TODO: make the ghci "worker" process startup automatic.
" TODO: also stop (cleanup) the ghcid processes automatically
" TODO: add timeout to syntastic client check; somehow make the checking
" asynchronous?

if exists('g:loaded_syntastic_haskell_ghcid_checker')
    finish
endif
let g:loaded_syntastic_haskell_ghcid_checker = 1

let s:save_cpo = &cpo
set cpo&vim

" TODO: pass options from vim to the wrapper/ghcid/stack
" if !exists('g:syntastic_ghcid_options')
"   let g:syntastic_ghcid_options = '--restart=stack.yaml --reload=package.yaml' .
"         \ ' --no-height-limit --no-status'
" endif

function! SyntaxCheckers_haskell_ghcid_IsAvailable() dict
  " NOTE: sanity check: stack.yaml exists in cwd.
  return executable(self.getExec())
        \ && file_readable('stack.yaml')
endfunction

function! SyntaxCheckers_haskell_ghcid_GetLocList() dict
  let makeprg = self.makeprgBuild({
        \ 'args':'client'
        \})

  return SyntasticMake({
        \ 'makeprg': makeprg,
        \ 'errorformat':
        \     '%C%*\s• %m,' .
        \     '%-C    (Note:%.%#,' .
        \     '%-C    Use -v%.%#,' .
        \     '%-C    You can%.%#,' .
        \      '%C  %#%m,' .
        \     '%A%f:%l:%c: %t%.%#',
        \ 'returns': [0, 1]})
endfunction

" TODO hardcoded path
 call g:SyntasticRegistry.CreateAndRegisterChecker({
       \ 'filetype': 'haskell',
       \ 'exec': '/home/sim/.vim/syntax_checkers/haskell/ghcid.sh',
       \ 'name': 'ghcid'})
"
let &cpo = s:save_cpo
unlet s:save_cpo
