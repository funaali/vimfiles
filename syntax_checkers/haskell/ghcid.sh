#!/usr/bin/env bash

set -eu

ghci_options=(
	-v1
	-fno-break-on-exception
	-fno-break-on-error
	-fdiagnostics-color=never
	-fno-error-spans
	-fno-print-unicode-syntax
	-fno-diagnostics-show-caret
	-fno-code
	-Wall
	-Werror
)

PID_FILE=$XDG_RUNTIME_DIR/vim-ghcid-syntastic.pid
OUT_FILE=$XDG_RUNTIME_DIR/vim-ghcid-syntastic.out
STATE_ARGS_FILE=$XDG_RUNTIME_DIR/vim-ghcid-syntastic.args
#STATE_WATCHED_FILE=$XDG_RUNTIME_DIR/vim-ghcid-syntastic.files

stop_bg_process(){
	PID=
	while PID=$(cat "$PID_FILE" 2>/dev/null) && ps "$PID" >/dev/null; do
		echo "Running ($PID), stopping..."
		kill "$PID"
		sleep 2
	done
	rm -f "$PID_FILE"
	echo > "$OUT_FILE"
}

start_bg_process(){
	if [[ -e "$PID_FILE" ]]; then
		echo "Error: pid file exists"
		return 1
	fi
	echo "${@@Q}" > "$STATE_ARGS_FILE"
	cmd=(stack --silent --color=never ghci "${ghci_options[@]/#/--ghci-options=}")

	echo "Starting... command: ${cmd[*]@Q}"
	echo $$ > "$PID_FILE"
	echo > "$OUT_FILE"

	ghcid --verbose --color=never \
		-o "$OUT_FILE" \
		--command="${cmd[*]@Q}" \
		--setup=":set -fno-error-spans" \
		"$@" <&- >/tmp/ghcid.log 2>/tmp/ghcid.logerr &

	pid=$(jobs -p)
	echo "$pid" > "$PID_FILE"
	disown -h %+
}

run_client(){
	if [[ "${*@Q}" != $(cat "$STATE_ARGS_FILE") ]]; then
		echo "error: state args are different" >&2
		return 1
	fi

	pid=$(cat "$PID_FILE")

	while [[ $1 -nt $OUT_FILE ]]; do
		if ! ps "$PID" >/dev/null; then
			echo ghcid is not running
			return 1
		fi
		sleep 1
	done

	cat "$OUT_FILE"
}

MODE=$1
shift
case $MODE in
	worker) stop_bg_process; start_bg_process "$@" ;;
	stop) stop_bg_process ;;
	client) run_client "$@" ;;
	auto) : ;; # TODO
	*) echo "unknown cmd $MODE"; exit 2 ;;
esac
