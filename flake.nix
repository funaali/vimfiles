{
  description = "A flake to organize bloated vimfiles";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
  let system = "x86_64-linux";
      pkgs = nixpkgs.outputs.legacyPackages.${system};

      vimLib = import ./nix { inherit nixpkgs system pkgs; };
  in
  {
    packages.${system} = {

      # To generate a plugins.nix
      dein-to-nix = pkgs.writeShellScriptBin "dein-to-nix" ''
        export VIM_PLUGINS_JSON=$HOME/.vim/nix/plugins.json
        export VIM_PLUGINS_NIX=$HOME/.vim/nix/plugins.nix
        ${./nix/dein-to-nix} "$@"
      '';

      install = pkgs.writeShellScriptBin "install-runtime-env" ''
        ${./nix/install-runtime-env} ${vimLib.runtimeEnv} "$@"
      '';

      lib = vimLib;
    };
  };
}
