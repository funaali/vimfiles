" ~/.vim/indent/hcl.vim

if exists('b:did_indent')
  finish
endif

let b:did_indent = 1

function! GetHclIndent() abort
  let prev = prevnonblank(v:lnum - 1)
  let ind  = indent(prev)
  # begin block: {, [ or (
  if getline(prev) =~# '[{[(]$'
    let ind = ind + shiftwidth()
  # end block: }, ] or )
  elseif getline(v:lnum) =~# '\s[}\])]\+$'
    let ind = ind - shiftwidth()
  # begin heredoc: <<EOT
  elseif getline(prev) =~# '\s<<\k\+$'
    let ind = 0
  endif
  return ind
endfunction

setl indentexpr=GetHclIndent()

let b:undo_indent = 'setl indentexpr<'
