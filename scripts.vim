" File: scripts.vim
"                                           <url:vimhelp:new-filetype-scripts>
"                                   <url:vimscript:sp $VIMRUNTIME/scripts.vim>

if did_filetype()
  finish
endif

" Detect Emacs file mode ("-*- mode: <filetype> -*-")
let s:line1 = getline(1)
let s:match = substitute(s:line1, '.*-\*-\s*mode:\s\(\S\+\)\s-\*-.*'.'\|.*', '\1', '')
if !empty(s:match)
  execute 'setfiletype' s:match
endif
