
let cfgHome = fnamemodify($MYVIMRC, ':h:p')

"let requirementsTxt = fnamemodify($MYVIMRC, ':h:p') . '/dein-python-requirements.txt'
"call writefile(mydein#get_py_requirements(), requirementsTxt)

let pluginsO = dein#get() " objects
let pluginsL = dein#util#_get_plugins([]) " list

call writefile([json_encode(pluginsL)], cfgHome . '/plugins.json')
