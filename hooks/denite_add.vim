" Plugin: denite
"
" <url:vimhelp:denite.txt>
" <url:vimhelp:denite-options>
"
" Deoplete source: "denite"

function! s:denite_setup() abort                                                 "{{{
  " Buffer options
  call denite#custom#option('_', 'prompt', '>')

  call denite#custom#option('_', 'statusline', v:false)
  call denite#custom#option('_', 'source-names', 'short')

  "call denite#custom#source('_', 'match-highlight', v:true) " Note: slow

  " Use fruzzy matcher if available
  if exists('g:fruzzy#version')
    call denite#custom#source('_', 'matchers', ['matcher/fruzzy'])
  endif

  " Source file/old
  call denite#custom#source('file/old','converters',['converter/relative_word'])

  " file/rec/git
  call denite#custom#alias('source', 'file/rec/git', 'file/rec')
  call denite#custom#var('file/rec/git', 'command', ['git','ls-files','-co','--exclude-standard'])

  " neoyank
  "call denite#custom#action('source/neoyank', 'my_paste', function('NeoyankMyPaste'))
  "call denite#custom#action('word', 'my_paste',
  "      \ {context -> denite#do_action(
  "      \  context, 'append', context['targets'])})

  if exists('#User#denite_setup')
    doautocmd <nomodeline> User denite_setup
  endif
endfunction "}}}

function! NeoyankMyPaste(ctx) "{{{
  if get(b:,'neoyank_cycle')
    normal! u
  endif
  let b:neoyank_cycle = 1
  let add = 'append' " 'insert'
  echomsg string(a:ctx)

  return denite#do_action(a:ctx, 'append', a:ctx['targets'])

endfunction "}}}

function! s:denite_my_settings() abort                                    "{{{

  " Options
  setlocal signcolumn=no

  " Maps
  nnoremap <silent><buffer><expr>         a       denite#do_map('choose_action')
  nnoremap <silent><buffer><expr>         <CR>    denite#do_map('do_action')
  nnoremap <silent><buffer><expr>         d       denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr>         p       denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr>         v       denite#do_map('do_action', 'vsplit')
  nnoremap <silent><buffer><expr><nowait> s       denite#do_map('do_action', 'split')
  nnoremap <silent><buffer><expr>         t       denite#do_map('do_action', 'tabopen')
  nnoremap <silent><buffer><expr>         C       denite#do_map('do_action', 'cd')
  nnoremap <silent><buffer><expr>         r       denite#do_map('do_action', 'quickfix')
  nnoremap <silent><buffer><expr>         u       denite#do_map('move_up_path')
  nnoremap <silent><buffer><expr>         q       denite#do_map('quit')
  nnoremap <silent><buffer><expr>         '       denite#do_map('quick_move')
  nnoremap <silent><buffer><expr>         i       denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr>         <Space> denite#do_map('toggle_select').'j'
  nnoremap <silent><buffer><expr>         <C-r>   denite#do_map('restore_sources')
endfunction "}}}

function! s:denite_filter_my_settings() abort                             "{{{
  setlocal signcolumn=no
  imap     <silent><buffer>       <C-c> <Plug>(denite_filter_quit)
  "<Plug>(denite_filter_backspace)
  "<Plug>(denite_filter_update)
  "inoremap <silent><buffer><expr> <CR>  denite#do_map('do_action')
endfunction "}}}

function! s:denite_aliases() abort "{{{
  Alias     den           DeniteBufferDir
  Alias     gb            DeniteBufferDir\ buffer
  Alias     gf            DeniteBufferDir\ file
  Alias     denCursor     DeniteCursorWord
  Alias     denProjectDir DeniteProjectDir
endfunction "}}}

" Autocommands                                                            {{{1

augroup myvimrc_denite
  autocmd!
  autocmd FileType denite                  call s:denite_my_settings()
  autocmd FileType denite-filter           call s:denite_filter_my_settings()
  autocmd User     cmdalias_setup          call s:denite_aliases()
  autocmd User     dein#post_source#denite if v:vim_did_enter | call s:denite_setup() | endif
  autocmd VimEnter *                       call s:denite_setup() | autocmd! myvimrc_denite VimEnter
augroup END
