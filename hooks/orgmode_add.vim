
let g:org_indent                   = 0
let g:org_agenda_files             = []
let g:org_aggressive_conceal       = 1
let g:org_todo_keywords            = ['TODO', 'WAITING', '|', 'DONE']
let g:org_todo_keyword_faces       = [['TODO', 'magenta'], ['WAITING',[':foreground magenta',':slant italic']] ]
let g:org_heading_highlight_colors = [ 'Special', 'PreProc', 'Statement', 'Type', 'Identifier', 'Constant', 'Title' ]
