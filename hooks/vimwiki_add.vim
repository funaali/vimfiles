" Plugin: vimwiki                                    <url:vimhelp:vimwiki.txt>
" See Also: taskwiki, ftplugin/vimwiki_maps, after/syntax/vimwiki.vim

scriptencoding utf-8

" Options:
let g:vimwiki_global_ext                = 0 " 1
let g:vimwiki_ext2syntax                = {'.wmd':'markdown'}
let g:vimwiki_hl_headers                = 1 " Use different colors for headers
let g:vimwiki_hl_cb_checked             = 1 " highlight only first line of [X]
let g:vimwiki_html_header_numbering     = 1
let g:vimwiki_html_header_numbering_sym = '.'
let g:vimwiki_folding                   = 'expr' " 'syntax' only for default syntax
let g:vimwiki_listsym_rejected          = '✗'
let g:vimwiki_table_mappings            = 0
let g:vimwiki_url_maxsave               = 40

" Tags:
let g:tagbar_type_vimwiki = {
      \ 'ctagstype'  : 'vimwiki',
      \ 'ctagsbin'   : globpath(&rtp, 'vwtags.py', 0, 1)[0],
      \ 'ctagsargs'  : 'markdown',
      \ 'kinds'      : ['h:header'],
      \ 'sro'        : '&&&',
      \ 'kind2scope' : {'h':'header'},
      \ 'sort'       : 0
      \ }

" Mappings:
let g:vimwiki_map_prefix = '<leader>_ww'

nmap     <silent> <Leader>wi   <Plug>VimwikiIndex
nmap     <silent> <Leader>wti  <Plug>VimwikiTabIndex
nmap     <silent> <Leader>wg   <Plug>VimwikiUISelect
nmap     <silent> <Leader>wdi  <Plug>VimwikiDiaryIndex
nmap     <silent> <Leader>wnd  <Plug>VimwikiMakeDiaryNote
nmap     <silent> <Leader>wnt  <Plug>VimwikiTabMakeDiaryNote
nmap     <silent> <Leader>wno  <Plug>VimwikiMakeYesterdayDiaryNote
nmap     <silent> <Leader>wnf  <Plug>VimwikiMakeTomorrowDiaryNote
nmap     <silent> <Leader>wh   <Plug>Vimwiki2HTML
nmap     <silent> <Leader>whh  <Plug>Vimwiki2HTMLBrowse
"nmap             <NOP>        <Plug>VimwikiDiaryGenerateLinks

" Wiki List:
let g:vimwiki_list = get(g:,'vimwiki_list',[])

let g:vimwiki_wiki_defaults = {
      \ 'template_default'      : 'default',
      \ 'template_ext'          : '.tpl',
      \ 'syntax'                : 'markdown',
      \ 'ext'                   : '.wmd',
      \ 'custom_wiki2html'      : 'vimwiki_markdown',
      \ 'custom_wiki2html_args' : '',
      \ 'html_filename_parameterization': 1,
      \ 'list_margin'           : 0,
      \ 'auto_toc'              : 0,
      \ 'auto_tags'             : 1,
      \ 'auto_diary_index'      : 0,
      \ 'maxhi'                 : 1,
      \ }

function! VimwikiAddWiki(cfg)
  for l:vw in g:vimwiki_list
    if resolve(l:vw.path) ==# resolve(a:cfg.path)
      return
    endif
  endfor
  let g:vimwiki_list += [extend(a:cfg, get(g:,'vimwiki_wiki_defaults',{}))]
endfunction

call VimwikiAddWiki({
      \ 'path'          : '~/Documents/wiki/',
      \ 'path_html'     : '~/Documents/wiki_html/',
      \ 'template_path' : '~/Documents/wiki_templates/',
      \ })

function! s:vimwiki_aliases() abort "{{{
  Alias Windex    VimwikiIndex
  Alias Wmv       VimwikiRenameFile
  Alias Wrm       VimwikiRemoveFile
  Alias Wgo       VimwikiGoto
  Alias Wfind     VimwikiSearch\ /.*/
  Alias Whtml     Vimwiki2HTML
  Alias Wallweb   VimwikiAll2HTML
  Alias Wweb      Vimwiki2HTMLBrowse
  Alias Wtoggle   VimwikiToggleListItem
  Alias W>>       VimwikiListChangeLevel\ >>
  Alias W<<       VimwikiListChangeLevel\ <<
  Alias Wsymbol   VimwikiChangeSymbolTo
endfunction "}}}

augroup myvimrc_vimwiki
  autocmd!
  autocmd User cmdalias_setup call s:vimwiki_aliases()
augroup END
