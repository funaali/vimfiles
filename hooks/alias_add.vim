
" Silently execute system program (:!)
command! -complete=shellcmd -nargs=1 -bang Silent
      \ execute ':silent<bang> !' . <q-args> | execute ':redraw!'

function! s:aliases() abort "{{{
  " Buffers/tabs
  Alias bs      buffers
  Alias detach  tab\ split\ |\ tabprev\ |\ close

  " Writing
  Alias w!! write\ !sudo\ sh\ -c\ 'umask\ 0022\ &&\ tee\ >/dev/null\ "$1"'\ -\ "%"

  " Misc.
  Alias man Man

  " Debug
  Alias setv  verbose\ set
  Alias setlv verbose\ setlocal
  Alias funv  verbose\ function
  Alias comv  verbose\ command

  " Git
  Alias g   Silent\ git
  Alias gg  Silent\ tig\ grep\ -p\ --
  Alias gst Silent\ tig\ status
  Alias gbl Silent\ tig\ blame\ +<c-r>=line('.')<cr>\ --\ %<c-left><c-left><left>
endfunction "}}}

augroup myvimrc_cmdalias
  autocmd!
  autocmd User cmdalias_setup call s:aliases()
augroup END
