" Plugin: quickfixsigns

scriptencoding utf-8

let g:quickfixsigns_classes      = ['qfl', 'loc', 'marks', 'vcsdiff', 'vcsmerge', 'longlines']
let g:quickfixsigns_echo_map     = '<leader>qq'
let g:quickfixsigns_echo_balloon = 1
let g:quickfixsigns#marks#texthl = 'MySignMarks'

" Signs
"
" QFS_QFL: quickfix list entries
" QFS_LOC: location-list entries
" QFS_Mark: marks               <url:vimhelp:autoload/quickfixsigns/marks.vim>
" QFS_CURSOR: cursor position
" QFS_BREAKPOINT: breakpoints
" QFS_REL: relative line numbers
" QFS_VCS: VCS changes        <url:vimhelp:autoload/quickfixsigns/vcsdiff.vim>

sign define QFS_QFL        texthl=MySignWarning     text=║►
sign define QFS_LOC        texthl=Identifier        text=│◊
sign define QFS_QFL_E      texthl=MySignWarning     text=│E
sign define QFS_QFL_W      texthl=MySignWarning     text=│W
sign define QFS_LOC_E      texthl=MySignWarning     text=│E
sign define QFS_LOC_W      texthl=MySignWarning     text=│W
sign define QFS_VCS_ADD    texthl=MySignDiffAdd     text=│+
sign define QFS_VCS_DEL    texthl=MySignDiffDelete  text=│-
sign define QFS_VCS_DELM   texthl=MySignDiffDelete  text=│-
sign define QFS_VCS_DEL1   texthl=MySignDiffDelete  text=-1
sign define QFS_VCS_DEL2   texthl=MySignDiffDelete  text=-2
sign define QFS_VCS_DEL3   texthl=MySignDiffDelete  text=-3
sign define QFS_VCS_DEL4   texthl=MySignDiffDelete  text=-4
sign define QFS_VCS_DEL5   texthl=MySignDiffDelete  text=-5
sign define QFS_VCS_DEL7   texthl=MySignDiffDelete  text=-7
sign define QFS_VCS_DEL8   texthl=MySignDiffDelete  text=-8
sign define QFS_VCS_DEL9   texthl=MySignDiffDelete  text=-9
sign define QFS_VCS_CHANGE texthl=MySignDiffChange  text=│=
sign define QFS_LONGLINES  texthl=MySignLonglines   text=│$

" XXX: these screw up refresh if used in vim inside tmux: 🛑 ⚠️
