" Plugin: windowswap                              <url:vimhelp:windowswap.txt>

scriptencoding utf-8

" Options:
let g:windowswap_map_keys = 0

" Mappings:
nnoremap <silent> <Leader>wy  :<C-u>call WindowSwap#MarkWindowSwap()<CR>
nnoremap <silent> <Leader>wp  :<C-u>call WindowSwap#DoWindowSwap()<CR>
nnoremap <silent> <Leader>ws  :<C-u>call WindowSwap#EasyWindowSwap()<CR>
" Note: Overwrites vim defaults for CTRL-W H …J …K …L
nnoremap <silent> <C-w>H      :<C-u>call <SID>wincmd(v:count1, 'h', 'H')<CR>
nnoremap <silent> <C-w>J      :<C-u>call <SID>wincmd(v:count1, 'j', 'J')<CR>
nnoremap <silent> <C-w>K      :<C-u>call <SID>wincmd(v:count1, 'k', 'K')<CR>
nnoremap <silent> <C-w>L      :<C-u>call <SID>wincmd(v:count1, 'l', 'L')<CR>

" Customizations:

" Mark current window. Execute first argument. Swap if current window is
" marked (new). Otherwise execute second arg.
function! s:wincmd(count, cmd, altcmd) abort
  call WindowSwap#MarkWindowSwap()
  execute a:count.'wincmd '.a:cmd
  if WindowSwap#IsCurrentWindowMarked()
    execute 'wincmd '.a:altcmd
  else
    call WindowSwap#DoWindowSwap()
  endif
endfunction
