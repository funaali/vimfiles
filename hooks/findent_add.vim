" Plugin: findent

" Options:
let g:findent#samples         = [2, 4, 8]
let g:findent#enable_messages = 0
let g:findent#enable_warnings = 0

" Autocmds:
augroup vimrc_findent
  autocmd!
  autocmd User vimrc_after_ftplugin
      \ if &l:modifiable
      \ && empty(&l:buftype)
      \ && empty(&l:shiftwidth)
      \ && empty(&l:vartabstop)
      \ && getfsize(expand('<afile>')) > 0
      \ | Findent
      \ | endif
augroup END
