" Plugin: CamelCaseMotion

function! s:camelcasemotion_setup()
  if exists('g:did_camelcasemotion_setup')
    return
  endif
  let g:did_camelcasemotion_setup = 1
  " Re-define w b e mappings and iw, ib, ie motions
  for l:motion in ['e','b','w']
    execute 'map'      l:motion '<Plug>CamelCaseMotion_'  . l:motion
    execute 'sunmap'   l:motion
    execute 'omap i' . l:motion '<Plug>CamelCaseMotion_i' . l:motion
    execute 'xmap i' . l:motion '<Plug>CamelCaseMotion_i' . l:motion
  endfor
  " Without this, last character is left out in operator-pending mode on the
  " last line
  set virtualedit+=onemore
endfunction

call s:camelcasemotion_setup()
