" Plugin: hexokinase (add) <url:vimhelp:hexokinase.txt>
"
" Requires termguicolors; disable if not available
if !has('termguicolors') || !&termguicolors
  let g:loaded_hexokinase = 1
else
  let g:Hexokinase_highlighters = ['sign_column']
  let g:Hexokinase_signIcon     = '██'
endif
