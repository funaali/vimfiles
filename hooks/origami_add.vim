" Plugin: origami                                        <url:vimhelp:origami>
" NOTE: All settings have buffer-local variants too.

" Options:
let g:OrigamiPadding          = 0 " 0
let g:OrigamiSeparateLevels   = 0 " 0
let g:OrigamiFoldAtCol        = '-3' " 0
let g:OrigamiIncAllLines      = 0 " 0
let g:OrigamiStaggeredSpacing = 0 " 0
let g:OrigamiMap              = {
      \ 'Leader'          : 'Z',
      \ 'Align'           : '',
      \ 'AlignAll'        : '',
      \ 'CommentedOpen'   : '',
      \ 'UncommentedOpen' : '',
      \ 'CommentedClose'  : '',
      \ 'UncommentedClose': '',
      \ 'Delete'          : '',
      \ }

function! s:AlignFoldmarkers(lvl) abort
  if a:lvl >=# 0
    call origami#AlignFoldmarkers(a:lvl)
    silent! call repeat#set("\<Plug>AlignFoldmarkers", a:lvl)
  else
    call origami#AlignFoldmarkers()
    silent! call repeat#set("\<Plug>AlignFoldmarkersAll", -1)
  endif
endfunction

function! s:DeleteFoldmarker() abort
  call origami#DeleteFoldmarker()
  silent! call repeat#set("\<Plug>DeleteFoldmarker", -1)
endfunction

function! s:SetFoldmarker(mode, comment_mode, lvl) abort
  call origami#DeleteFoldmarker()
  call origami#InsertFoldmarker(tolower(a:mode), tolower(a:comment_mode), a:lvl)
  silent! call repeat#set("\<Plug>SetFoldmarker" . a:mode . a:comment_mode, a:lvl)
endfunction

nnoremap <silent> <Plug>AlignFoldmarkers            :<C-U>call <SID>AlignFoldmarkers(v:count)<CR>
nnoremap <silent> <Plug>AlignFoldmarkersAll         :<C-U>call <SID>AlignFoldmarkers(-1)<CR>
nnoremap <silent> <Plug>DeleteFoldmarker            :<C-U>call <SID>DeleteFoldmarker()<CR>
nnoremap <silent> <Plug>SetFoldmarkerOpenNocomment  :<C-U>call <SID>SetFoldmarker('Open', 'Nocomment', v:count1)<CR>
nnoremap <silent> <Plug>SetFoldmarkerOpenComment    :<C-U>call <SID>SetFoldmarker('Open', 'Comment',   v:count1)<CR>
nnoremap <silent> <Plug>SetFoldmarkerCloseNocomment :<C-U>call <SID>SetFoldmarker('Close', 'Nocomment', v:count1)<CR>
nnoremap <silent> <Plug>SetFoldmarkerCloseComment   :<C-U>call <SID>SetFoldmarker('Close', 'Comment',   v:count1)<CR>
