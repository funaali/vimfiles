" File: ~/.vim/rc/options_defx.vim
" Author: Samuli Thomasson
" Description: Options for defx (file explorer)
"   <url:vimhelp:defx>
"   <url:vimhelp:b/defx>

scriptencoding utf-8

" Options                                                            {{{1

function! s:SID()
  return matchstr(expand('<sfile>'), '<SNR>\zs\d\+\ze_SID$')
endfunction

function! s:root(path) abort
  return pathshorten(fnamemodify(a:path, ':~:.'))
endfunction

function! s:defx_setup() abort
  call defx#custom#option('_', {
        \ 'root_marker': ':',
        \ })

  call defx#custom#column('mark', {
        \ 'readonly_icon': '✗',
        \ 'selected_icon': '✓',
        \ })

  call defx#custom#column('icon', {
        \ 'directory_icon':'▸',
        \ 'opened_icon':   '▾',
        \ 'root_icon':     ' ',
        \ })

  call defx#custom#source('file', 'root', '<SNR>' . s:SID() . '_root')
endfunction

" Key mappings                                                       {{{1

nnoremap <silent> <Leader>n     :<C-u>Defx `expand('%:p:h')` -toggle -split=vertical -winwidth=50 -direction=topleft -auto-cd<CR>
nnoremap <silent> <Leader>N     :<C-u>Defx -listed -resume -buffer-name=defx-`tabpagenr()`<CR>

" defx window
function! s:defx_maps() abort
  " move/open
  nnoremap <silent><buffer><expr> j       line('.') == line('$') ? 'gg' : 'j'
  nnoremap <silent><buffer><expr> k       line('.') == 1 ? 'G' : 'k'
  nnoremap <silent><buffer><expr> l       defx#do_action('drop')
  nnoremap <silent><buffer><expr> h       defx#do_action('cd', ['..'])
  nnoremap <silent><buffer><expr> u       defx#do_action('cd', ['..'])
  nnoremap <silent><buffer><expr> ~       defx#do_action('cd')
  nnoremap <silent><buffer><expr> <CR>    defx#is_directory() ? defx#async_action('open_tree', 'toggle') : defx#do_action('drop')
  nnoremap <silent><buffer><expr> O       defx#async_action('open_tree', 'recursive:3')
  " open
  nnoremap <silent><buffer><expr> t         defx#do_action('drop', 'tabnew')
  nnoremap <silent><buffer><expr> v         defx#do_action('open', 'wincmd p <BAR> vsplit')
  nnoremap <silent><buffer><expr><nowait> s defx#do_action('open', 'wincmd p <BAR> split')
  " Misc.
  nnoremap <silent><buffer><expr> <C-l>   defx#do_action('redraw')
  nnoremap <silent><buffer><expr> .       defx#do_action('repeat')
  nnoremap <silent><buffer><expr> <C-g>   defx#do_action('print')
  nnoremap <silent><buffer><expr> q       defx#do_action('quit')
  nnoremap <silent><buffer><expr> C       chdir(input("cd ",defx#get_candidate().action__path,"dir"))
  nnoremap <silent><buffer><expr> <Tab>   winnr('$') != 1 ? ':<C-u>wincmd w<CR>' : ':<C-u>Defx -buffer-name=temp -split=vertical<CR>'
  " Sort/ignored/columns
  nnoremap <silent><buffer><expr> S       defx#do_action('toggle_sort', 'Time')
  nnoremap <silent><buffer><expr> I       defx#do_action('toggle_ignored_files')
  nnoremap <silent><buffer><expr> W       defx#do_action('toggle_columns', 'mark:indent:filename:type:size:time')
  " Actions
  nnoremap <silent><buffer><expr> !       defx#do_action('execute_command')
  nnoremap <silent><buffer><expr> x       defx#do_action('execute_system')
  nnoremap <silent><buffer><expr> d       defx#do_action('remove')
  nnoremap <silent><buffer><expr> m       defx#do_action('move')
  nnoremap <silent><buffer><expr> r       defx#do_action('rename')
  # yank/paste
  nnoremap <silent><buffer><expr> yy      defx#do_action('copy')
  nnoremap <silent><buffer><expr> p       defx#do_action('paste')
  nnoremap <silent><buffer><expr> Y       defx#do_action('yank_path')
  " New dirs/files
  nnoremap <silent><buffer><expr> f       defx#do_action('new_file')
  nnoremap <silent><buffer><expr> F       defx#do_action('new_multiple_files')
  nnoremap <silent><buffer><expr> D       defx#do_action('new_directory')
  " Select
  nnoremap <silent><buffer><expr> <Space> defx#do_action('toggle_select') . 'j'
  nnoremap <silent><buffer><expr> a       defx#do_action('toggle_select_all')
  nnoremap <silent><buffer><expr> A       defx#do_action('clear_select_all')
  xnoremap <silent><buffer><expr> <CR>    defx#do_action('toggle_select_visual')
  " Resize
  nnoremap <silent><buffer><expr> >       defx#do_action('resize', defx#get_context().winwidth + 10)
  nnoremap <silent><buffer><expr> <       defx#do_action('resize', defx#get_context().winwidth - 10)
  " Session
  nnoremap <silent><buffer><expr> R       defx#do_action('add_session')
  nnoremap <silent><buffer><expr> <C-R>   defx#do_action('load_session')
  " Misc
  nnoremap <silent><buffer><expr> <PageUp>   <C-U>
  nnoremap <silent><buffer><expr> <PageDown> <C-D>
endfunction

function! s:open_defx_if_directory()
  " Ensure that we have set our configuration if we are being called at
  " startup.
  if has('vim_starting')
    call s:defx_setup()
  endif
  " This throws an error if the buffer name contains unusual characters like
  " [[buffergator]]. Desired behavior in those scenarios is to consider the
  " buffer not to be a directory.
  try
    let l:full_path = expand(expand('%:p'))
  catch
    return
  endtry
  if isdirectory(l:full_path)
    execute "Defx `expand('%:p')` | bd " . expand('%:r')
  endif
endfunction

augroup myvimrc_defx
  autocmd!
  autocmd User     dein#post_source#defx    call s:defx_setup()
  "autocmd VimEnter     *    call s:defx_setup()
  "autocmd FileType     defx call s:defx_maps()
  "autocmd BufEnter     *    call s:open_defx_if_directory()
  "autocmd BufWritePost *    call defx#redraw()
augroup END
