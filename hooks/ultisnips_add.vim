" File: ~/.vim/rc/options_ultisnips.vim
" Author: Samuli Thomasson
" License: WTFPL
" Description: options_ultisnips

scriptencoding utf-8

let g:UltiSnipsEditSplit                = 'context'
let g:UltiSnipsSnippetDirectories       = ['UltiSnips'] " [$HOME.'/.vim/UltiSnips']
let g:UltiSnipsEnableSnipMate           = 1
let g:UltiSnipsRemoveSelectModeMappings = 1 " <url:vimhelp:UltiSnips-warning-smappings>
"let g:UltiSnipsMappingsToIgnore         = ["UltiSnips"]

let g:UltiSnipsExpandTrigger            = '<TAB>'  " NOTE: UltiSnips (un)maps trigger keys when they have no valid action.
let g:UltiSnipsListSnippets             = '<c-x>?' " <c-tab> not liked by terminal, don't map to that.
let g:UltiSnipsJumpForwardTrigger       = '<TAB>'  " '<TAB>'  Note: can be same as ExpandTrigger.
let g:UltiSnipsJumpBackwardTrigger      = '<S-TAB>'

let g:snips_author    = get(g:,'author_name','(empty)')
let g:default_license = get(g:,'default_license','WTFPL')

augroup myvimrc_ultisnips
  autocmd!
  autocmd User UltiSnipsEnterFirstSnippet let g:in_snippet = 1
  autocmd User UltiSnipsExitLastSnippet   let g:in_snippet = 0
augroup END
