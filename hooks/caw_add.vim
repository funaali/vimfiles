" Plugin: caw

" Options:
let g:caw_operator_keymappings = 1
let g:caw_zeropos_sp           = ' '
let g:caw_dollarpos_sp_left    = ' '

" Mappings:
nmap <silent> C <Plug>(caw:prefix)
xmap <silent> C <Plug>(caw:prefix)
nmap <silent> <Plug>(caw:prefix)A <Plug>(caw:dollarpos:comment)
nmap <silent> <Plug>(caw:prefix)I <Plug>(caw:zeropos:comment)
