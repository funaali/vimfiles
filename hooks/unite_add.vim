" Plugin: unite (add)

function! s:unite_setup() abort
  if !dein#is_sourced('unite')
    call dein#source('unite')
  endif
  if exists('#User#unite_setup')
    doautocmd <nomodeline> User unite_setup
  endif
endfunction

augroup myvimrc_unite
  autocmd!
  autocmd VimEnter * call s:unite_setup()
  autocmd User denite_setup call s:unite_setup()
augroup END
""
"if v:vim_did_enter
"  call s:setup()
"endif
