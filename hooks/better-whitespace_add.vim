" Plugin: better-whitespace                <url:vimhelp:better-whitespace.txt>

" Options:
let g:strip_only_modified_lines     = 1 " 0
let g:strip_max_file_size           = 20000 " 1000
let g:better_whitespace_operator    = '<Leader>W' " <Leader>s
let g:better_whitespace_ctermcolor  = '9' " red
let g:show_spaces_that_precede_tabs = 1 " 0
let g:strip_whitespace_on_save      = 1 " 0
let g:strip_whitelines_at_eof       = 1 " 0
