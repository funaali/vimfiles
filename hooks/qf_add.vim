" vim-qf

" Settings
let g:qf_mapping_ack_style     = 1
let g:qf_window_bottom         = 1
let g:qf_loclist_window_bottom = 0
"let g:qf_statusline            = {}
let g:qf_auto_open_quickfix    = 0
let g:qf_auto_open_loclist     = 0
let g:qf_auto_resize           = 0
let g:qf_max_height            = 16
let g:qf_auto_quit             = 1
let g:qf_bufname_or_text       = 0
let g:qf_save_win_view         = 0
let g:qf_nowrap                = 0
let g:qf_shorten_path          = 3

" Mappings
" Go to prev/next qf or loc location (wraps around)
nmap [l <Plug>(qf_loc_previous)
nmap ]l <Plug>(qf_loc_next)
nmap [q <Plug>(qf_qf_previous)
nmap ]q <Plug>(qf_qf_next)
" Switch between the buffer and location/quickfix window
nmap <leader>qs <Plug>(qf_qf_switch)
