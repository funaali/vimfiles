" Plugin: utl
"
" Manual   <url:vimhelp:utl_usr.txt>
" Commands <url:vimscript:Utl help commands>
" Config   <url:config:>

" Schemas
" ftp:    delegates to http (web browser)
" https:  delegates to http (web browser)
" mailto: delegates to mail client: g:utl_cfg_hdl_scm_mailto
" scp:    uses vim's netrw: g:utl_cfg_hdl_scm_scp
" man:
" vimhelp:
" vimscript:
"   etc.

" Note: vim-pandoc defines mappings like "<localleader>gl" to interact with markdown
" links like [description](url) and <file://.config/foobar/README.md> and so
" on.
"
" Note: This script <url:../after/plugin/utl_uri.vim> fixes some corner
" cases for file: scheme.

"function! s:setup()
"  syn region myUrl matchgroup=myUrlTag start=+\v\c\<(url|lnk):+ end=+>+ oneline containedin=ALL
"  hi def link myUrl Underlined
"endfun

" Options                                                                 {{{1
let g:utl_opt_verbose        = 0
let g:utl_opt_highlight_urls = 'yes' " yes|no

" Handlers                                                                {{{1

function! UtlSetMediaHdl(mt, name, cmd) abort "{{{
  if empty(a:name)
    let g:utl_cfg_hdl_mt_{a:mt} = a:cmd
  else
    let g:utl_cfg_hdl_mt_{a:mt}__{a:name} = a:cmd
  endif
endfunction "}}}

function! UtlSetScmHdl(scm, name, cmd) abort "{{{
  if empty(a:name)
    let g:utl_cfg_hdl_scm_{a:scm} = a:cmd
  else
    let g:utl_cfg_hdl_scm_{a:scm}__{a:name} = a:cmd
  endif
endfunction "}}}

call UtlSetMediaHdl('generic',         '',    "silent !xdg-open '%p'") " The generic handler
call UtlSetMediaHdl('text_directory',  '',    'VIM') " directories: text/directory
call UtlSetMediaHdl('text_directory',  'vim', 'VIM') " <url:/etc> VIM = vim's netrw (or it's substitute)
call UtlSetMediaHdl('text_html',       '',    "!qutebrowser -r default '%u'")
call UtlSetMediaHdl('application_pdf', 'evince', "silent !evince '%u#%f'")

" Schema handlers
call UtlSetScmHdl('mailto',      '',     "!neomutt '%u'")
call UtlSetScmHdl('http',        '',     "silent !xdg-open '%u#%f'")
call UtlSetScmHdl('http_system', '',     "silent !xdg-open '%u#%f'")
call UtlSetScmHdl('http',        'wget', "call Utl_if_hdl_scm_http__wget('%u')")
call UtlSetScmHdl('scp',         '',     'silent %d %u')
