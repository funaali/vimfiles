" Plugin: autoformat

" Options:
let g:autoformat_autoindent             = 0
let g:autoformat_retab                  = 0
let g:autoformat_remove_trailing_spaces = 0

" Custom Formatters:
" let g:formatters_<filetype> = ['<identifier>']
" let g:formatdef_<identifier> = '<command>'
