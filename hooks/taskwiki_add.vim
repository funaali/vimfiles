" Plugin: taskwiki                                  <url:vimhelp:taskwiki.txt>
" See Also: vimwiki

"let g:taskwiki_disable = 1
let g:taskwiki_markup_syntax = 'markdown'
let g:taskwiki_sort_orders = {'E':'end-','M':'modified-'}
let g:taskwiki_dont_preserve_folds = 1
let g:taskwiki_maplocalleader = g:maplocalleader.'t'
"let g:taskwiki_suppress_mappings = 'yes' " Disable default mappings

function! s:taskwiki_aliases() abort "{{{
  Alias     Tann      TaskWikiAnnotate
  Alias     Tinfo     TaskWikiInfo
  Alias     Tinspect  TaskWikiInspect
  Alias     Tedit     TaskWikiMod
  Alias     Tgrid     TaskWikiGrid
  Alias     Tdone     TaskWikiDone
  Alias     Tdelete   TaskWikiDelete
  Alias     Tprojects TaskWikiProjects
  Alias     Tcalendar TaskWikiCalendar
  Alias     Ttags     TaskWikiTags
  Alias     Tread     TaskWikiBufferLoad
  Alias     Twrite    TaskWikiBufferSave
  Alias     Ttag      TaskWikiChooseTag
  Alias     Tpro      TaskWikiChooseProject
  Alias     Tstart    TaskWikiStart
  Alias     Tstop     TaskWikiStop
endfunction "}}}

augroup myvimrc_taskwiki
  autocmd!
  autocmd User cmdalias_setup call s:taskwiki_aliases()
augroup END
