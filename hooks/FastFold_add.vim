" Plugin: FastFold                                      <url:vimhelp:FastFold>

let g:fastfold_fold_command_suffixes  = []
let g:fastfold_fold_movement_commands = []
let g:fastfold_minlines               = 400
let g:fastfold_skip_filetypes         = ['taglist', 'nerdtree']
let g:fastfold_savehook               = 0
let g:fastfold_force                  = 0
