" Plugin: polyglot

" - Disable 'sensible', because more fine-tuned settings are applied anyways.
" - Disable 'autoindent', so polyglot won't try to guess file indentation.
" - Disable language packs for which specialized plugins are used.
"
" - Keep 'ftdetect' enabled despite the double sourcing issue,
"   <https://github.com/sheerun/vim-polyglot/issues/795> because of custom
"   patch in polyglot for it.
"
let g:polyglot_disabled = [
      \ 'sensible', 'autoindent',
      \ 'markdown', 'csv', 'org', 'tmux', 'hcl', 'jinja', 'dosini'
      \ ]

" FileType: haskell
let g:haskell_enable_quantification   = 1 " highlight `forall`
let g:haskell_enable_recursivedo      = 1 " highlight `mdo` and `rec`
let g:haskell_enable_arrowsyntax      = 1 " highlight `proc`
let g:haskell_enable_pattern_synonyms = 1 " highlight `pattern`
let g:haskell_enable_typeroles        = 1 " highlight type roles
let g:haskell_enable_static_pointers  = 1 " highlight `static`
let g:haskell_backpack                = 1 " highlight backpack keywords
