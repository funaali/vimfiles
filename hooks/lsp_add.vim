" Plugin: vim-lsp                                    <url:vimhelp:vim-lsp.txt>
scriptencoding utf-8

" Alternatives:                                                            {{{
"   - <url:https://github.com/natebosch/vim-lsc>
"   - <url:https://github.com/autozimu/LanguageClient-neovim>
"   - <url:https://github.com/neovim/nvim-lspconfig>
"}}}
" Complete:                                                                {{{
"   - <url:vimhelp:deoplete.txt>
"   - <url:https://github.com/Shougo/deoplete-lsp>
"   - <url:https://github.com/lighttiger2505/deoplete-vim-lsp>
"}}}
" Commands:                                                                {{{
" - ":LspDocumentSymbols" populate qf list with all symbols in document.
"}}}
" Configuration (keys)                                                     {{{
"  - cmd (List ex: ['clangd-6.0', '-enable-snippets'])
"  - initialization_options (Dictionary)
"  - allowlist (List)
"  - blocklist (List)
"  - config (Dictionary)
"  - workspace_config (Dictionary)
"  - disabled (Boolean)
"  - root_uri (String)
"  - root_uri_patterns (List)
"  - semantic_highlight (Dictionary)
"}}}
" User autocommands                                                        {{{
"   lsp_complete_done
"   lsp_float_opened
"   lsp_float_closed
"   lsp_register_server
"   lsp_unregister_server
"   lsp_server_init
"   lsp_server_exit
"   lsp_diagnostics_updated
"}}}

" INIT                                                                    {{{1
if !exists('g:lsp_my_servers')
  let g:lsp_my_servers = {}
endif
if !exists('g:lsp_settings')
  let g:lsp_settings = {}
endif

" Options                                                                 {{{1

" vim-lsp
let g:lsp_diagnostics_echo_cursor          = 1
let g:lsp_diagnostics_echo_delay           = 100
let g:lsp_diagnostics_virtual_text_enabled = has('nvim') || has('patch-9.0.0178')
let g:lsp_diagnostics_virtual_text_wrap    = "truncate"
let g:lsp_diagnostics_signs_error          = {'text': '✗'}
let g:lsp_diagnostics_signs_warning        = {'text': '‼'}
let g:lsp_semantic_enabled                 = 1 " Enable semantic highlighting
let g:lsp_log_file                         = g:myvimrc#datadir . '/vim-lsp.log'

" vim-lsp-settings                          <url:vimhelp:vim-lsp-settings.txt>
let g:lsp_settings_enable_suggestions  = 0 " Disable "Please do :LspInstallServer" messages
let g:lsp_settings_servers_dir         = g:myvimrc#datadir . '/lsp-settings/servers'
let g:lsp_settings_global_settings_dir = g:myvimrc#datadir . '/lsp-settings'

" Functions                                                               {{{1

function! s:lsp_setup() abort                                             "{{{
  for server in values(g:lsp_my_servers)
    call lsp#register_server(server)
  endfor
endfunction "}}}

function! s:lsp_add_server(exe, opts, ...) abort                          "{{{
  if !empty(a:exe) && !executable(a:exe)
    return
  endif
  if type(a:opts) ==# v:t_dict
    let cfg = copy(a:opts)
  else
    let cfg = a:0 ? eval(a:opts . join(a:000)) : {}
  endif
  let cfg.name = get(cfg, 'name', a:exe)
  let g:lsp_my_servers[cfg.name] = cfg
endfunction "}}}

" Extra servers                                                           {{{1
" <url:vimhelp:vim-lsp-server_info>

" haskell: haskell-language-server                                        {{{2
call s:lsp_add_server('haskell-language-server-wrapper', {
      \ 'name': 'haskell-language-server-wrapper',
      \ 'cmd': {server_info->['haskell-language-server-wrapper', '--lsp']},
      \ 'whitelist': ['haskell'],
      \ })

" nix: nil                                                                {{{2
" https://github.com/oxalica/nil
call s:lsp_add_server('nil', {
      \ 'name': 'nil',
      \ 'cmd': {server_info->['nil']},
      \ 'whitelist': ['nix'],
      \ })

" Autocommands                                                            {{{1
augroup myvimrc_lsp
  autocmd!
  autocmd User lsp_setup          call s:lsp_setup()
  autocmd User lsp_buffer_enabled call init#rc('lsp_buffer')
augroup END
