" Plugin: editorconfig                          <url:vimhelp:editorconfig.txt>
" Options:                                 <url:vimhelp:editorconfig-settings>
let g:EditorConfig_exclude_patterns       = ['fugitive://.\*'] " []
let g:EditorConfig_preserve_formatoptions = 1 " 0
