

" Use CWD
nnoremap <silent> <leader>S :<C-U>Deol -split<CR>

" Use current buffer directory
nnoremap <silent> <leader>g$ :<C-U>exe 'Deol' '-split' '-cwd='.fnamemodify(expand('%'),':h')<CR>

" Denite chooser
nnoremap <silent> <leader>gS :<C-u>Denite deol<CR>

" Exit terminal mode with <esc>
" XXX broken with kitty
"tnoremap <Esc> <C-\><C-n>

" Customize mappings
let g:deol#custom_map = {
      \ 'edit'               : 'e',
      \ 'start_insert'       : 'i',
      \ 'start_insert_first' : 'I',
      \ 'start_append'       : 'a',
      \ 'start_append_last'  : 'A',
      \ 'execute_line'       : '<CR>',
      \ 'previous_prompt'    : '<C-p>',
      \ 'next_prompt'        : '<C-n>',
      \ 'paste_prompt'       : '<C-y>',
      \ 'bg'                 : '<C-z>',
      \ 'quit'               : 'q',
      \ }

" External history
let g:deol#shell_history_path = g:myvimrc#datadir . '/deol_history'

" Prompt pattern
"let g:deol#prompt_pattern = '% \|%$'
