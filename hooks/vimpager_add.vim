" Plugin: vimpager
" Options:
"   g:vimpager: gvim X11 passthrough ansiesc
"   g:less: enabled(1) scrolloff(5) hlsearch(1) number(0) tail statusfunc

let g:vimpager = get(g:, 'vimpager', {})
let g:less     = get(g:, 'less', {})
let g:less.enabled   = 0 " 1
let g:less.scrolloff = 0 " 5
