" Plugin: NERDTree                                  <url:vimhelp:NERDTree.txt>
"
" Global Commands:                        <url:vimhelp:NERDTreeGlobalCommands>
" Bookmark Commands:                    <url:vimhelp:NERDTreeBookmarkCommands>

scriptencoding utf-8

" Options:                                                                {{{1
"                                               <url:vimhelp:NERDTreeSettings>
let g:NERDTreeUseTCD                = 1 " 0
let g:NERDTreeChDirMode             = 3 " 0
let g:NERDTreeIgnore                = ['\~$', '\.tags', '^result$[[dir]]'] " ['\~$']
let g:NERDTreeRespectWildIgnore     = 1 " 0
let g:NERDTreeShowBookmarks         = 1 " 0
let g:NERDTreeBookmarksSort         = 2 " 1
let g:NERDTreeBookmarksFile         = get(g:,'myvimrc#datadir',$HOME).'/NERDTreeBookmarks' " $HOME/.NERDTreeBookmarks
let g:NERDTreeSortOrder             = ['\/$', '*', '[[-timestamp]]']
let g:NERDTreeNaturalSort           = 1 " 0
let g:NERDTreeMinimalUI             = 1 " 0
let g:NERDTreeCascadeSingleChildDir = 0 " 1
let g:NERDTreeAutoDeleteBuffer      = 1 " 0
let g:NERDTreeCreatePrefix          = 'silent keepalt keepjumps' " 'silent'

" Behavior of <CR>
let g:NERDTreeCustomOpenArgs = {
      \ 'file': {'reuse' : 'currenttab', 'where':'v', 'keepopen':1, 'stay':0},
      \ 'dir' : {}}

" Mappings:                                                               {{{1
"                                               <url:vimhelp:NERDTreeMappings>
" See Also:                      <url:../nerdtree_plugin/my_mappings.vim>

let g:NERDTreeMapOpenVSplit = 'v'
let g:NERDTreeMapOpenSplit  = '<nowait> s' " Note: <nowait> b/c oper-surround maps start with s, too
let g:NERDTreeMapHelp       = '<F1>' " '?'
let g:NERDTreeMapCWD        = '<LocalLeader>cd' " 'CD' (conflicting prefix)

nnoremap <silent> <Leader>n  <Cmd>execute 'NERDTreeToggle' fnameescape(getcwd())<CR>
nnoremap <silent> <Leader>NP <Cmd>NERDTreeCWD<CR>
nnoremap <silent> <Leader>NV <Cmd>NERDTreeToggleVCS<CR>
map      <silent> <Leader>N% <Plug>(NERDTreeFindEx)

" NERDTreeFindEx: Find current file in  NERDTree "{{{
function! s:NERDTreeFindEx(itemStr) abort
  let l:file = empty(a:itemStr) ? expand('%:p') : a:itemStr
  try
    let l:cwd = g:NERDTreePath.New(getcwd())
  catch /^NERDTree./
    return
  endtry
  call NERDTreeFocus()
  if !b:NERDTree.root.path.equals(l:cwd)
    let l:newRoot = g:NERDTreeFileNode.New(l:cwd, b:NERDTree)
    call b:NERDTree.changeRoot(l:newRoot)
  endif
  exe 'NERDTreeFind' fnameescape(l:file)
  "if g:NERDTree.IsOpen()
  "  g:NERDTree.CursorToTreeWin()
  "else
  "  g:NERDTreeCreator.ToggleTabTree()
  "endif
endfunction
noremap <silent> <Plug>(NERDTreeFindEx) :<C-U>call <SID>NERDTreeFindEx('')<CR>
"}}}

" Plugin: nerdtree-git-plugin                                             {{{1
"
"     \ 'Modified'  :'✹',
"     \ 'Staged'    :'✚',
"     \ 'Untracked' :'✭',
"     \ 'Renamed'   :'➜',
"     \ 'Unmerged'  :'═',
"     \ 'Deleted'   :'✖',
"     \ 'Dirty'     :'✗',
"     \ 'Ignored'   :'!',
"     \ 'Clean'     :'✔︎',
"     \ 'Unknown'   :nr2char(120744),

let g:NERDTreeGitStatusShowClean          = 1 " default: 0
let g:NERDTreeGitStatusShowIgnored        = 1 " default: 0
let g:NERDTreeGitStatusConcealBrackets    = 1 " default: 0
let g:NERDTreeGitStatusIndicatorMapCustom = {
      \ 'Unknown'   :'?',
      \ }
let g:NERDTreeGitStatusHighlightingCustom = {
      \ 'Modified'  :'cterm=NONE ctermfg=1  ctermbg=NONE gui=NONE guifg=#dc322f guibg=NONE',
      \ 'Staged'    :'cterm=NONE ctermfg=1  ctermbg=NONE gui=NONE guifg=#dc322f guibg=NONE',
      \ 'Dirty'     :'cterm=NONE ctermfg=1  ctermbg=NONE gui=NONE guifg=#dc322f guibg=NONE',
      \ 'Ignored'   :'cterm=NONE ctermfg=10 ctermbg=NONE gui=NONE guifg=#586e75 guibg=NONE',
      \ 'Clean'     :'cterm=NONE ctermfg=2  ctermbg=NONE gui=NONE guifg=#859900 guibg=NONE',
      \ }
