" Floaterm
"
" Integrations with other plugins:
"  FZF:
"    :Floaterms
"  Denite:
"    Sources: floaterms
"    Actions: open (default) new preview
"    Example: :Denite floaterm:new -auto-action=preview
"  Ranger:

scriptencoding utf-8

let g:floaterm_shell = $SHELL

" FZF
"nnoremap <Leader>ff <Cmd>FloatermNew

"autocmd User FloatermOpen        " triggered after opening a new/existed floaterm

function! s:floaterm_syntax() abort "{{{
  "" Set floaterm window background to gray once the cursor moves out from it
  hi FloatermNC guibg=gray

  " Set floaterm window's background to black
  hi Floaterm guibg=black

  " Set floating window border line color to cyan, and background to orange
  hi FloatermBorder guibg=orange guifg=cyan
endfunction "}}}

augroup myvimrc_floaterm
  autocmd!
  autocmd ColorScheme * call :floaterm_syntax()
  "autocmd Syntax floaterm call s:floaterm_syntax()
augroup END
