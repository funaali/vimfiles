" Plugin: syntastic                       <url:vimhelp:syntastic-checkers.txt>
"                                                  <url:vimhelp:syntastic.txt>
scriptencoding utf-8

" NOTES                                                                   {{{1
" Checkers:                                    g:syntastic_<filetype>_checkers
" Extra FileTypes:                                 g:syntastic_extra_filetypes
" Checker Options:                   g:syntastic_{filetype}_{checker}_{option}
"                                               g:syntastic_{checker}_{option}
"   exec, args, post_args, tail, fname, sort, quiet_messages
" Buffer Vars:
"   b:syntastic_mode        = "…"     override syntastic_mode_map in the buffer
"   b:syntastic_checkers    = [...]   override checkers in the buffer
"   b:syntastic_skip_checks = 1       disables all checks in the buffer
" Note: To show available/enabled checkers for a given filetype:
"   :SyntasticInfo <filetype>

" INIT                                                                    {{{1
let g:syntastic_extra_filetypes = get(g:, 'syntastic_extra_filetypes', [])

" HELPERS                                                                 {{{1

function! MySyntasticSetCheckers(filetype, ...)                           "{{{
  let g:syntastic_{a:filetype}_checkers = flatten(copy(a:000))
  if index(g:SyntasticRegistry.getKnownFiletypes(), a:filetype) < 0
    call add(g:syntastic_extra_filetypes, a:filetype)
  endif
endfunction "}}}

function! g:MySyntasticAddChecker(ft, checker, ...)                       "{{{
  let cfg = a:0 ? eval(join(a:000)) : {}
  for [key,val] in items(cfg)
    let g:syntastic_{a:ft}_{a:checker}_{key} = val
  endfor
  let checkers = copy(get(g:, 'syntastic_'.a:ft.'_checkers', []))
  call add(checkers, a:checker)
  call MySyntasticSetCheckers(a:ft, checkers)
endfunction " }}}

" COMMANDS                                                                {{{1

command! -nargs=+ SyntasticSetCheckers call MySyntasticSetCheckers(<f-args>)
command! -nargs=+ SyntasticAddChecker  call MySyntasticAddChecker(<f-args>)

" OPTIONS                                                                 {{{1

" Automatically open/close location-list
" Options: 0:none 1:both 2:close only 3:open only
" Default: 2
" XXX: The "open" settings (1 and 3) makes commands such as ":close" etc.
" produce noise from the "#BufEnter" command that follows right after, where
" ":lopen" throws E242 <url:vimhelp:E242>.
"   A workaround is to use "SyntasticLoclistHide()" i.e. "silent! lclose".
"   Alternatively use ":SyntasticSetLocList", ":lopen" etc.
let g:syntastic_auto_loc_list = 2

" https://no-color.org/
let g:syntastic_shell = '/usr/bin/env NO_COLOR=1 /bin/sh'

" Automatically update location-list when checkers run
" Default: 0
let g:syntastic_always_populate_loc_list = 1

" TODO
let g:syntastic_check_on_open            = 1 " 0
let g:syntastic_check_on_wq              = 0 " 1
let g:syntastic_aggregate_errors         = 1 " 0
let g:syntastic_sort_aggregated_errors   = 0 " 1 (don't leave errors grouped by checker)
let g:syntastic_enable_highlighting      = 0 " 1 (use highlighting to mark errors)
let g:syntastic_auto_jump                = 0 " 0:no auto (default), 1:jump any, 2:jump errors first, 3:jump errors only
let g:syntastic_ignore_files             = ['\m^/usr/'] " []
let g:syntastic_enable_signs             = 0 " 1 (overlap with the QFS plugin)
let g:syntastic_reuse_loc_lists          = 0 " 1 (undocumented)
let g:syntastic_nested_autocommands      = 1 " 0

" CHECKERS                                                                {{{1

function! s:syntastic_setup() "{{{
  " Haskell: ghcid - checker is local, see <url:./syntax_checkers/haskell/ghcid.vim>
  " Vim: vint: needs pip install vim-vint (pacman -S vint)
  "   Note: ignore parse warning crap
  " Sh: shellcheck
  let g:syntastic_sh_shellcheck_args = '--external-sources'
  SyntasticSetCheckers help      vimhelplint
  SyntasticSetCheckers python    python pylint
  SyntasticSetCheckers yaml      yamllint       " yamllint <url:vimhelp:syntastic-yaml-yamllint>
  SyntasticSetCheckers make      gnumake        " From syntastic-extras
  SyntasticSetCheckers yaml      pyyaml         " From syntastic-extras
  SyntasticSetCheckers gitcommit proselint      " From syntastic-extras
  SyntasticSetCheckers json      json_tool      " From syntastic-extras
  SyntasticSetCheckers dosini    dosini         " From syntastic-extras
  SyntasticSetCheckers cfg       cfg            " From syntastic-extras
  SyntasticSetCheckers haskell   hlint
  SyntasticAddChecker  haskell   ghcid
  SyntasticSetCheckers java                     " Disabled (prefer vim-lsp)
  SyntasticAddChecker  vim       vint   {'post_args':'--json 2>/dev/null'}
  SyntasticAddChecker  nix       nix_linter
endfunction "}}}

" HOOKS                                                                   {{{1

" CheckHook: Location list callback. Called right before populating the location-list.
function! SyntasticCheckHook(errors)                                    "{{{
  if !empty(a:errors)
    let height  = min([len(a:errors), 15])
    let do_show = get(g:,'syntastic_my_show',2)
    if do_show == 0
      " Never show
    elseif do_show == 1
      " Show loc-lists, every file window (multiple qf windows)
      "exe 'lwindow' height
      " XXX lwindow doesn't work here because the loclist is only populated
      " after this checkhook
      exe 'lopen' height
    elseif do_show == 2
      let ll = getloclist(0, {'all':0})
      " Show loc-lists, single qf window per tabpage
        " getwinvar(0, 'syntastic_loclist_set', [])
      for wid in gettabinfo(0)[0].windows
        if getwinvar(wid, '&buftype') ==# 'quickfix'
          call setloclist(wid, a:errors, 'r')
          return
        endif
      endfor
      " open
      exe 'botright lopen' height
      wincmd p
    else
      echoerr 'Unknown value for syntastic_my_show: ' . do_show
    endif
  endif
endfunction " }}}

" AUTOCOMANDS                                                             {{{1
augroup myvimrc_syntastic
  autocmd!
  autocmd User dein#post_source#syntastic call s:syntastic_setup()
augroup END
