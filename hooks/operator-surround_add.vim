" Plugin: operator-surround

let g:operator#surround#blocks = get(g:, 'operator#surround#blocks', {})

let s:tilde_bl    = {'block':["~~~\n","\n~~~"],'motionwise':['line', 'block'],'keys':['~']}
let s:backtick_bl = {'block':["```\n","\n```"],'motionwise':['line', 'block'],'keys':['`']}

call extend(g:operator#surround#blocks, {
      \ 'markdown':[s:tilde_bl, s:backtick_bl],
      \ 'pandoc'  :[s:tilde_bl, s:backtick_bl],
      \ 'vimwiki' :[s:tilde_bl, s:backtick_bl],
      \ 'snippets':[]
      \ })
