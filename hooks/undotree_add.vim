" Plugin: undotree

"" Layout
let g:undotree_CustomUndotreeCmd  = 'vertical 32 new'
let g:undotree_CustomDiffpanelCmd = 'belowright 12 new'
let g:undotree_SplitWidth         = 32

"let g:undotree_TreeNodeShape = '*'
"let g:undotree_ShortIndicators

" Mappings
"   function g:Undotree_CustomMap()
"   ...

" Various
let g:undotree_HelpLine = 0
