" Plugin: airline                                    <url:vimhelp:airline.txt>
"  Theme: <url:vimhelp:airline-themes>
"         <url:vimhelp:airline-themes.txt>

scriptencoding utf-8

" INIT                                                                    {{{1
let g:airline_filetype_overrides = get(g:, 'airline_filetype_overrides', {})
let g:airline_extensions         = get(g:, 'airline_extensions', [])
let g:airline_symbols            = get(g:, 'airline_symbols', {})
let g:airline_mode_map           = get(g:, 'airline_mode_map', {})
let g:airline_exclude_filenames  = get(g:, 'airline_exclude_filenames', [])
let g:airline_exclude_filetypes  = get(g:, 'airline_exclude_filetypes', [])

" HELPERS                                                                 {{{1

" CUSTOM PARTS & PLUGINS                                                  {{{1

function! s:airline_my_parts()                                            "{{{
  call airline#parts#define_function('pwd', 'AirlineMyPartPwd')
  call airline#parts#define_function('myfiletype', 'AirlineMyPartFiletype')
  call airline#parts#define_function('snippet', 'AirlineMyPartInSnippet')
  call airline#parts#define_raw('denite-input', '%{denite#get_status("input")}')
  " Accents
  call airline#parts#define_accent('tagbar', 'red')
endfunction "}}}

function! AirlineMyPartFiletype()                                         "{{{
  return (airline#util#winwidth() < 45 && strlen(&filetype) > 3) ? matchstr(&filetype, '...'). (&encoding is? 'utf-8' ? '…' : '>') : &filetype
endfunction "}}}

function! AirlineMyPartPwd()                                              "{{{
  let l:win = fnamemodify(getcwd(),':p:~')
  let l:tab = fnamemodify(getcwd(-1,0),':p:~')
  return l:win ==# l:tab ? l:win : printf('%s (tab: %s)', l:win, l:tab)
endfunction "}}}

function! AirlineMyPartInSnippet()                                        "{{{
  return get(g:,'in_snippet',0) ? 'Snippet' : ''
endfunction "}}}

" GENERAL OPTIONS                                                         {{{1

let g:airline_inactive_collapse   = 1 " collapse left section of inactive windows (1)
let g:airline_inactive_alt_sep    = 0 " use alternative separators in inactive windows (1)
let g:airline_detect_iminsert     = 0 " (0)
let g:airline_exclude_preview     = 0 " don't modify stl of preview window (0)
let g:airline_skip_empty_sections = 0 " no separators for empty sections (active window) (1)
let g:airline_highlighting_cache  = 1 " cache hl group changes (0)
let g:airline_focuslost_inactive  = 0 " disable airline on FocusLost autocmd (1)

let g:airline#ignore_bufadd_pat = '!|defx|gundo|nerd_tree|startify|tagbar|term://|undotree|vimfiler'

let g:airline#parts#ffenc#skip_expected_string = 'utf-8[unix]'

let g:airline#extensions#disable_rtp_load = 1

" Which sections truncate at which window width
let g:airline#extensions#default#section_truncate_width = {
      \'b':20,
      \'x':60,
      \'y':88,
      \'z':45,
      \'warning':80,
      \'error':80,
      \}

" SYMBOLS                                                                 {{{1
let g:airline_powerline_fonts     = 1
let g:airline_symbols_ascii       = 0
let g:airline_left_sep           = ''
let g:airline_left_alt_sep       = ''
let g:airline_right_sep          = ''
let g:airline_right_alt_sep      = ''
let g:airline_symbols.branch     = ''
let g:airline_symbols.crypt      = '🔒'
let g:airline_symbols.dirty      = '⚡'
let g:airline_symbols.linenr     = ''
let g:airline_symbols.maxlinenr  = '¶'
let g:airline_symbols.notexists  = 'Ɇ'
let g:airline_symbols.paste      = 'Þ'
let g:airline_symbols.readonly   = ''
let g:airline_symbols.spell      = 'Ꞩ'
let g:airline_symbols.whitespace = 'Ξ'

" Modes                                                                   {{{2
let g:airline_mode_map['__']    = '-'
let g:airline_mode_map['c']     = 'C'
let g:airline_mode_map['i']     = 'I'
let g:airline_mode_map['ic']    = 'I*'
let g:airline_mode_map['ix']    = 'X'
let g:airline_mode_map['multi'] = 'M'
let g:airline_mode_map['n']     = 'N'
let g:airline_mode_map['ni']    = 'iN'
let g:airline_mode_map['no']    = 'O'
let g:airline_mode_map['R']     = 'R'
let g:airline_mode_map['Rv']    = 'vR'
let g:airline_mode_map['s']     = 'S'
let g:airline_mode_map['S']     = 'Ss'
let g:airline_mode_map['']    = 'Sc'
let g:airline_mode_map['t']     = 'T'
let g:airline_mode_map['v']     = 'V'
let g:airline_mode_map['V']     = 'L'
let g:airline_mode_map['']    = 'B'

" THEME                                                                   {{{1
" Theme (solarized)                                                       {{{2
let g:airline_theme                              = 'solarized' " (dark)
let g:airline_theme_patch_func                   = 'AirlineThemePatch'
let g:airline_solarized_normal_green             = 1 " (0)
let g:airline_solarized_dark_inactive_background = 1 " (0)
let g:airline_solarized_dark_text                = 0 " (0)
let g:airline_solarized_dark_inactive_border     = 1 " (0)
let airline_solarized_enable_command_color       = 1

function! AirlineThemePatch(palette)                                      "{{{
  " Indices: 0:guifg 1:guibg 2:ctermfg 3:ctermbg
  if g:airline_theme =~# '\v^(base16_|)solarized$'
    let a:palette.inactive.airline_a[0] = '#eee8d5'
    let a:palette.inactive.airline_a[1] = '#586e75'
    let a:palette.inactive.airline_a[3] = 10
    " Accents
    let a:palette.accents.blue = ['#268bd2', '#073642', 4, '0', '']
  endif
endfunction "}}}

" SECTIONS                                                                {{{1
" <url:vimhelp:airline.txt#  |airline-default-sections>

function! s:MyAirlineCustomize() "{{{
  call airline#add_statusline_funcref('s:MyStlParts'->function())
  "call airline#add_statusline_func('MyCustomStl')
endfunction "}}}

function! s:MyStlParts(...) abort "{{{
  let w:airline_section_a = airline#section#create_left(['mode', 'crypt', 'paste', 'spell', 'iminsert', 'snippet'])
  let w:airline_section_c = airline#section#create_left(['file']) " bufferline#get_status_string()
  let w:airline_section_x = airline#section#create_right(['tagbar', 'myfiletype'])
endfunction "}}}

" Override statusline for some filetypes
function! MyCustomStl(...)                                   "{{{
  if &l:filetype ==# 'denite'
    let w:airline_section_a = 'Denite'
    let w:airline_section_b = '%{denite#get_status("sources")}'
    "let w:airline_section_c = airline#section#create(['%{denite#get_status("input")}'])
    let w:airline_section_y = ''
  elseif &l:filetype ==# 'denite-filter'
    let w:airline_section_a    = airline#section#create_left(['mode', 'paste'])
    let w:airline_render_right = 0
  endif
  return 0
endfunction "}}}

" FILETYPE OVERRIDES                                                      {{{1

function! s:airline_override(...)                                         "{{{
  " The g:airline_filetype_overrides can be used to override sections a and b.
  if a:0 <= 1 " Print
    echohl Title
    echomsg printf('%-10s %-30s %s', 'filetype', 'left', 'right')
    echohl None
    for ft in a:0 == 0 ? keys(g:airline_filetype_overrides) : [a:1]
      let sections = get(g:airline_filetype_overrides, ft)
      if !empty(sections)
        echomsg printf('%-10s %-30s %s', ft, sections[0], sections[1])
      endif
    endfor
  elseif a:0 == 3 " set left + right
    let g:airline_filetype_overrides[a:1] = [a:2, a:3]
  else " unknown
    echoerr 'Unknown number of arguments'
  endif
endfunction "}}}
command! -nargs=* AirlineOverride call s:airline_override(<f-args>)

function! AirlineQuickfixStatus(what)                                     "{{{
  " First Argument: title, cmd, filename
  let qfwinid = getqflist({'winid':0}).winid
  if a:what ==# 'title'
    return qfwinid == win_getid() ? 'QF' : 'LL'
  elseif a:what ==# 'cmd'
    return get(w:,'quickfix_title','')
  elseif a:what ==# 'filename'
    let filewinid = getloclist('',{'filewinid':0}).filewinid
    return filewinid > 0 ? bufname(winbufnr(filewinid)) : ''
  endif
endfunction "}}}

AirlineOverride help      Help   %f
AirlineOverride vimfilter vimfilter %{vimfilter#get_status_string()}
AirlineOverride vimshell  vimshell  %{vimshell#get_status_string()}
AirlineOverride qf        %{AirlineQuickfixStatus('title')} %{AirlineQuickfixStatus('cmd')}
AirlineOverride nerdtree  /      %{exists('b:NERDTree')?pathshorten(fnamemodify(b:NERDTree.root.path.str(),':p:~'),3):''}
AirlineOverride tagbar    Tagbar %{tagbar#currentfile()}
AirlineOverride defx      Defx   %{b:defx.paths[0]}

" EXTENSIONS                                                              {{{1

function! EnableAirlineExtension(ext, ...)                                "{{{
  " Set settings
  let cfg = (a:0 == 0 || empty(a:1))
        \ ? {} : (type(a:1) == v:t_dict) ? a:1 : eval(a:1)
  for [l:key, l:val] in items(cfg)
    let g:airline#extensions#{a:ext}#{l:key} = l:val
  endfor
  " Enable
  if index(g:airline_extensions, a:ext) < 0
    call add(g:airline_extensions, a:ext)
  endif
  let g:airline#extensions#{a:ext}#enabled = 1
  " init
  if v:vim_did_enter && get(g:,'loaded_airline')
    if exists('#airline')
      if index(airline#extensions#get_loaded_extensions(), a:ext) < 0 && !exists('#myvimrc_airline#CursorHold')
        au myvimrc_airline CursorHold ++once ++nested call MyAirlineReset()
      endif
    endif
  endif
endfunction "}}}
command! -nargs=+ AirlineEnableExtension
      \ call EnableAirlineExtension(matchstr(<q-args>,'\S*'), matchstr(<q-args>,'\s\zs.*'))

AirlineEnableExtension searchcount

AirlineEnableExtension wordcount
      \ {'filetypes':['asciidoc','help','mail','markdown','pandoc','vimdoc','org','rst','tex','text']}

AirlineEnableExtension whitespace {
      \ 'show_message'             : 0,
      \ 'checks'                   : ['indent', 'trailing', 'mixed-indent-file', 'conflicts'],
      \ 'long_format'              : 'long[%s]',
      \ 'trailing_format'          : 'trailing[%s]',
      \ 'mixed_indent_format'      : 'mixed-indent[%s]',
      \ 'mixed_indent_file_format' : 'mixed-indent-file[%s]',
      \ 'conflicts_format'         : 'conflicts[%s]',
      \}

" TABLINE                                                                 {{{1

function! MyTabTitleFormatter(n) abort                                    "{{{
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  let bufnr = buflist[winnr - 1]
  let winid = win_getid(winnr, a:n)
  let title = bufname(bufnr)

  if empty(title)
    if getqflist({'qfbufnr' : 0}).qfbufnr == bufnr
      let title = '[Quickfix List]'
    elseif winid && getloclist(winid, {'qfbufnr' : 0}).qfbufnr == bufnr
      let title = '[Location List]'
    else
      let title = '[--]'
    endif
  else
    let ft = getbufvar(bufnr, '&filetype')
    if ft ==# 'tagbar'
      let title = '[tagbar]'
    elseif ft ==# 'nerdtree'
      let title = '[nerdtree]'
    elseif ft ==# 'help'
      let title = '[help|'.fnamemodify(title, ':t:r').']'
    endif
  endif

  return title
endfunction "}}}

"function! MyAirlineTabnrFormatter(tab_nr_type, nr)                        "{{{
"  let spc=g:airline_symbols.space
"  let tabpwd = fnamemodify(getcwd(-1,a:nr),':p:~')
"  return spc. a:nr . ':' . tabpwd " . '.%{len(tabpagebuflist('.a:nr.'))}'
"endfunction "}}}

AirlineEnableExtension tabline {
      \ 'show_tabs'         : 1,
      \ 'show_tab_count'    : 1,
      \ 'show_tab_nr'       : 1,
      \ 'show_tab_type'     : 1,
      \ 'show_buffers'      : 1,
      \ 'show_splits'       : 1,
      \ 'show_close_button' : 0,
      \ 'excludes'          : [],
      \ 'exclude_preview'   : 1,
      \ 'alt_sep'           : 0,
      \ 'buf_label_first'   : 1,
      \ 'buffers_label'     : 'buf',
      \ 'tabs_label'        : 'tab',
      \ 'overflow_marker'   : '…',
      \ 'current_first'     : 0,
      \ 'buffer_nr_show'    : 1,
      \ 'buffer_nr_format'  : '%s:',
      \ 'buffer_min_count'  : 0,
      \ 'tab_min_count'     : 0,
      \ 'fnamemod'          : ':p:~:.',
      \ 'fnamecollaps'      : 0,
      \ 'fnametruncate'     : 16,
      \ 'formatter'         : 'unique_tail_improved',
      \ 'tabtitle_formatter': 'MyTabTitleFormatter',
      \}
      "\ 'tabnr_formatter'   : 'MyAirlineTabnrFormatter',
      "\ 'tab_nr_type'       : 2,
      "\ 'keymap_ignored_filetypes': ['vimfiler', 'nerdtree', 'tagbar', 'quickfix'],
      "\ 'ignore_bufadd_pat' : '\c\vundotree|vimfiler|tagbar|nerd_tree',

" AUTOCOMANDS                                                             {{{1

function! MyAirlineEnable()                                               "{{{
  call s:airline_my_parts()
  call s:MyAirlineCustomize()
  "call airline#init#sections()
endfunction "}}}

function! MyAirlineReset()                                                "{{{
  unlet! g:airline_section_a g:airline_section_b g:airline_section_c g:airline_section_x g:airline_section_y g:airline_section_z g:airline_section_error g:airline_section_warning
  au! User AirlineToggledOn
  silent! call airline#extensions#load()
  call s:MyAirlineCustomize()
  call airline#init#sections()
  call airline#highlighter#reset_hlcache()
  call airline#util#doautocmd('AirlineAfterInit')
endfunction "}}}

function! MyAirlineHandle(ev)                                             "{{{
  if !get(g:,'loaded_airline_settings') && get(g:,'loaded_airline')
    let loaded_airline_settings = 1
    call s:airline_my_parts()
  endif
  if !get(g:,'loaded_airline_settings')
    return
  endif

  if a:ev ==# 'AirlineAfterInit'

  elseif a:ev ==# 'AirlineAfterTheme'
    hi airline_cwd ctermfg=7 ctermbg=11 guifg=#eee8d5 guibg=#657b83

  "elseif a:ev ==# 'AirlineBeforeRefresh'

  elseif a:ev ==# 'AirlineToggledOn'
    if v:vim_did_enter
      if exists('#airline#WinEnter')
        let ct = tabpagenr()
        tabdo doautoall <nomodeline> airline WinEnter
        exe 'tabnext' ct
      endif
      AirlineRefresh
    endif

  elseif a:ev ==# 'AirlineToggledOff'
    set tabline&
    au! User AirlineToggledOn " from tabline
    for t in gettabinfo()
      for w in t.windows
        call settabwinvar(t.tabnr, w, '&statusline', &l:stl)
      endfor
    endfor
  endif
endfunction "}}}

augroup myvimrc_airline
  autocmd!
  autocmd User AirlineAfterInit             call MyAirlineHandle(expand('<afile>'))
  autocmd User AirlineAfterTheme            call MyAirlineHandle(expand('<afile>'))
  autocmd User AirlineToggledOn             call MyAirlineHandle(expand('<afile>'))
  autocmd User AirlineToggledOff            call MyAirlineHandle(expand('<afile>'))
  autocmd User dein#post_source#airline     call MyAirlineEnable()
  autocmd User dein#post_source#bufferline  AirlineEnableExtension bufferline
  autocmd User dein#post_source#fugitive    AirlineEnableExtension branch
  autocmd User dein#post_source#zoomwintab  AirlineEnableExtension zoomwintab
  autocmd User dein#post_source#syntastic   AirlineEnableExtension syntastic
  autocmd User dein#post_source#taskwarrior AirlineEnableExtension taskwarrior
  autocmd User dein#post_source#undotree    AirlineEnableExtension undotree
  autocmd User dein#post_source#unicode     AirlineEnableExtension unicode
  autocmd User dein#post_source#bookmarks   AirlineEnableExtension bookmark
  autocmd User dein#post_source#denite      AirlineEnableExtension denite
  autocmd User dein#post_source#tagbar      AirlineEnableExtension tagbar {'flags':'s'}
  autocmd User dein#post_source#lsp         AirlineEnableExtension lsp
  "autocmd FileType help,man silent! call airline#extensions#whitespace#disable() " TODO
augroup END
