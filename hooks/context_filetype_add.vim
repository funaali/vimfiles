" <url:vimhelp:context_filetype-interface>

if !exists('g:context_filetype#filetypes')
  let g:context_filetype#filetypes = {}
endif

" let g:context_filetype#filetypes.toml =
"       \[{'filetype':'vim',
"       \  'start': '\(''\{3}\)',
"       \  'end': '\1'
"       \}]

" [{'filetype':'vim', 'start' : "=\\s*'\\{3}\\zs", 'end' : "'\\{3}"}]
