" Plugin: tagbar                                      <url:vimhelp:tagbar.txt>
" Wiki:                        <url:https://github.com/majutsushi/tagbar/wiki>

scriptencoding utf-8

" Options:                                                                {{{1
let g:tagbar_left                 = 0  " 0 (window position)
let g:tagbar_width                = 50 " 40
let g:tagbar_show_linenumbers     = 0  " 0
let g:tagbar_autoshowtag          = 1  " 0
let g:tagbar_autopreview          = 1  " 0
let g:tagbar_autoclose            = 1  " 0
let g:tagbar_autofocus            = 0  " 0
let g:tagbar_case_insensitive     = 0  " 0
let g:tagbar_compact              = 1  " 0 (omit help text)
let g:tagbar_indent               = 1  " 2
let g:tagbar_show_data_type       = 1  " 0 (display data-type right of the tag)
let g:tagbar_show_visibility      = 1  " 1
let g:tagbar_visibility_symbols   = {'public':'+','protected':'#','private':'-'}
let g:tagbar_show_tag_linenumbers = 1 " 0
let g:tagbar_singleclick          = 1 " 0
let g:tagbar_foldlevel            = 2 " 99
let g:tagbar_iconchars            = ['▸', '▾'] " ['▶', '▼']
let g:tagbar_autoshowtag          = 1 " 0
let g:tagbar_previewwin_pos       = 'topleft'
let g:tagbar_autopreview          = 1 " 0
let g:tagbar_silent               = 1 " 0
let g:tagbar_use_cache            = 0 " 1 (invoke ctags on tmp file. NOTE: breaks network filesystems and such if disabled)
let g:tagbar_wrap                 = 0 " 0
let g:tagbar_file_size_limit      = 2 * 1024 * 1024 " 0 (bytes)
let g:tagbar_jump_lazy_scroll     = 1 " 0
let g:tagbar_no_status_line       = 1 " (use airline)

" Mappings:                                                               {{{1
let g:tagbar_map_help          = '<F1>' " <F1>, ?
let g:tagbar_map_hidenonpublic = 'I' " v
let g:tagbar_map_zoomwin       = 'A' " x

" Functions                                                               {{{1
function! s:TagbarOpenOk() abort
  if &l:buftype != '' || &l:previewwindow ==# 1 || &l:ft ==# '' || &l:ft ==# 'tagbar'
    return 0
  elseif tagbar#inspect('init_done') ==# 1 && empty(get(tagbar#inspect('known_types'),&l:ft,{})) " unsupported filetype
    return 0
  elseif winwidth(0) + winwidth(bufwinid(get(t:,'tagbar_buf_name',-1))) < get(g:,'tagbar_width',40) + max([78, &l:tw])
    return 0
  endif
  return 1
endfunction

function! MyTagbarAutoOpen() abort
  if s:TagbarOpenOk()
    call tagbar#autoopen(1)
    if has('vim_starting')
      TagbarForceUpdate
    endif
  endif
endfunction

function! MyTagbarPreviewSetup() abort
  if &l:previewwindow && exists('t:tagbar_state') && get(t:tagbar_state.getCurrent(0),'fpath') ==# expand('<afile>:p')
      setlocal number norelativenumber
      let t:pwin_by_tagbar = 1
  endif
endfunction

function! MyTagbarPreviewClose() abort
  if !&l:previewwindow && &l:ft != "tagbar" && mode() == "n" && get(t:,'pwin_by_tagbar',0)
    pclose
    unlet! t:pwin_by_tagbar
  endif
endfunction

function! MyTagbarHighlight()                                             "{{{
  hi link TagbarSignature Type
endfunction "}}}

" Autocommands:                                                           {{{1

augroup myvimrc_tagbar
  autocmd!
  " Extra highlights
  autocmd Syntax tagbar :call MyTagbarHighlight()

  " Auto-open&auto-close Tagbar when changing file
  autocmd BufWinEnter * nested :call MyTagbarAutoOpen()

  " Set options for preview window and auto-close preview window when leaving Tagbar buffer
  autocmd WinEnter  * call MyTagbarPreviewSetup()
  autocmd SafeState * call MyTagbarPreviewClose()
augroup END

" Extra File Types:                                                       {{{1
"                                                  <url:vimhelp:tagbar-extend>
" g:tagbar_type_{filetype} = {...}
"
" key "kinds" (list): "{short}:{long}[:{fold}[:{stl}]]"
"   - {short}: one-character abbr ctags uses
"   - {long}: arbitrary string tagbar uses
"   - {fold}: folded by default?
"   - {stl}: shown in statusline?

" Haskell: hasktags                                                       {{{2
let g:haskell_tagbar_hasktags = {
      \ 'ctagstype' : 'haskell',
      \ 'ctagsbin'  : 'hasktags',
      \ 'ctagsargs' : '-x -c -o-',
      \ 'kinds'     : [
          \ 'm:modules:0:1',
          \ 'd:data:1:1',
          \ 'd_gadt:data gadt:0:1',
          \ 't:type names:0:1',
          \ 'nt:new types:0:1',
          \ 'c:classes:0:1',
          \ 'i:class instances:1:1',
          \ 'cons:constructors:1:1',
          \ 'c_gadt:constructor gadt:1:1',
          \ 'c_a:constructor accessors:1:1',
          \ 'ft:function types:1:1',
          \ 'fi:function implementations:1:1',
          \ 'o:others:0:1'
      \ ],
      \ 'sro'        : '.',
      \ 'kind2scope' : {
          \ 'm' : 'module',
          \ 'c' : 'class',
          \ 'd' : 'data',
          \ 't' : 'type'
      \ },
      \ 'scope2kind' : {
          \ 'module' : 'm',
          \ 'class'  : 'c',
          \ 'data'   : 'd',
          \ 'type'   : 't'
      \ },
      \ 'sort' : 0
  \ }

" Haskell: fast-tags                                                      {{{2
let g:haskell_tagbar_fasttags = {
      \ 'ctagsbin'  : 'fast-tags',
      \ 'ctagsargs' : '-o-',
      \ 'kinds'     : [
          \  'm:modules:0:0',
          \  'c:classes:0:1',
          \  't:types:0:1',
          \  'C:constructors:0:1',
          \  'p:patterns:0:1',
          \  'o:operators:0:1',
          \  'f:functions:0:1'
      \ ],
      \ 'sro':'.',
      \ 'kind2scope':{'m':'module','c':'class','d':'data','t':'type'},
      \ 'scope2kind':{'module':'m','class':'c','data':'d','type':'t'}
      \ }

" Haskell: (default)                                                      {{{2
let g:tagbar_type_haskell = g:haskell_tagbar_hasktags
