" Plugin: deoplete                                  <url:vimhelp:deoplete.txt>

" NOTES                                                                   {{{1
" Sorters: default: ['sorter_rank']
" Converts: ...
" Sources: sources, ignore_sources              <url:vimhelp:deoplete-sources>
" Options:                                      <url:vimhelp:deoplete-options>
" Keyword Patterns: py3 regex + \k for iskeyword pattern (by <filetype>)
" Omni Patterns:
"   Note: When omni_patterns is set, deoplete calls omnifunc as soon as a
"   pattern is matched. Circumvents the usual source filtering. The omni
"   source won't be used.
" Source Options:
"   mark, rank, min_pattern_length,
"   matchers, converters,
"   max_abbr_width, max_menu_width
"   filetypes (enable for these filetypes only),
"   disabled_syntaxes (disable candidates for these syntax items)

" HELPERS                                                                 {{{1
let s:cfg    = {} " -> deoplete#custom#option(opt, value)         (options)
let s:source = {} " -> deoplete#custom#source(source, opt, value) (source specialized options)
let s:var    = {} " -> deoplete#custom#var(source, var, value)    (source specialized variables)

function! s:apply()                                                       "{{{
  for [l:opt, l:value] in items(s:cfg)
    call deoplete#custom#option(l:opt, l:value)
  endfor
  for [l:source, l:dict] in items(s:source)
    for [l:opt, l:value] in items(l:dict)
      call deoplete#custom#source(l:source, l:opt, l:value)
    endfor
  endfor
  for [l:source, l:dict] in items(s:var)
    for [l:var, l:value] in items(l:dict)
      call deoplete#custom#var(l:source, l:var, l:value)
    endfor
  endfor
endfunction "}}}

function! s:option(opt, ...)                                              "{{{
  " s:option(opt, value)
  " s:option(opt, filetype, value
  if a:0 == 1
    let s:cfg[a:opt]      = a:1
  elseif a:0 == 2
    let s:cfg[a:opt]      = get(s:cfg,a:opt,{})
    let s:cfg[a:opt][a:1] = a:2
  else
    echoerr 'unexpected number of arguments'
  endif
endfunction "}}}

function! s:source(source, opt, value)                                    "{{{
  let s:source[a:source]        = get(s:source,a:source,{})
  let s:source[a:source][a:opt] = a:value
endfunction "}}}

function! s:var(source, ...)                                              "{{{
  " s:var(source, dict)
  " s:var(source, var, value)
  " s:var(source, var, filetype, value)
  let s:var[a:source] = get(s:var,a:source,{})
  if a:0 == 1
    call extend(s:var[a:source], a:1)
  elseif a:0 == 2
    let s:var[a:source][a:1] = a:2
  elseif a:0 == 3
    let s:var[a:source][a:1]      = get(s:var[a:source],a:1,{})
    let s:var[a:source][a:1][a:2] = a:3
  else
    echoerr 'unexpected number of arguments'
  endif
endfunction "}}}

" GLOBAL OPTIONS                                                          {{{1
call s:option('auto_complete_delay', 200) " 20
call s:option('auto_refresh_delay',  250) " 100
call s:option('camel_case',          v:true) " v:false
call s:option('candidate_marks',     ['A', 'B', 'C', 'D', 'E']) " []
call s:option('max_list',            200) " 500
call s:option('num_processes',       2) " 1
call s:option('skip_chars',          []) " ['(', ')']

" FILETYPE OPTIONS                                                        {{{1
call s:option('omni_patterns',    'haskell',   ['^\s*{-#\s*LANGUAGE.*', '^\s*{-#\s*OPTIONS_GHC.*', '^\s*import.*'])
call s:option('sources',          'gitcommit', ['github'])
call s:option('keyword_patterns', 'gitcommit', '.+')
"call s:option('omni_patterns',    'terraform', ['[^ *\t"{=$]\w*'])

" FILETYPE OPTION DEFAULTS                                                {{{1
call s:option('sources',          '_', []) " load all eligible sources by default
call s:option('keyword_patterns', '_', '[a-zA-Z_]\k*')

" SOURCE RANKS                                                            {{{1
call s:source('around',     'rank', 300) " [A]
call s:source('buffer',     'rank', 100) " [B]
call s:source('file',       'rank', 150)
call s:source('member',     'rank', 100)
call s:source('tag',        'rank', 250)  " [T]
call s:source('dictionary', 'rank', 100)  " [D]  uses &dictionary
call s:source('ultisnips',  'rank', 1000) " [US]
call s:source('tabnine',    'rank', 200)  " [TN]
call s:source('webcomplete','rank', 150)  " [web]
call s:source('tmux',       'rank', 150)  " [tmux]
call s:source('vim',        'rank', 500)  " [vim] - for ft=vim
call s:source('jedi',       'rank', 500)  " [jedi] - for ft=python
call s:source('omni',       'rank', 500)

call s:source('lsp',        'rank', 900)
call s:var('lsp', 'max_num_results', 50)

" SOURCE DEFAULTS                                                         {{{1
call s:source('_', 'matchers', [empty(globpath(&rtp,'bin/cpsm_py.so')) ? 'matcher_fuzzy' : 'matcher_cpsm'])

" SOURCE VARIABLES                                                        {{{1
call s:var('buffer', 'require_same_filetype', v:false)
call s:var('omni', 'input_patterns', 'gitcommit', '.+')
call s:var('tmux', 'max_num_results', 20)
call s:var('tabnine', {'line_limit':5000,'max_num_results':10})

" SETUP                                                                   {{{1
function! s:deoplete_maps()                                               "{{{
  inoremap <expr> <C-G>      deoplete#undo_completion()
  inoremap <expr> <CR>       deoplete#close_popup() . '<CR>'
  inoremap <expr> <C-B><C-G> deoplete#close_popup()
  for s:mark in s:cfg.candidate_marks
    exe 'inoremap <expr> <C-B>'.s:mark." pumvisible() ? deoplete#insert_candidate('".s:mark."') : '".s:mark."'"
  endfor
  " Note also standard complete mappings e.g. <C-y>: select candidate
endfunction "}}}

function! s:deoplete_setup()                                              "{{{
  " Run once
  autocmd! myvimrc_deoplete InsertEnter
  " Settings required for omni-completion via deoplete to work.
  set completeopt+=noselect completeopt+=noinsert
  " Apply customizations & enable
  silent doautocmd <nomodeline> User deoplete-setup
  call deoplete#enable()
endfunction "}}}

function! s:deoplete_buffer_setup()                                       "{{{
  if !exists('*deoplete#is_enabled') || !deoplete#is_enabled()
    return
  endif
  " Don't complete special buffers
  if !empty(&l:bt)
    call deoplete#custom#buffer_option('auto_complete', v:false)
  endif
endfunction "}}}

" AUTOCOMMANDS                                                            {{{1
augroup myvimrc_deoplete
  autocmd!
  autocmd User deoplete-setup call s:apply()
  autocmd InsertEnter * call s:deoplete_setup()
  autocmd BufEnter    * call s:deoplete_buffer_setup()
augroup END
