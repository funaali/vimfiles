" Plugin: bookmarks                                <url:vimhelp:bookmarks.txt>

scriptencoding utf-8

" Options                                                            {{{1

let g:bookmark_auto_save_file          = get(g:, 'myvimrc#datadir', $HOME) . '/bookmarks'
let g:bookmark_auto_save               = 1   " load and save whenever buffer changes
let g:bookmark_no_default_key_mappings = 1
let g:bookmark_highlight_lines         = 1   " Highlight lines with bookmarks
let g:bookmark_center                  = 1   " Vertical line centering when jumping to bmark
let g:bookmark_disable_ctrlp           = 1
let g:bookmark_sign                    = '♥'
let g:bookmark_annotation_sign         = '♥'

" Highlights etc.                                                    {{{1

function! s:bookmarks_highlight()
  highlight BookmarkSign           ctermfg=6 ctermbg=0 guifg=#2aa198 guibg=#073642
  highlight BookmarkAnnotationSign ctermfg=1 ctermbg=0 guifg=#cb4b16 guibg=#073642
  highlight link BookmarkLine           CursorLine
  highlight link BookmarkAnnotationLine CursorLine
endfunction

function! s:bookmarks_setup_unite()
  call unite#custom#profile('source/vim_bookmarks', 'context', {
  \ 'profile-name':'vim_bookmarks',
  \ 'direction':'dynamictop',
  \ 'auto-resize':1,
  \ 'start_insert':0,
  \ 'no_quit':0,
  \ 'keep_focus':0,
  \ 'immediately':1,
  \ 'auto-preview':1,
  \ 'auto-highlight':1,
  \ 'hide-source-names':1,
  \ })
endfunction

function! s:bookmarks_denite_highlight()
  syn match BookmarksHead     /vim_bookmarks:/ contained containedin=deniteSource_unite nextgroup=BookmarksLocation
  syn match BookmarksLocation /[^|]\+/         contained
  hi link BookmarksHead NonText
  hi link BookmarksLocation Identifier
endfunction

function! s:bookmarks_setup_denite()
  call denite#custom#alias('source', 'bookmark', 'unite')
  call denite#custom#source('bookmark', {'args':['vim_bookmarks']})
endfunction

" Autocmds                                                           {{{1

augroup myvimrc_bookmarks
  autocmd!
  autocmd ColorScheme * call s:bookmarks_highlight()
  autocmd FileType denite
        \ autocmd SafeState <buffer=abuf> ++once call s:bookmarks_denite_highlight()
  autocmd User unite_setup  call s:bookmarks_setup_unite()
  autocmd User denite_setup call s:bookmarks_setup_denite()
augroup END
