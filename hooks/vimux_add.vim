
" ",vp" prompt for command to run
" ",r." rerun last command
"
" ",rs" build Haskell project
"
" ",vC" close the runner
"
nnoremap <leader>rp :VimuxPromptCommand<CR>
nnoremap <Leader>rs :call VimuxRunCommand("clear; stack build --fast --pedantic")<CR>
nnoremap <leader>r. :VimuxRunLastCommand<CR>
nnoremap <leader>rC :VimuxCloseRunner<CR>
nnoremap <leader>rz :VimuxZoomRunner<CR>


" <url:vimhelp:vimux>
"
let g:VimuxHeight = '30'

