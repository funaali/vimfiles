" Plugin: bufferline <url:vimhelp:bufferline.txt>

let g:bufferline_echo                = 0
let g:bufferline_active_buffer_left  = '['
let g:bufferline_active_buffer_right = ']'
let g:bufferline_modified            = '+'
let g:bufferline_show_bufnr          = 1
let g:bufferline_rotate              = 0 " 0, 1 or 2
let g:bufferline_fixed_index         = 1 " 0, 1 or -1
let g:bufferline_fname_mod           = ':.' " <url:vimhelp:filename-modifiers>
let g:bufferline_inactive_highlight  = 'StatusLineNC'
let g:bufferline_active_highlight    = 'StatusLine'
let g:bufferline_solo_highlight      = 0
let g:bufferline_pathshorten         = 1 " pathshorten() paths in buffer names?
let g:bufferline_separator           = ' '

let g:bufferline_excludes = ['^$']
