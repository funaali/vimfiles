" Plugin: fzf                                                <url:vimhelp:fzf>

function! s:fzf_set_qf(lines) abort "{{{
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction "}}}

function! s:fzf_action() abort "{{{
  for k in keys(g:fzf_action)
    echo k . ':  ' . string(g:fzf_action[k])
  endfor
endfunction "}}}

" FZF Actions:
let g:fzf_action = {
      \ 'ctrl-t': 'tab split',
      \ 'ctrl-x': 'split',
      \ 'ctrl-v': 'vsplit',
      \ 'ctrl-q': function('s:fzf_set_qf'),
      \ }

" FZF Layout:
" Note: tmux layout requires tmux 3.2 or above
if exists('$TMUX')
  let g:fzf_layout = { 'tmux': '-p90%,60%' }
else
  " Popup window
  let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true } }
  " - Window using a Vim command
  "let g:fzf_layout = { 'window': 'enew' }
  " down
  "let g:fzf_layout = { 'down': '60%' }
endif

" Commands:
command! FZFAction call s:fzf_action()

" Maps:
nnoremap <leader>ff <Cmd>FZF<CR>

" Misc:
let g:fzf_history_dir = g:myvimrc#datadir . '/fzf-history'
