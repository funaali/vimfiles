" Plugin: unicode                                    <url:vimhelp:unicode.txt>
" Example Search: /<CTRL-R>=unicode#Regex(unicode#Search('euro'))<cr>

" Options:
let g:Unicode_data_directory      = g:myvimrc#datadir " . 'UnicodeData.txt'
let g:Unicode_cache_directory     = g:myvimrc#cachedir
let g:Unicode_no_default_mappings = v:true
let g:Unicode_ShowPreviewWindow   = 1
"let g:Unicode_fuzzy_color       = 123

" Mappings:
" Replace built-in "ga" with unicode version
nmap ga         <Plug>(UnicodeGA)
" Browse and choose a glyph with FZF
imap <C-B><C-U> <Plug>(LoadFZF).<Plug>(UnicodeFuzzy)
" Complete
imap <C-X><C-G> <Plug>(DigraphComplete)
imap <C-X><C-Z> <Plug>(UnicodeComplete)
imap <C-X><C-B> <Plug>(HTMLEntityComplete)
" Toggle completion
nmap <leader>uc <Plug>(UnicodeSwapCompleteName)
" Make digraph from chars (operator)
nmap <leader>ud <Plug>(MakeDigraph)
vmap <leader>ud <Plug>(MakeDigraph)

inoremap <script><silent> <Plug>(LoadFZF) <C-\><C-O>:call dein#source('fzf')<CR>
