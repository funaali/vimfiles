" Plugin: vim-pandoc                                  <url:vimhelp:vim-pandoc>
"   Note: Configuration variables            <url:vimhelp:vim-pandoc-settings>
" Plugin: vim-pandoc-syntax                    <url:vimhelp:vim-pandoc-syntax>
" Plugin: vim-pandoc-after                      <url:vimhelp:vim-pandoc-after>
"   Note: will always integrate with Goyo. Default enabled modules is empty.

" SYNTAX                                                                  {{{1
let g:pandoc#syntax#conceal#use                = 1
let g:pandoc#syntax#conceal#blacklist          = []
let g:pandoc#syntax#conceal#cchar_overrides    = {}
let g:pandoc#syntax#conceal#urls               = 1
let g:pandoc#syntax#codeblocks#ignore          = ['delimited'] " ['definition', 'delimited']
let g:pandoc#syntax#codeblocks#embeds#use      = 1
let g:pandoc#syntax#codeblocks#embeds#langs    = ['yaml', 'ruby', 'haskell', 'sshconfig', 'help', 'systemd', 'bash=sh']
let g:pandoc#syntax#style#emphases             = 1
let g:pandoc#syntax#style#underline_special    = 1
let g:pandoc#syntax#style#use_definition_lists = 1

" FILETYPES                                                               {{{1
let g:pandoc#filetypes#handled                 = ['pandoc', 'markdown']
let g:pandoc#filetypes#pandoc_markdown         = 1

" MODULES                                                                 {{{1
let g:pandoc#modules#disabled                  = []
let g:pandoc#after#modules#enabled             = ['deoplete', 'fastfold', 'nrrwrgn', 'ultisnips']

" FORMATTING                                                              {{{1
let g:pandoc#formatting#mode                   = 'ha' " h/a/A/s

" FOLDING                                                                 {{{1
let g:pandoc#folding#mode                      = 'stacked'
let g:pandoc#folding#fastfolds                 = 1
let g:pandoc#folding#fold_fenced_codeblocks    = 1
let g:pandoc#folding#fold_yaml                 = 1

" KEYBOARD                                                                {{{1
let g:pandoc#keyboard#use_default_mappings     = 0

" SPELL                                                                   {{{1
let g:pandoc#spell#default_langs = ['en']

" COMPLETION (DEOPLETE)                                                   {{{1
function! s:pandoc_deoplete() "{{{
  call deoplete#custom#var('omni', 'input_patterns', {'pandoc': '@'})
endfunction "}}}

" Autocommands                                                            {{{1
augroup myvimrc_pandoc
  autocmd!
  autocmd User deoplete-setup call s:pandoc_deoplete()
  "autocmd User dein#post_source#deoplete call s:pandoc_deoplete()
augroup END
