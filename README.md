# My vimfiles (`~/.vim`)

## Installation

Checkout as `~/.vim`. Plugins should install automatically on first launch.

Update plugins:

```bash
scripts/dein-update
```

## Snippets

- Plugin: `UltiSnips`
- Custom snippets in `UltiSnips`
- `UltiSnips/all.snippets` is loaded for all filetypes.

## Plugins

Almost all plugins are installed with dein.

```vim
# Set early to skip installing dein if it's not present.
let myvimrc#dein_only_cached = 1
```

Hooks:
- add:         After line is parsed.
- source:      Before plugins are sourced. Lazy plugins only!
- post_source: After plugins are sourced.
- post_update: After plugins are updated.
- done_update: After all plugins are updated.

### Plugin Nix Dependencies

Declare dependencies to be installed with Nix in `nix_depends` on the plugin's
TOML configuration.

1.  Install dein and plugins inside vim (new host only):

    ```bash
    # Open vim and install dein and plugins
    ```

2.  Extract plugins.json and plugins.nix:

    ```bash
    nix run .#dein-to-nix
    ```

3.  Install runtime deps and configure vim to use them. Bonus: update upstream
    dependencies too.

    ```bash
    nix flake update
    nix run .#install
    ```

4.  Run setup hooks for individual plugins

    ```bash
    # TODO
    ```

-------------------------

Supported attributes for Nix dependencies:

```nix
{
  # Packages (e.g. binaries etc.) (list of strings | (pkgset -> pkg list))
  packages = [ "hello" ];
  # Alternative form. Allows arbitrary overrides.
  packages = pkgs: [ pkgs.zip ];

  # Python3 packages (list of strings | (pkgset -> pkg list))
  python3.pkgs = ps: [ ps.pynvim ];

  # Install/Upgrade hooks (attrs of string)
  hook.update = "echo update hook running";
}
```

### Upgrade plugins

```vim
:DeinUpdate
```

### Temporarily disabling a plugin

Call `call dein#disable(PLUGIN)` between `dein#begin()` and `dein#end()`.

### Python dependencies in plugins

Declare wanted packages in `nix_depends`: `python3.pkgs`. See [Plugin Nix
Dependencies] for installation.

NOTE: the old `mydein#install_python_requirements()` method is not to be used.


# Startup time

```bash
vim --not-a-term --startuptime /dev/stdout +qall | awk '{$1="" ; print $0}' | grep -E '^ *[0-9]' | sort
```

# Links

- <http://www.drchip.org/astronaut/vim/index.html#MANPAGEVIEW>

# Plugin: vim-easy-align

ALign table: visual select table and `:'<,'>EasyAlign` + `**|` etc.

a     |    bar | x   | FOOOC |
----- |  ----- | --  |       |
xx    | foobar | xxx |    6k |
1     |      2 | 3   |     4 | 5 |

<https://github.com/junegunn/vim-easy-align>
