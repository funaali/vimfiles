" Vim syntax file
" Language: rtorrent (.rtorrent.rc)

" Quit if syntax already loaded                                           {{{1
if exists('b:current_syntax')
  finish
endif
let s:cpo_save = &cpo
set cpo&vim

" iskeyword                                                               {{{1
syn iskeyword @,48-57,192-255,_,.

" Comments                                                                {{{1
syn keyword rtorrentTodo        contained FIXME NOTE TODO XXX
syn match   rtorrentLineComment "#.*" contains=rtorrentTodo,@Spell

" Literals                                                                {{{1
syn cluster rtorrentLiteral contains=rtorrentBool,rtorrentNumber,rtorrentConst,rtorrentStringLiteral

" Bare values, i.e. "foobar.com:1234"
syn match   rtorrentValue      contained "\c\<\%(\w\|[.:]\)\+\>"

syn keyword rtorrentBool   yes no false true

" Seconds "123", intervals "[dd:]hh:mm:ss"
syn match   rtorrentSecs "\<\%(\d\d:\)\?\d\d:\d\d:\d\d\|\d\+\>"

" Numbers "123", size "1234M"
syn match   rtorrentNumber "\<\d\+\>"
syn match   rtorrentConst  "\<\d\+[MG]\>"

" Strings "string"
syn match   rtorrentStringEscape   contained "\\."
syn region  rtorrentStringLiteral  contained start=+"+ skip=+\\"+ end=+"+me=s  contains=rtorrentStringEscape,@Spell
" Once escaped string, \"...\"
syn region  rtorrentStringLiteralE contained matchgroup=Special start=+\\"+ end=+\\"+ contains=@Spell

" Delimiters                                                              {{{1
syn match   rtorrentDelimiter    "[,;]"
syn match   rtorrentBraces       "[()]"

" Operators                                                               {{{1
syn keyword rtorrentOper    contained if not and or
syn match   rtorrentOper    contained "[-=|.]"
syn match   rtorrentEq      contained "="
syn match   rtorrentContinue  contained /\\$/

" Command Names                                                           {{{1
" https://github.com/rakshasa/rtorrent/wiki/rTorrent-0.9-Comprehensive-Command-list-(WIP)
syn keyword rtorrentCmdName contained encoding.add keys.layout.set import
syn keyword rtorrentCmdName contained execute2 execute.throw execute.throw.bg execute.nothrow execute.nothrow.bg execute.capture execute.capture_nothrow execute.raw execute.raw.bg execute.raw_nothrow execute.raw_nothrow.bg
syn keyword rtorrentCmdName contained directory.default directory.default.set
syn keyword rtorrentCmdName contained group.seeding.ratio.disable group.seeding.ratio.enable group2.seeding.ratio.max group2.seeding.ratio.max.set group2.seeding.ratio.min group2.seeding.ratio.min.set group2.seeding.ratio.upload group2.seeding.ratio.upload.set
syn keyword rtorrentCmdName contained convert.date convert.elapsed_time convert.gm_date convert.gm_time convert.kb convert.mb convert.time convert.throttle convert.xb
syn keyword rtorrentCmdName contained protocol.connection.leech protocol.connection.leech.set protocol.connection.leech.set protocol.connection.seed protocol.connection.seed.set protocol.connection.seed.set protocol.encryption.set protocol.pex protocol.pex.set protocol.pex.set
syn keyword rtorrentCmdName contained pieces.memory.current pieces.memory.max pieces.memory.max.set pieces.memory.max.set pieces.hash.on_completion pieces.hash.on_completion.set pieces.hash.on_completion.set pieces.preload.type pieces.preload.min_size pieces.preload.min_size.set pieces.preload.min_rate pieces.preload.min_rate.set pieces.preload.type.set pieces.stats_preloaded pieces.stats_not_preloaded pieces.sync.always_safe pieces.sync.always_safe.set pieces.sync.timeout pieces.sync.timeout.set pieces.sync.timeout_safe pieces.sync.timeout_safe.set
syn keyword rtorrentCmdName contained throttle.down throttle.down.max throttle.down.rate throttle.global_down.max_rate throttle.global_down.max_rate.set throttle.global_down.max_rate.set_kb throttle.global_down.rate throttle.global_down.total throttle.global_up.max_rate throttle.global_up.max_rate.set throttle.global_up.max_rate.set_kb throttle.global_up.rate throttle.global_up.total throttle.ip throttle.max_downloads.set throttle.max_downloads.div throttle.max_downloads.div.set throttle.max_downloads.div.set throttle.max_downloads.global throttle.max_downloads.global.set throttle.max_downloads.global.set throttle.max_peers.normal throttle.max_peers.normal.set throttle.max_peers.normal.set throttle.max_peers.seed throttle.max_peers.seed.set throttle.max_peers.seed.set throttle.max_uploads throttle.max_uploads.set throttle.max_uploads.set throttle.max_uploads.div throttle.max_uploads.div.set throttle.max_uploads.div.set throttle.max_uploads.global throttle.max_uploads.global.set throttle.max_uploads.global.set throttle.min_downloads.set throttle.min_peers.normal throttle.min_peers.normal.set throttle.min_peers.normal.set throttle.min_peers.seed throttle.min_peers.seed.set throttle.min_peers.seed.set throttle.min_uploads.set throttle.up throttle.up.max throttle.up.rate
syn keyword rtorrentCmdName contained dht.add_node dht.mode.set dht.port dht.port.set dht.port.set dht.throttle.name dht.throttle.name.set dht.statistics
syn keyword rtorrentCmdName contained network.bind_address network.bind_address.set network.bind_address.set network.local_address network.local_address.set network.local_address.set network.http.capath network.http.capath.set network.http.capath.set network.http.cacert network.http.cacert.set network.http.cacert.set network.http.max_open network.http.max_open.set network.http.proxy_address network.http.proxy_address.set network.http.proxy_address.set network.max_open_files network.max_open_files.set network.max_open_sockets network.port_open network.port_open.set network.port_open.set network.port_random network.port_random.set network.port_random.set network.port_range network.port_range.set network.port_range.set network.proxy_address network.proxy_address.set network.proxy_address.set network.receive_buffer.size network.receive_buffer.size.set network.scgi.dont_route.set network.scgi.dont_route network.scgi.open_local network.scgi.open_port network.send_buffer.size network.send_buffer.size.set network.xmlrpc.dialect.set network.xmlrpc.dialect.set network.xmlrpc.size_limit network.xmlrpc.size_limit.set network.xmlrpc.size_limit.set
syn keyword rtorrentCmdName contained session.path session.path.set session.path.set session.name session.name.set session.on_completion session.on_completion.set session.save session.use_lock session.use_lock.set
syn keyword rtorrentCmdName contained method.insert method.erase method.get method.set method.list_keys method.has_key method.set_key
syn keyword rtorrentCmdName contained system.file.allocate system.file.allocate.set system.file.max_size system.file.max_size.set system.file.split_size system.file.split_size.set system.file.split_suffix system.file.split_suffix.set system.daemon.set system.<env>.set system.shutdown.normal system.shutdown.quick
syn keyword rtorrentCmdName contained load.normal load.start load.start_verbose load.raw load.raw_start load.raw_verbose load.verbose
syn keyword rtorrentCmdName contained trackers.numwant trackers.numwant.set trackers.numwant.set trackers.use_udp trackers.use_udp.set trackers.use_udp.set
syn keyword rtorrentCmdName contained d.base_filename d.base_path d.bitfield d.bytes_done d.check_hash d.chunk_size d.chunks_hashed d.complete d.completed_bytes d.completed_chunks d.connection_current d.connection_current.set d.connection_leech d.connection_seed d.custom d.custom.set d.custom1 d.custom1.set d.custom2 d.custom2.set d.custom3 d.custom3.set d.custom4 d.custom4.set d.custom5 d.custom5.set d.custom_throw d.create_link d.creation_date d.delete_link d.delete_tied d.directory d.directory.set d.directory_base d.directory_base.set d.down.rate d.down.total d.free_diskspace d.hash d.hashing d.hashing_failed d.hashing_failed.set d.ignore_commands d.ignore_commands.set d.left_bytes d.local_id d.local_id_html d.loaded_file d.max_file_size d.max_file_size.set d.max_size_pex d.message d.message.set d.mode d.multicall2 d.name d.peer_exchange d.peers_accounted d.peers_complete d.peers_connected d.peers_max d.peers_max.set d.peers_min d.peers_min.set d.peers_not_connected d.priority d.priority.set d.priority_str d.ratio d.size_bytes d.size_chunks d.size_files d.size_pex d.skip.rate d.skip.total d.start d.state d.state_changed d.state_counter d.stop d.throttle_name d.throttle_name.set d.tied_to_file d.tied_to_file.set d.timestamp.finished d.timestamp.started d.tracker_focus d.tracker_numwant d.tracker_numwant.set d.tracker_size d.up.rate d.up.total d.uploads_max d.uploads_max.set
syn keyword rtorrentCmdName contained t.group t.id t.is_enabled.set t.min_interval t.normal_interval t.scrape_complete t.scrape_downloaded t.scrape_incomplete t.scrape_time_last t.type t.url
syn keyword rtorrentCmdName contained f.completed_chunks f.frozen_path f.last_touched f.match_depth_next f.match_depth_prev f.offset f.path f.path_components f.path_depth f.priority f.range_first f.range_second f.size_bytes f.size_chunks f.priority.set fi.filename_last
syn keyword rtorrentCmdName contained p.address p.client_version p.completed_percent p.down_rate p.down_total p.id p.id_html p.options_str p.peer_rate p.peer_total p.port p.up_rate p.up_total
syn keyword rtorrentCmdName contained view.add view.filter view.filter_on view.list view.set view.sort view.sort_current view.sort_new

" Types                                                                   {{{1
syn keyword rtorrentType              simple string const private value

" Identifiers                                                             {{{1
syn match   rtorrentIdentifier contained "\c\<[a-z][a-z0-9_.]*\>"
syn match   rtorrentVar        contained "\c\<[a-z]\k*\>"

syn match   rtorrentMethod     contained "\k\+"

" Commands:                                                               {{{1
" "<command>=[<args>...][ ; <command>=...]" list of commands separated by ";"
syn cluster rtorrentCmdTop contains=rtorrentCmdName,rtorrentEq,rtorrentValue,rtorrentDelimiter,rtorrentBool,rtorrentNumber,rtorrentConst

" "<command>=[<args>...][ ; <command>=...]" wrapped in "..." (double quotes)
syn region  rtorrentCmdString    contained matchgroup=Special start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=@rtorrentCmdTop,rtorrentStringLiteralE
" "<command>=[<args>...][ ; <command>=...]" wrapped in \"...\" (escaped double quotes)
syn region  rtorrentCmdStringE   contained matchgroup=Special start=+\\"+ end=+\\"+ contains=@rtorrentCmdTop
" "(( <command>[, <args>...] ))" new style command
syn region  rtorrentCmdParens    contained matchgroup=Special start=+((+ skip=+((.\{-}))+ end=+))+ contains=@rtorrentCmdTop,rtorrentStringLiteral

syn region  rtorrentParens contained matchgroup=Special start=+(+ end=+)+ contains=@rtorrentCmdList

syn cluster rtorrentCmdList contains=@rtorrentLiteral,rtorrentDelimiter,rtorrentOper,rtorrentVar,rtorrentParens,rtorrentContinue,rtorrentType
syn region  rtorrentCmd       contained start="\_." skip=+\\\n+ end=+$+ contains=@rtorrentCmdList

" (schedule2 =) <name>, <start>, <interval>, ((<command>[, <args>...]))
" (schedule2 =) <name>, <start>, <interval>, "<command>=[<args...][ ; <command>=...]"
syn region  rtorrentSchedule  contained start="\_." skip=+\\\n+ end=+$+ contains=rtorrentDelimiter,rtorrentVar,rtorrentSecs,rtorrentContinue,rtorrentCmdString,rtorrentCmdParens

" Statements:                                                             {{{1
syn match   rtorrentStatement           "^\s*[a-zA-Z]\k*\(\\\\\|\\\n\|.\)*$" contains=rtorrentLhs
syn match   rtorrentLhs       contained "^\k\+\>\s*="                        contains=rtorrentMethod,rtorrentEq skipwhite nextgroup=rtorrentCmd
syn match   rtorrentLhs       contained "^schedule2\?\>\s*="                 contains=rtorrentMethod,rtorrentEq skipwhite nextgroup=rtorrentSchedule

" Sync:                                                                   {{{1

syn sync minlines=50

" Highlights:                                                             {{{1
hi def link rtorrentTodo Todo
hi def link rtorrentLineComment Comment
hi def link rtorrentBool Boolean
hi def link rtorrentNumber Number
hi def link rtorrentSecs Constant
hi def link rtorrentConst Constant
hi def link rtorrentStringEscape Special
hi def link rtorrentStringLiteral String
hi def link rtorrentStringLiteralE String
hi def link rtorrentDelimiter Delimiter
hi def link rtorrentBraces Delimiter
hi def link rtorrentType Type
hi def link rtorrentContinue Special
hi def link rtorrentOper Operator
hi def link rtorrentEq Operator
hi def link rtorrentIdentifier Identifier
hi def link rtorrentVar Identifier
hi def link rtorrentMethod Statement
hi def link rtorrentCmdName Statement

" Current Syntax:                                                         {{{1
let b:current_syntax = 'rtorrent'

" Cleanup:                                                                {{{1
let &cpo = s:cpo_save
unlet s:cpo_save
