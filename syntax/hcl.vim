" based on https://github.com/b4b4r07/vim-hcl/blob/master/syntax/hcl.vim

if exists('b:current_syntax')
  finish
endif

syn match hclEqual '='
syn match hclBraces "[\[\]]"

syn keyword hclTodo contained TODO FIXME XXX
syn region hclComment display oneline start='\%\(^\|\s\)#' end='$' contains=hclTodo,@Spell
syn region hclComment display oneline start='\%\(^\|\s\)//' end='$' contains=hclTodo,@Spell

syn match hclValueNumber "\<[0-9]\+\>"

syn keyword hclValueBool true false

""" skip \" and \\ in strings.
syn region hclValueString   start=/"/ skip=/\\\\\|\\"/ end=/"/ contains=hclStringInterp,hclConsulTemplate
syn region hclStringInterp  matchgroup=hclBraces start=/\${/ end=/}/ contained contains=ALL
syn region hclHereDocText   start=/<<-\?\z([a-z0-9A-Z]\+\)/ end=/^\s*\z1/ contains=hclStringInterp,hclConsulTemplate

syn region hclBlock matchgroup=hclBraces start=/{/ end=/}/ fold transparent

syn region hclConsulTemplate matchgroup=hclBraces start=/{{/ end=/}}/ contained contains=consulTemplateKeyword,consulTemplateIdentifier

syn keyword consulTemplateKeyword    contained if else end with range
syn match   consulTemplateIdentifier contained '\(\.\|\$\)\k\+\>'

hi def link hclComment           Comment
hi def link hclTodo              Todo
hi def link hclEqual             Operator
hi def link hclBraces            Delimiter
hi def link hclValueBool         Boolean
hi def link hclValueString       String
hi def link hclHereDocText       String
hi def link hclValueNumber       Number

hi def link consulTemplateKeyword Keyword
hi def link consulTemplateIdentifier Identifier

let b:current_syntax = 'hcl'
