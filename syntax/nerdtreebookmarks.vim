" syntax/nerdtreebookmarks.vim

if exists('b:current_syntax')
  finish
endif

syn match NERDTreeBookmarkName /^.\{-} /he=e-1
syn match NERDTreeBookmark     / {.*}/hs=s+1
syn match NERDTreeBookmarkInvalid contained /.*/

syn region NERDTreeBookmarks        start=/\%^/ end=/^$/  contains=NERDTreeBookmark,NERDTreeBookmarkName
syn region NERDTreeBookmarksInvalid start=/^$/  end=/\%$/ contains=NERDTreeBookmarkInvalid

"hi def link NERDTreeBookmark     Normal
hi def link NERDTreeBookmarkInvalid Comment
hi def link NERDTreeBookmarkName    Identifier

let b:current_syntax = 'nerdtreebookmarks'
