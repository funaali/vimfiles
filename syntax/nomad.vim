" syntax/nomad.vim

if exists('b:current_syntax')
  finish
endif

runtime! syntax/hcl.vim
unlet b:current_syntax

syn keyword nomadBlockType job group task
"syn keyword nomadStanza datacenters constraint group task
"syn keyword nomadParameter datacenters attribute value nextgroup=hclEqual

hi def link nomadBlockType Structure

let b:current_syntax = 'nomad'
